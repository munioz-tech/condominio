<?php 
Class Utils{
    static public function PlantillaAlerta(){
        $html = "Se ha presenciado a una o varias personas peligrosas en los alrededores del condominio,
         se pide tener cuidado y estar atento ante las situaciones que podr&iacute;an presentarse";
        return $html;
    }

    static public function envioAlerta($usersendalert,$correoAdm,$txtAlert)
    {   $Mail = new Mailer();
        $Mail->setFromName("Condominio Los Jardines");
        $Mail->setSubject("Alerta de Peligro");
        $Mail->setBodyHTML(self::ArmarCorreo($usersendalert,$txtAlert));
        $Mail->addTo('antony.cangao@ug.edu.ec');
        $Mail->addCopyTo($correoAdm); 
        return $Mail->enviaCorreo();
    }

    static public function ArmarCorreo($enviaAlert, $txtAlert)
    {   $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <h3>Alerta: Aviso de situaci&oacute;n que lo pondr&aacute; en peligro en el Condominio y sus alrededores.</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <b>El inquilino ".utf8_decode($enviaAlert["NOMBRE"])." ".utf8_decode($enviaAlert["APELLIDOS"])." Informa: </b>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>"
                    .utf8_decode($txtAlert).
                    "</td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <b>Datos del Inquilino</b><br>
                        <b>C&eacute;dula: </b> ".$enviaAlert["CEDULA"]."<br>
                        <b>Correo: </b> ".utf8_decode($enviaAlert["CORREO"])."<br>
                        <b>N&uacute;mero de Edificio: </b> ".$enviaAlert["N_EDIFICIO"]."<br>
                        <b>N&uacute;mero de Habitaci&oacute;n: </b> ".$enviaAlert["N_HABITACION"]."<br>
                        <b>Tel&eacute;fono: </b> ".$enviaAlert["TELEFONO"]."<br>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <br><b>Saludos.</b>
                        <br>
                        <br>Condominio Los Jardines.
                        <br>Velamos por su seguridad.
                        <br><br>
                        Por favor no responda a esta direcci&oacute;n de correo.
                    </td>
                </tr>
                </table>";
                return $mnj;
    }

    static public function avisoAlerta($usersendalert,$txtAlert)
    {   $Mail = new Mailer();
        $Mail->setFromName("Condominio Los Jardines");
        $Mail->setSubject("ALERTA EXITOSA");
        $Mail->setBodyHTML(self::ArmarCorreoAviso($usersendalert,$txtAlert));
        $Mail->addTo($usersendalert["CORREO"]);
        return $Mail->enviaCorreo();
    }
    
    static public function ArmarCorreoAviso($avisoAlert, $txtAlert)
    {   $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <h3>Ha enviado satisfactoriamente un aviso.</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <b>Sr(a): ".utf8_decode($avisoAlert["NOMBRE"])." ".utf8_decode($avisoAlert["APELLIDOS"])."</b>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                        Usted ha enviado la siguiente alerta a los dirigentes del condominio: <br>".utf8_decode($txtAlert)."
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                    Si usted no ha enviado alertas, p&oacute;ngase en contacto con el administrador.
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <br><b>Saludos.</b>
                        <br>
                        <br>Condominio Los Jardines.
                        <br>Velamos por su seguridad.
                        <br><br>
                        Por favor no responda a esta direcci&oacute;n de correo.
                    </td>
                </tr>
                </table>";
                return $mnj;
    }
/*====================================
            PUBLICACIONES
====================================*/
    static public function ArmarCorreoProcesarPublicaciones($datos){
        if($datos["procesar"] == 1){
            $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <h3>Publicaci&oacute;n Rechazada.</h3>
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                Su publicaci&oacute;n ha sido rechazada debido a que infringe las normas que se deben seguir en el Condominio. A continuaci&oacute;n se detallar&aacute; el motivo por el cual ha sido rechazado:
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <b>".utf8_decode($datos["observacion"])."</b>
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <br><b>Saludos.</b>
                    <br>
                    <br>Condominio Los Jardines.
                    <br>Velamos por su seguridad.
                    <br><br>
                    Por favor no responda a esta direcci&oacute;n de correo.
                </td>
            </tr>
            </table>";
            return $mnj;
        }
        if($datos["procesar"] == 2){
            $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <h3>Publicaci&oacute;n Aceptada.</h3>
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                Su publicaci&oacute;n ha sido aceptada. A continuaci&oacute;n se detallar&aacute; el motivo por el cual se acepta su publicaci&oacute;n:
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <b>".utf8_decode($datos["observacion"])."</b>
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <br><b>Saludos.</b>
                    <br>
                    <br>Condominio Los Jardines.
                    <br>Velamos por su seguridad.
                    <br><br>
                    Por favor no responda a esta direcci&oacute;n de correo.
                </td>
            </tr>
            </table>";
            return $mnj;
        }
    }

    static public function EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos){
        $Mail = new Mailer();
        $Mail->setFromName("Condominio Los Jardines");
        $Mail->setSubject(utf8_decode("Petición de Publicación"));
        $Mail->setBodyHTML(self::ArmarCorreoProcesarPublicaciones($datos));
        $Mail->addTo($CorreoUsuario);
        return $Mail->enviaCorreo();
    }

/*====================================
                NUME SEG
====================================*/
    static public function EnviarNumSeg($datos)
    {   $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <h3>Usuario Creado.</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <b>Sr(a): ".utf8_decode($datos["nombre"])." ".utf8_decode($datos["apellidos"])."</b>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                        Su usuario ha sido creado correctamente para que pueda hacer uso del Sistema web del Condominio Los Jardines.
                        Por favor, compruebe que sus datos estén correctos: 
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <br>Nombre: ".$datos["nombre"]."
                        <br>Apellidos ".$datos["apellidos"]."
                        <br>Edificio N°: ".$datos["edificio"]."
                        <br>Habitación: ".$datos["habitacion"]."
                        <br>Teléfono: ".$datos["telefono"]."
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                        Su número de seguridad para poder enviar alertas es: ".$datos["num_seg"]."
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                        Esperamos cumpla con las reglas impuestas por la administración, si infringe las reglas, su usuario será desactivado.
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <br><b>Saludos.</b>
                        <br>
                        <br>Condominio Los Jardines.
                        <br>Velamos por su seguridad.
                        <br><br>
                        Por favor no responda a esta direcci&oacute;n de correo.
                    </td>
                </tr>
                </table>";
                return $mnj;
    }

    static public function EnviarCorreoNumSeg($datos){
        $Mail = new Mailer();
        $Mail->setFromName("Condominio Los Jardines");
        $Mail->setSubject(utf8_decode("Usuario Creado"));
        $Mail->setBodyHTML(self::EnviarNumSeg($datos));
        $Mail->addTo($datos["email"]);
        return $Mail->enviaCorreo();
    }

    static public function EnviarNumSegEditado($datos)
    {   $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
        <tr>
            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                <h3>Usuario Modificado.</h3>
            </td>
        </tr>
        <tr>
            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                <b>Sr(a): ".utf8_decode($datos["nombre"])." ".utf8_decode($datos["apellidos"])."</b>
            </td>
        </tr>
        <tr>
            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                Su usuario ha sido modificado correctamente para que continúe haciendo uso del Sistema web del Condominio Los Jardines.
                Por favor, compruebe que sus datos estén correctos: 
            </td>
        </tr>
        <tr>
            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                <br>Nombre: ".$datos["nombre"]."
                <br>Apellidos ".$datos["apellidos"]."
                <br>Edificio N°: ".$datos["edificio"]."
                <br>Habitación: ".$datos["habitacion"]."
                <br>Teléfono: ".$datos["telefono"]."
            </td>
        </tr>
        <tr>
            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                Su número de seguridad para poder enviar alertas es: ".$datos["num_seg"]."
            </td>
        </tr>
        <tr>
            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                Esperamos cumpla con las reglas impuestas por la administración, si infringe las reglas, su usuario será desactivado.
            </td>
        </tr>
        <tr>
            <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                <br><b>Saludos.</b>
                <br>
                <br>Condominio Los Jardines.
                <br>Velamos por su seguridad.
                <br><br>
                Por favor no responda a esta direcci&oacute;n de correo.
            </td>
        </tr>
        </table>";
        return $mnj;
    }

    static public function EnviarCorreoNumSegEditado($datos){
        $Mail = new Mailer();
        $Mail->setFromName("Condominio Los Jardines");
        $Mail->setSubject(utf8_decode("Usuario Modificado"));
        $Mail->setBodyHTML(self::EnviarNumSegEditado($datos));
        $Mail->addTo($datos["email"]);
        return $Mail->enviaCorreo();
    }

/*====================================
                BLOQUEO
====================================*/
    static public function avisoBloqueo($usuario)
    {   $Mail = new Mailer();
        $Mail->setFromName("Condominio Los Jardines");
        $Mail->setSubject("USUARIO BLOQUEADO");
        $Mail->setBodyHTML(self::ArmarCorreoBloqueo($usuario));
        $Mail->addTo($usuario["CORREO"]);
        return $Mail->enviaCorreo();
    }
    
    static public function ArmarCorreoBloqueo($usuario)
    {   $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
                <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <h3>Su usuario ha sido bloqueado.</h3>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <b>Sr(a): ".utf8_decode($usuario["NOMBRE"])." ".utf8_decode($usuario["APELLIDOS"])."</b>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                        Su usuario ha sido bloqueado debido a que ha sobrepasado el límite de intentos de inicios de sesión.
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                    Si usted no ha realizado ningún intento, p&oacute;ngase en contacto con el administrador.
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                        <br><b>Saludos.</b>
                        <br>
                        <br>Condominio Los Jardines.
                        <br>Velamos por su seguridad.
                        <br><br>
                        Por favor no responda a esta direcci&oacute;n de correo.
                    </td>
                </tr>
                </table>";
                return $mnj;
    }

/*====================================
            REUNIONES
====================================*/
static public function avisoReunion($correoAdm)
{   $Mail = new Mailer();
    $Mail->setFromName("Condominio Los Jardines");
    $Mail->setSubject("Convocatoria a Reunión");
    $Mail->setBodyHTML(self::ArmarCorreoReunion());
    $Mail->addTo($correoAdm["CORREO"]);
    return $Mail->enviaCorreo();
}

static public function ArmarCorreoReunion()
{   $mnj = "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
            <table border='1' cellspacing='10' width='600px' style='border:1px solid #ddd'>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <h3>Se lo ha convocado a una reuni&oacute;n en l&iacute;nea.</h3>
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <b>Estimado inquilino del Condominio los Jardines</b>
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'><br>
                    Se ha creado una reuni&oacute;n mediante la plataforma Zoom, para conocer m&aacute;s acerca de esta invitaci&oacute;n se le pide ingresar al sistema web del condominio
                </td>
            </tr>
            <tr>
                <td colspan='2' style='text-align:justify; font-family:arial; font-size:11pt; border:0'>
                    <br><b>Saludos.</b>
                    <br>
                    <br>Condominio Los Jardines.
                    <br>Velamos por su seguridad.
                    <br><br>
                    Por favor no responda a esta direcci&oacute;n de correo.
                </td>
            </tr>
            </table>";
            return $mnj;
}
/*====================================
                OTROS
====================================*/
    static public function obtenerIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {   $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            {   $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
            $ip = $_SERVER['REMOTE_ADDR'];
            } return $ip;
    }

    static public function MensajesSweetAlert($tipo,$titulo,$txtAlert,$pagina){
        if($tipo == "success"){
            if($pagina == ""){
                 echo '<script language="javascript">
                        Swal.fire({
                            icon: "'.$tipo.'",
                            title: "'.$titulo.'",
                            text: "'.$txtAlert.'",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        })
                    </script>';
                return;
            }   
            elseif($pagina != ""){
                echo '<script language="javascript">
                        Swal.fire({
                            icon: "'.$tipo.'",
                            title: "'.$titulo.'",
                            text: "'.$txtAlert.'",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        }).then(function(result){
                            if (result.value) {
                            window.location = "'.$pagina.'";
                            }
                        })
                    </script>';
                return;
            }
        }
        elseif($tipo == "error"){
            if($pagina == ""){ 
                echo '<script language="javascript">
                        Swal.fire({
                            icon: "'.$tipo.'",
                            title: "'.$titulo.'",
                            text: "'.$txtAlert.'",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        })
                        </script>';
                    return;
            }
            elseif($pagina != ""){
                echo '<script language="javascript">
                        Swal.fire({
                            icon: "'.$tipo.'",
                            title: "'.$titulo.'",
                            text: "'.$txtAlert.'",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        }).then(function(result){
                            if (result.value) {
                            window.location = "'.$pagina.'";
                            }
                        })
                    </script>';
                return;
            }
        }
    }
}