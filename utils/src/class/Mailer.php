<?php
        require_once("utils/src/plugins/mailer/src/PHPMailer.php");
        require_once("utils/src/plugins/mailer/src/SMTP.php"); 
    
        class  Mailer //extends DAO
        {       private $ObjMail;
                private $FrmName;
                private $Subject;
                private $Destiny;
                private $BodyHTM;
                private $CopyTo;
                private $Cuenta = "antony.cangao@gmail.com";
                private $Tokens = "Akcor18:)";
                                  

                function __construct()
                {   $this->Utils = new Utils();
                }

                function setFromName ($frmn) {  $this->FrmName   = $frmn;  }
                function setSubject  ($subj) {  $this->Subject   = $subj;  }
                function setBodyHTML ($html) {  $this->BodyHTM   = $html;  }
                function addTo       ($toem) {  $this->Destiny[]   = $toem;  }
                function addCopyTo   ($tocp) {  $this->CopyTo    = $tocp;  }

                /*protected function EmailDecoder($cuentas)
                {       $vcorreos = array();
                        $elements = explode(';',$cuentas);
                        for ($m=0; $m<count($elements); $m++)
                        {   if (filter_var($elements[$m], FILTER_VALIDATE_EMAIL))
                            {   $vcorreos [] = trim($elements[$m]);
                            }
                        }
                        return $vcorreos;
                }*/

                function enviaCorreo()
                {       $ecorreos = $this->Destiny;
                        if (count($ecorreos)>0)
                        {   $this->ObjMail = new PHPMailer();
                            $this->ObjMail->SetLanguage("es", 'utils/src/plugins/mailer/language/'); 
                            $this->ObjMail->IsSMTP();
                            
                            $this->ObjMail->Host = 'smtp.gmail.com';
                            $this->ObjMail->Port = 587;                    
                            $this->ObjMail->SMTPSecure = 'tls';
                            $this->ObjMail->SMTPAuth = true;
                            $this->ObjMail->Username = $this->Cuenta; 
                            $this->ObjMail->Password = $this->Tokens;
                            $this->ObjMail->From = $this->Cuenta; 
                            $this->ObjMail->FromName = $this->FrmName;
                            $this->ObjMail->Timeout = 30;

                            for ($m=0; $m<count($ecorreos); $m++)
                            {    $this->ObjMail->AddAddress($ecorreos[$m]);
                            }

                            //print_r(count($this->CopyTo));
                                if(!empty($this->CopyTo))
                                {   $ccorreos = $this->CopyTo;
                                        for ($m=0; $m<count($ccorreos); $m++)
                                        {       $this->ObjMail->AddCC($ccorreos[$m]);
                                        }
                                }
                                $this->ObjMail->Subject = $this->Subject;
                                $this->ObjMail->IsHTML(true);
                                $this->ObjMail->Body = trim($this->BodyHTM);
                                return ($this->ObjMail->Send() ? "OK": "Error");
                        }
                        return array(false,"No existen destinatarios del correo.");
                }
        }
?>