<!--=================================
  PLUGINS JS
  ==================================-->
  <!-- jQuery -->
  <script src="view/plugins/jquery/jquery.min.js"></script>
  <script src="view/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- FastClick -->
  <script src="view/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="view/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="view/dist/js/demo.js"></script>
  <!-- Personalize JS -->
  <script src="view/js/plantilla.js"></script>
  <script src="view/js/usuario.js"></script>
  <script src="view/js/reporte.js"></script>
  <script src="view/js/actividad.js"></script>
  <script src="view/js/negocio.js"></script>
  <script src="view/js/buzon.js"></script>
  <script src="view/js/reservas.js"></script>
  <script src="view/js/noticias.js"></script>
  <script src="view/js/reuniones.js"></script>
  <script src="view/js/publicaciones.js"></script>
  <script src="view/js/configuracion.js"></script>
  <script src="utils/Utils.js"></script>
  <!-- DataTables -->
  <script src="view/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="view/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="view/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="view/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <!-- Daterangepicker -->
  <script src="view/plugins/daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <!-- Botones de DataTables -->
  <script src="view/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="view/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="view/plugins/datatables-buttons/js/buttons.print.min.js"></script>
  <script src="view/plugins/pdfmake/pdfmake.min.js"></script>
  <script src="view/plugins/jszip/jszip.min.js"></script>
  <script src="view/plugins/pdfmake/vfs_fonts.js"></script>
  <!-- FULL CALENDAR -->
  <script src='view/plugins/full-calendar/lib/locales-all.js'></script>
  <script src='view/plugins/full-calendar/lib/locales-all.min.js'></script>
  <script src='view/plugins/full-calendar/lib/main.js'></script>
  <script src='view/plugins/full-calendar/lib/main.min.js'></script>
  <script src='view/plugins/full-calendar/lib/locales/es-us.js'></script>
  <!-- ChartJS -->
  <!--<script src="view/plugins/chart.js/Chart.js"></script>
  <script src="view/plugins/chart.js/Chart.bundle.js"></script>-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.js" integrity="sha512-G8JE1Xbr0egZE5gNGyUm1fF764iHVfRXshIoUWCTPAbKkkItp/6qal5YAHXrxEu4HNfPTQs6HOu3D5vCGS1j3w==" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js" integrity="sha512-vBmx0N/uQOXznm/Nbkp7h0P1RfLSj0HQrFSzV8m7rOGyj30fYAOKHYvCNez+yM8IrfnW0TCodDEjRqf6fodf/Q==" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js" integrity="sha512-QEiC894KVkN9Tsoi6+mKf8HaCLJvyA6QIRzY5KrfINXYuP9NxdIkRQhGq3BZi0J4I7V5SidGM3XUQ5wFiMDuWg==" crossorigin="anonymous"></script>
  <!-- MorrisJS -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

<!-- GRAFICO DE ACCESOS -->
<?php
    $accesos = ControladorUsuario::ctrConsultarAccesos();
?>
<script> 
  Morris.Line({
      element: 'chart',
      data: [
        <?php
          if($accesos != null){
            foreach ($accesos as $key => $value)
            {
              echo "{ year: '".$value["FECHA"]."', value: ".$value["TOTAL"]." },";
            }
            echo "{ year: '".$value["FECHA"]."', value: ".$value["TOTAL"]." }";
          }
          else{
            echo "{ year: '0', value: '0' },";
          }
        ?>
      ],
      xkey: 'year',
      ykeys: ['value'],
      labels: ['Value'],
      lineColors       : ['#efefef'],
      gridTextColor    : '#fff',
      pointStrokeColors: ['#efefef'],
      gridLineColor    : '#efefef',
      gridTextFamily   : 'Open Sans',
      gridTextSize     : 12,
      lineWidth        : 3,
      hideHover        : 'auto',
      gridStrokeWidth  : 0.5,
      pointSize        : 5
  });
</script>

<!-- GRAFICO DE ACCESOS -->
<?php
    $actividades = ControladorActividades::ctrTotalActividades();
    $buzon = ControladorBuzon::ctrTotalBuzon();
    $reservas = ControladorReservas::ctrTotalReservas();
    $negocios = ControladorNegocios::ctrTotalNegocios();
?>
<script>
var ctx = document.getElementById('pieChart').getContext('2d');
var chart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Actividades', 'Reservas', 'Negocios', 'Buzon'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: ['#28a745','#ffc107', '#007bff', '#dc3545'],
            borderColor: 'rgb(0, 0, 0)',
            data:[<?php echo $actividades["total"]?>,<?php echo $reservas["total"]?>,<?php echo $negocios["total"]?>,<?php echo $buzon["total"]?>]
        }]
    },

    // Configuration options go here
    options: {}
});
</script>
