-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-10-2020 a las 07:14:28
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdcondominio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_actividades`
--

CREATE TABLE `adm_actividades` (
  `id` int(11) NOT NULL,
  `tipo_actividad` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL,
  `u_modifica` int(11) NOT NULL,
  `f_modifica` datetime NOT NULL,
  `estado` int(11) NOT NULL,
  `id_padre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_actividades`
--

INSERT INTO `adm_actividades` (`id`, `tipo_actividad`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`, `id_padre`) VALUES
(1, 'Mantenimiento', 1, '2020-08-24 01:44:32', 0, '0000-00-00 00:00:00', 1, 0),
(2, 'Extravío', 1, '2020-08-24 01:44:32', 0, '0000-00-00 00:00:00', 1, 0),
(3, 'Varios', 1, '2020-08-24 03:01:39', 0, '0000-00-00 00:00:00', 1, 0),
(4, 'Pintar Canchas', 1, '2020-08-24 03:02:25', 0, '0000-00-00 00:00:00', 1, 1),
(5, 'Regar Plantas', 1, '2020-08-24 03:02:25', 0, '0000-00-00 00:00:00', 1, 1),
(6, 'Minga', 1, '2020-08-24 03:02:25', 0, '0000-00-00 00:00:00', 1, 1),
(7, 'Objeto', 1, '2020-08-24 03:02:25', 0, '0000-00-00 00:00:00', 1, 2),
(8, 'Mascota', 1, '2020-08-24 03:02:25', 0, '0000-00-00 00:00:00', 1, 2),
(9, 'Reunión', 1, '2020-08-24 03:02:25', 0, '0000-00-00 00:00:00', 1, 3),
(10, 'Festividad', 1, '2020-08-24 03:02:25', 0, '0000-00-00 00:00:00', 1, 3),
(11, 'Bailoterapia', 1, '2020-08-24 03:06:12', 0, '0000-00-00 00:00:00', 1, 3),
(12, 'Otro', 1, '2020-08-24 03:06:12', 0, '0000-00-00 00:00:00', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_buzon`
--

CREATE TABLE `adm_buzon` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `queja_idea` int(2) NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_procesa` int(11) NOT NULL,
  `f_procesa` datetime NOT NULL DEFAULT '1999-01-01 00:00:00',
  `observacion` varchar(100) COLLATE utf8_spanish_ci DEFAULT '',
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_buzon`
--

INSERT INTO `adm_buzon` (`id`, `descripcion`, `queja_idea`, `u_crea`, `f_crea`, `u_procesa`, `f_procesa`, `observacion`, `estado`) VALUES
(8, 'Los vecinos hacen ruido con sus parlantes', 1, 1, '2020-10-13 00:22:51', 1, '2020-10-13 00:28:07', 'Acepto la queja (prueba)', 10),
(9, 'Necesitamos guardias de seguridad', 2, 1, '2020-10-13 00:24:44', 1, '2020-10-13 00:27:44', 'Acepto la idea (prueba)', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_canchas`
--

CREATE TABLE `adm_canchas` (
  `id` int(11) NOT NULL,
  `cancha` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_modifica` int(11) NOT NULL,
  `f_modifica` datetime NOT NULL DEFAULT '1999-01-01 00:00:00',
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_canchas`
--

INSERT INTO `adm_canchas` (`id`, `cancha`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`) VALUES
(1, 'Tenis', 1, '2020-09-21 18:32:52', 0, '0000-00-00 00:00:00', 1),
(2, 'Indor', 1, '2020-09-21 18:32:52', 0, '0000-00-00 00:00:00', 1),
(3, 'Voley', 1, '2020-09-21 18:32:52', 0, '0000-00-00 00:00:00', 1),
(4, 'Básquet', 1, '2020-09-21 18:32:52', 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_invitado`
--

CREATE TABLE `adm_invitado` (
  `id` int(11) NOT NULL,
  `cedula` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `f_ingreso` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_invitado`
--

INSERT INTO `adm_invitado` (`id`, `cedula`, `nombre`, `apellidos`, `telefono`, `correo`, `ip`, `f_ingreso`) VALUES
(1, '0931130363', 'Gonzalo', 'ALCH ACH', '0893215651', 'munioz@gmail.com', '::1', '2020-09-25 23:49:32'),
(2, '0931130363', 'Gonzalo', 'Munoz G', '0990318933', 'munioz.gonzalog@gmail.com', '::1', '2020-10-07 13:52:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_negocios`
--

CREATE TABLE `adm_negocios` (
  `id` int(11) NOT NULL,
  `tipo_negocio` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL,
  `u_modifica` int(11) NOT NULL,
  `f_modifica` datetime NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_negocios`
--

INSERT INTO `adm_negocios` (`id`, `tipo_negocio`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`) VALUES
(1, 'Venta', 1, '2020-08-26 15:36:54', 0, '0000-00-00 00:00:00', 1),
(2, 'Alquiler', 1, '2020-08-26 15:36:54', 0, '0000-00-00 00:00:00', 1),
(3, 'Curso', 1, '2020-08-26 15:37:46', 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_noticia`
--

CREATE TABLE `adm_noticia` (
  `id` int(11) NOT NULL,
  `id_publicacion` int(11) NOT NULL,
  `id_tipo_publicacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `adm_noticia`
--

INSERT INTO `adm_noticia` (`id`, `id_publicacion`, `id_tipo_publicacion`) VALUES
(9, 34, 1),
(10, 33, 1),
(11, 32, 1),
(12, 10, 3),
(13, 9, 3),
(14, 8, 3),
(15, 9, 4),
(16, 8, 4),
(17, 33, 2),
(18, 31, 2),
(19, 32, 2),
(20, 30, 2),
(29, 6, 5),
(30, 7, 5),
(31, 8, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_registro_act`
--

CREATE TABLE `adm_registro_act` (
  `id` int(10) NOT NULL,
  `id_actividad` int(10) NOT NULL,
  `id_act_padre` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `ruta_img` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `u_crea` int(10) NOT NULL,
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_procesa` int(11) NOT NULL,
  `f_procesa` datetime NOT NULL DEFAULT '1999-01-01 00:00:00',
  `observacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_registro_act`
--

INSERT INTO `adm_registro_act` (`id`, `id_actividad`, `id_act_padre`, `descripcion`, `ruta_img`, `u_crea`, `f_crea`, `u_procesa`, `f_procesa`, `observacion`, `estado`) VALUES
(32, 6, 1, 'Prueba de Minga.', 'view/img/actividades/0952806339/554.jpg', 1, '2020-10-13 00:13:22', 1, '2020-10-13 00:25:49', 'Acepto actividad (prueba 3)', 10),
(33, 8, 2, 'Perro extraviado.', 'view/img/actividades/0952806339/327.jpg', 1, '2020-10-13 00:13:47', 1, '2020-10-13 00:25:33', 'Acepto actividad (prueba 2)', 10),
(34, 10, 3, 'Halloween', 'view/img/actividades/0952806339/815.jpg', 1, '2020-10-13 00:14:40', 1, '2020-10-13 00:25:16', 'Acepto actividad (prueba 1)', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_registro_cancha`
--

CREATE TABLE `adm_registro_cancha` (
  `id` int(11) NOT NULL,
  `id_cancha` int(11) NOT NULL,
  `separa` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `fyh_desde` datetime NOT NULL,
  `fyh_hasta` datetime NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_procesa` int(11) NOT NULL,
  `f_procesa` datetime NOT NULL DEFAULT '1999-01-01 00:00:00',
  `observacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_registro_cancha`
--

INSERT INTO `adm_registro_cancha` (`id`, `id_cancha`, `separa`, `descripcion`, `fyh_desde`, `fyh_hasta`, `u_crea`, `f_crea`, `u_procesa`, `f_procesa`, `observacion`, `estado`) VALUES
(30, 1, 'Antony Canga Ortega', 'Prueba tenis', '2020-10-14 08:00:00', '2020-10-15 10:00:00', 1, '2020-10-13 00:15:33', 1, '2020-10-13 00:31:01', 'Acepto reserva (prueba 4)', 10),
(31, 2, 'Antony Canga Ortega', 'Prueba Indor', '2020-10-14 11:00:00', '2020-10-15 12:00:00', 1, '2020-10-13 00:15:54', 1, '2020-10-13 00:30:30', 'Acepto reserva (prueba 2)', 10),
(32, 3, 'Antony Canga Ortega', 'Prueba Voley', '2020-10-14 10:00:00', '2020-10-15 12:00:00', 1, '2020-10-13 00:16:12', 1, '2020-10-13 00:30:46', 'Acepto reserva (prueba 3)', 10),
(33, 4, 'Antony Canga Ortega', 'Prueba Básquet', '2020-10-16 12:15:00', '2020-10-20 15:45:00', 1, '2020-10-13 00:16:39', 1, '2020-10-13 00:30:11', 'Acepto reserva (prueba 1)', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_registro_neg`
--

CREATE TABLE `adm_registro_neg` (
  `id` int(11) NOT NULL,
  `id_negocio` int(11) NOT NULL,
  `descripcion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `t_negocio` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `precio` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_procesa` int(11) NOT NULL,
  `f_procesa` datetime NOT NULL DEFAULT '1999-01-01 00:00:00',
  `ruta_img` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `observacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_registro_neg`
--

INSERT INTO `adm_registro_neg` (`id`, `id_negocio`, `descripcion`, `t_negocio`, `precio`, `u_crea`, `f_crea`, `u_procesa`, `f_procesa`, `ruta_img`, `observacion`, `estado`) VALUES
(8, 1, 'Venta de crema Nutz', 'Nutz', '350,00', 1, '2020-10-13 00:18:50', 1, '2020-10-13 00:27:14', 'view/img/negocios/0952806339/521.jpg', 'Acepto negocio (prueba 3)', 10),
(9, 2, 'Alquiler de proyectores', 'Proyector', '4,50', 1, '2020-10-13 00:21:10', 1, '2020-10-13 00:26:57', 'view/img/negocios/0952806339/885.jpg', 'Acepto negocio (prueba 2)', 10),
(10, 3, 'Cursos de Inglés', 'Inglés', '5,25', 1, '2020-10-13 00:22:18', 1, '2020-10-13 00:26:14', 'view/img/negocios/0952806339/592.jpg', 'Acepto negocio (prueba 1)', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_reuniones`
--

CREATE TABLE `adm_reuniones` (
  `id` int(11) NOT NULL,
  `id_reunion` int(11) NOT NULL,
  `url` varchar(100) NOT NULL,
  `codigo_acceso` varchar(10) NOT NULL DEFAULT '',
  `fecha_inicio` datetime NOT NULL,
  `tema` text NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_modifica` int(11) NOT NULL DEFAULT '0',
  `f_modifica` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` int(11) NOT NULL DEFAULT '10'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `adm_reuniones`
--

INSERT INTO `adm_reuniones` (`id`, `id_reunion`, `url`, `codigo_acceso`, `fecha_inicio`, `tema`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`) VALUES
(8, 2147483647, 'https://us04web.zoom.us/j/71567298757?pwd=ekNaYTQxL204bHFnMktubHhtaXIxUT09', 'Webos1', '2020-10-14 15:00:00', 'Esto Es Un Temita xD', 2, '2020-10-13 18:48:09', 0, '2020-10-13 18:48:09', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_tipo_publicacion`
--

CREATE TABLE `adm_tipo_publicacion` (
  `id` int(11) NOT NULL,
  `tipo_publicacion` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `adm_tipo_publicacion`
--

INSERT INTO `adm_tipo_publicacion` (`id`, `tipo_publicacion`) VALUES
(1, 'Actividades'),
(2, 'Reservas'),
(3, 'Negocio'),
(4, 'Buzon'),
(5, 'Reunión');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adm_usuarios`
--

CREATE TABLE `adm_usuarios` (
  `id` int(11) NOT NULL,
  `cedula` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `n_edificio` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `n_habitacion` int(3) NOT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL,
  `u_modifica` int(11) DEFAULT '0',
  `f_modifica` datetime DEFAULT '1999-01-01 00:00:00',
  `estado` int(11) NOT NULL DEFAULT '1',
  `foto` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `adm_usuarios`
--

INSERT INTO `adm_usuarios` (`id`, `cedula`, `nombre`, `apellidos`, `n_edificio`, `n_habitacion`, `telefono`, `correo`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`, `foto`) VALUES
(1, '0952806339', 'Antony', 'Canga Ortega', 'B2A', 5, '0962799286', 'antonycanorte@gmail.com', 1, '2020-08-01 00:00:00', 2, '2020-10-07 13:49:49', 1, 'view/img/usuarios/0952806339/636.png'),
(2, '0931130363', 'Gonzalo', 'Muñoz García', 'B1B', 5, '0990318933', 'munioz.gonzalog@gmail.com', 1, '2020-08-01 00:00:00', 22, '2020-09-25 23:45:36', 1, 'view/img/usuarios/0931130363/467.png'),
(3, '1729747715', 'Paula', 'Dora Bonilla', 'B2A', 1, '0967637717', 'pauladorabonilla@gmail.com', 1, '2020-08-24 01:28:50', 22, '2020-09-16 15:51:31', 1, 'view/img/usuarios/1729747715/730.jpg'),
(19, '0953352242', 'Ailyn ', 'Bones Flores', 'H46', 8, '042470001', 'ailyn@gmail.com', 2, '2020-09-14 14:59:07', 2, '2020-10-07 13:50:18', 1, 'view/img/usuarios/0953352242/787.png'),
(22, '0123456789', 'Usuario', 'Super Admin', 'XXX', 0, '000000000', 'admin@info.com', 19, '2020-09-14 15:21:23', 0, '1999-01-01 00:00:00', 1, 'view/img/usuarios/0123456789/851.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_estado`
--

CREATE TABLE `gen_estado` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `gen_estado`
--

INSERT INTO `gen_estado` (`id`, `descripcion`) VALUES
(1, 'Activo'),
(2, 'En proceso'),
(3, 'Desactivado'),
(4, 'Cancelado'),
(5, 'Rechazado'),
(6, 'Histórico'),
(7, 'Exitoso'),
(8, 'Fallido'),
(9, 'Bloqueado'),
(10, 'Publicado'),
(11, 'Eliminado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gen_login`
--

CREATE TABLE `gen_login` (
  `id` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `cedula` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `ip_ingreso` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `f_ingreso` datetime NOT NULL,
  `observacion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `idestado` int(11) NOT NULL,
  `n_intentos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `gen_login`
--

INSERT INTO `gen_login` (`id`, `id_rol`, `cedula`, `ip_ingreso`, `f_ingreso`, `observacion`, `idestado`, `n_intentos`) VALUES
(1, 1, '0952806339', '::1', '2020-08-16 23:46:39', 'Exitoso', 1, 0),
(2, 1, '0952806339', '::1', '2020-08-16 23:48:06', 'Exitoso', 1, 0),
(3, 1, '0952806339', '::1', '2020-08-16 23:51:53', 'Fallido', 0, 0),
(4, 1, '0952806339', '::1', '2020-08-16 23:52:29', 'Fallido', 0, 0),
(11, 1, '0952806339', '::1', '2020-08-16 23:56:49', 'Exitoso', 1, 0),
(12, 1, '0952806339', '::1', '2020-08-16 23:56:59', 'Fallido', 0, 1),
(13, 1, '0952806339', '::1', '2020-08-17 00:02:31', 'Exitoso', 1, 0),
(14, 1, '0952806339', '::1', '2020-08-17 00:09:19', 'Fallido', 0, 1),
(15, 1, '0952806339', '::1', '2020-08-17 00:09:47', 'Fallido', 0, 2),
(16, 1, '0952806339', '::1', '2020-08-17 00:16:42', 'Exitoso', 1, 0),
(17, 1, '0952806339', '::1', '2020-08-17 18:42:28', 'Intento Exitoso', 1, 0),
(18, 1, '0952806339', '::1', '2020-08-17 18:42:51', 'Intento Fallido', 0, 1),
(21, 1, '0952806339', '::1', '2020-08-17 18:43:59', 'Intento Exitoso', 1, 0),
(22, 1, '0952806339', '::1', '2020-08-17 18:44:41', 'Intento Fallido', 0, 1),
(23, 1, '0952806339', '::1', '2020-08-17 18:44:50', 'Intento Fallido', 0, 2),
(24, 1, '0952806339', '::1', '2020-08-17 18:45:06', 'Intento Exitoso', 1, 0),
(25, 1, '0952806339', '::1', '2020-08-17 18:45:23', 'Intento Fallido', 0, 1),
(26, 1, '0952806339', '::1', '2020-08-17 18:45:31', 'Intento Exitoso', 1, 0),
(27, 1, '0123456789', '::1', '2020-09-25 19:03:25', 'Intento Exitoso', 7, 0),
(28, 1, '0931130363', '::1', '2020-09-25 23:44:58', 'Intento Fallido', 8, 1),
(29, 1, '0123456789', '::1', '2020-09-25 23:45:07', 'Intento Exitoso', 7, 0),
(30, 1, '0931130363', '::1', '2020-09-25 23:45:54', 'Intento Exitoso', 7, 0),
(31, 1, '0931130363', '::1', '2020-09-25 23:51:00', 'Intento Exitoso', 7, 0),
(32, 1, '0931130363', '::1', '2020-09-26 10:13:06', 'Intento Exitoso', 7, 0),
(33, 1, '0931130363', '::1', '2020-09-28 21:48:35', 'Intento Exitoso', 7, 0),
(34, 1, '0931130363', '::1', '2020-10-06 11:25:10', 'Intento Exitoso', 7, 0),
(35, 1, '0931130363', '::1', '2020-10-06 11:44:05', 'Intento Exitoso', 7, 0),
(36, 1, '0931130363', '::1', '2020-10-07 13:46:50', 'Intento Exitoso', 7, 0),
(37, 2, '0953352242', '::1', '2020-10-07 13:51:01', 'Intento Exitoso', 7, 0),
(38, 1, '0931130363', '::1', '2020-10-07 14:05:40', 'Intento Exitoso', 7, 0),
(39, 2, '0953352242', '::1', '2020-10-07 14:07:23', 'Intento Exitoso', 7, 0),
(40, 1, '0931130363', '::1', '2020-10-07 14:07:32', 'Intento Exitoso', 7, 0),
(41, 1, '0931130363', '::1', '2020-10-08 11:30:20', 'Intento Exitoso', 7, 0),
(42, 1, '0931130363', '::1', '2020-10-12 08:56:54', 'Intento Exitoso', 7, 0),
(43, 2, '0953352242', '::1', '2020-10-12 11:56:12', 'Intento Exitoso', 7, 0),
(44, 1, '0931130363', '::1', '2020-10-12 11:56:46', 'Intento Exitoso', 7, 0),
(45, 2, '0953352242', '::1', '2020-10-12 11:58:43', 'Intento Exitoso', 7, 0),
(46, 1, '0931130363', '::1', '2020-10-12 11:58:54', 'Intento Exitoso', 7, 0),
(47, 1, '0952806339', '::1', '2020-10-12 22:03:45', 'Intento Exitoso', 7, 0),
(48, 1, '0931130363', '::1', '2020-10-13 18:24:22', 'Intento Exitoso', 7, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seg_alerta`
--

CREATE TABLE `seg_alerta` (
  `id` int(11) NOT NULL,
  `cedula` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `detalle` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `f_crea` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `seg_alerta`
--

INSERT INTO `seg_alerta` (`id`, `cedula`, `detalle`, `ip`, `f_crea`) VALUES
(9, '0931130363', 'Informa que se ha presenciado a una o varias personas peligrosas en los alrededores del condominio,\r\n         se pide tener cuidado y estar atento ante las situaciones que podr&iacute;an presentarse', '::1', '2020-08-12 00:15:40'),
(10, '0931130363', 'Informa que se ha presenciado a una o varias personas peligrosas en los alrededores del condominio,\r\n         se pide tener cuidado y estar atento ante las situaciones que podr&iacute;an presentarse', '::1', '2020-08-12 00:16:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seg_perfiles`
--

CREATE TABLE `seg_perfiles` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL,
  `u_modifica` int(11) NOT NULL,
  `f_modifica` datetime NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `seg_perfiles`
--

INSERT INTO `seg_perfiles` (`id`, `descripcion`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`) VALUES
(1, 'Administrador', 1, '2020-08-01 10:00:00', 1, '2020-08-01 10:00:00', 1),
(2, 'Colaborador', 1, '2020-08-01 10:00:00', 1, '2020-08-01 10:00:00', 1),
(3, 'Seguridad', 1, '2020-08-01 10:01:00', 1, '2020-08-01 10:01:00', 1),
(4, 'Inquilino', 1, '2020-08-01 10:01:00', 1, '2020-08-01 10:01:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seg_rol`
--

CREATE TABLE `seg_rol` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(15) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `seg_rol`
--

INSERT INTO `seg_rol` (`id`, `descripcion`) VALUES
(1, 'Administrativo'),
(2, 'Inquilino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seg_usuario`
--

CREATE TABLE `seg_usuario` (
  `cedula` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `num_seg` varchar(3) COLLATE utf8_spanish_ci NOT NULL,
  `confirmacion` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL,
  `u_modifica` int(11) NOT NULL DEFAULT '0',
  `f_modifica` datetime NOT NULL DEFAULT '1999-01-01 00:00:00',
  `estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `seg_usuario`
--

INSERT INTO `seg_usuario` (`cedula`, `clave`, `num_seg`, `confirmacion`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`) VALUES
('0123456789', '$2a$07$usesomesillystringforegFOeQOp8RK/V8Yn0LZIZwSlh5IkndD.', '600', 'N', 1, '2020-09-14 15:21:23', 0, '1999-01-01 00:00:00', 1),
('0931130363', '$2a$07$usesomesillystringfores.79oDL.a4zuwVec9LIxd5sYmqRGkPq', 'G63', 'N', 1, '2020-08-01 00:00:00', 22, '2020-09-25 23:45:35', 1),
('0952806339', '$2a$07$usesomesillystringforegFOeQOp8RK/V8Yn0LZIZwSlh5IkndD.', 'A39', 'N', 1, '2020-08-01 10:02:00', 2, '2020-10-07 13:49:49', 1),
('0953352242', '$2a$07$usesomesillystringfores.79oDL.a4zuwVec9LIxd5sYmqRGkPq', 'A42', 'N', 2, '2020-09-14 14:59:06', 2, '2020-10-07 13:50:18', 1),
('1729747715', '$2a$07$usesomesillystringforegFOeQOp8RK/V8Yn0LZIZwSlh5IkndD.', 'P15', 'N', 1, '2020-08-24 01:26:55', 2, '2020-10-07 15:35:03', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seg_usuario_perfil`
--

CREATE TABLE `seg_usuario_perfil` (
  `id` int(11) NOT NULL,
  `id_user_rol` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  `f_desde` datetime NOT NULL,
  `f_hasta` datetime NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL,
  `u_modifica` int(11) NOT NULL,
  `f_modifica` datetime NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `seg_usuario_perfil`
--

INSERT INTO `seg_usuario_perfil` (`id`, `id_user_rol`, `id_perfil`, `f_desde`, `f_hasta`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`) VALUES
(1, 1, 1, '2020-08-08 00:00:00', '2021-01-09 00:00:00', 1, '2020-08-08 00:00:00', 1, '2020-08-08 00:00:00', 1),
(2, 2, 2, '2020-08-08 00:00:00', '2020-12-05 00:00:00', 1, '2020-08-08 00:00:00', 1, '2020-08-08 00:00:00', 1),
(3, 3, 4, '2020-08-24 01:32:19', '0000-00-00 00:00:00', 1, '2020-08-24 01:32:19', 1, '2020-08-24 01:32:19', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seg_usuario_rol`
--

CREATE TABLE `seg_usuario_rol` (
  `id` int(11) NOT NULL,
  `cedula` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `id_rol` int(11) NOT NULL,
  `u_crea` int(11) NOT NULL,
  `f_crea` datetime NOT NULL,
  `u_modifica` int(11) NOT NULL DEFAULT '0',
  `f_modifica` datetime NOT NULL DEFAULT '1999-01-01 00:00:00',
  `estado` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `seg_usuario_rol`
--

INSERT INTO `seg_usuario_rol` (`id`, `cedula`, `id_rol`, `u_crea`, `f_crea`, `u_modifica`, `f_modifica`, `estado`) VALUES
(1, '0952806339', 1, 1, '2020-08-01 00:00:00', 2, '2020-10-07 13:49:49', 1),
(2, '0931130363', 1, 1, '2020-08-01 00:00:00', 22, '2020-09-25 23:45:36', 1),
(3, '1729747715', 2, 1, '2020-08-24 01:31:34', 2, '2020-10-07 15:35:03', 1),
(4, '0953352242', 2, 2, '2020-09-14 14:59:06', 2, '2020-10-07 13:50:18', 1),
(7, '0123456789', 1, 19, '2020-09-14 15:21:23', 0, '1999-01-01 00:00:00', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adm_actividades`
--
ALTER TABLE `adm_actividades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admactividades_admusuarios_ucrea` (`u_crea`);

--
-- Indices de la tabla `adm_buzon`
--
ALTER TABLE `adm_buzon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admbuzon_admusuarios_ucrea` (`u_crea`);

--
-- Indices de la tabla `adm_canchas`
--
ALTER TABLE `adm_canchas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admcanchas_admusuarios_ucrea` (`u_crea`);

--
-- Indices de la tabla `adm_invitado`
--
ALTER TABLE `adm_invitado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_negocios`
--
ALTER TABLE `adm_negocios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admnegocios_admusuarios_ucrea` (`u_crea`);

--
-- Indices de la tabla `adm_noticia`
--
ALTER TABLE `adm_noticia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_registro_act`
--
ALTER TABLE `adm_registro_act`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admregistroactividad_admactividades_idactividad` (`id_actividad`),
  ADD KEY `fk_admregistroact_admusuarios_ucrea` (`u_crea`);

--
-- Indices de la tabla `adm_registro_cancha`
--
ALTER TABLE `adm_registro_cancha`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admregistrocancha_admcanchas_idcancha` (`id_cancha`),
  ADD KEY `fk_admregistrocancha_admusuarios_ucrea` (`u_crea`);

--
-- Indices de la tabla `adm_registro_neg`
--
ALTER TABLE `adm_registro_neg`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admregistroneg_admusuarios_ucrea` (`u_crea`),
  ADD KEY `fk_admregistroneg_admnegocios_idnegocio` (`id_negocio`);

--
-- Indices de la tabla `adm_reuniones`
--
ALTER TABLE `adm_reuniones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_tipo_publicacion`
--
ALTER TABLE `adm_tipo_publicacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `adm_usuarios`
--
ALTER TABLE `adm_usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_admusuarios_segusuario_cedula` (`cedula`);

--
-- Indices de la tabla `gen_estado`
--
ALTER TABLE `gen_estado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gen_login`
--
ALTER TABLE `gen_login`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seg_alerta`
--
ALTER TABLE `seg_alerta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_segpanico_segusuario_cedula` (`cedula`);

--
-- Indices de la tabla `seg_perfiles`
--
ALTER TABLE `seg_perfiles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seg_rol`
--
ALTER TABLE `seg_rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seg_usuario`
--
ALTER TABLE `seg_usuario`
  ADD PRIMARY KEY (`cedula`);

--
-- Indices de la tabla `seg_usuario_perfil`
--
ALTER TABLE `seg_usuario_perfil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_segusuarioperfil_segusuariorol_iduserrol` (`id_user_rol`),
  ADD KEY `fk_segusuarioperfil_segperfiles_idperfil` (`id_perfil`);

--
-- Indices de la tabla `seg_usuario_rol`
--
ALTER TABLE `seg_usuario_rol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_segusuariorol_segrol` (`id_rol`),
  ADD KEY `fk_segusuariorol_segusuario_cedula` (`cedula`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adm_actividades`
--
ALTER TABLE `adm_actividades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `adm_buzon`
--
ALTER TABLE `adm_buzon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `adm_canchas`
--
ALTER TABLE `adm_canchas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `adm_invitado`
--
ALTER TABLE `adm_invitado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `adm_negocios`
--
ALTER TABLE `adm_negocios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `adm_noticia`
--
ALTER TABLE `adm_noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `adm_registro_act`
--
ALTER TABLE `adm_registro_act`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `adm_registro_cancha`
--
ALTER TABLE `adm_registro_cancha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `adm_registro_neg`
--
ALTER TABLE `adm_registro_neg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `adm_reuniones`
--
ALTER TABLE `adm_reuniones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `adm_tipo_publicacion`
--
ALTER TABLE `adm_tipo_publicacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `adm_usuarios`
--
ALTER TABLE `adm_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `gen_estado`
--
ALTER TABLE `gen_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `gen_login`
--
ALTER TABLE `gen_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT de la tabla `seg_alerta`
--
ALTER TABLE `seg_alerta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `seg_perfiles`
--
ALTER TABLE `seg_perfiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `seg_rol`
--
ALTER TABLE `seg_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `seg_usuario_perfil`
--
ALTER TABLE `seg_usuario_perfil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `seg_usuario_rol`
--
ALTER TABLE `seg_usuario_rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `adm_actividades`
--
ALTER TABLE `adm_actividades`
  ADD CONSTRAINT `fk_admactividades_admusuarios_ucrea` FOREIGN KEY (`u_crea`) REFERENCES `adm_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `adm_buzon`
--
ALTER TABLE `adm_buzon`
  ADD CONSTRAINT `fk_admbuzon_admusuarios_ucrea` FOREIGN KEY (`u_crea`) REFERENCES `adm_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `adm_canchas`
--
ALTER TABLE `adm_canchas`
  ADD CONSTRAINT `fk_admcanchas_admusuarios_ucrea` FOREIGN KEY (`u_crea`) REFERENCES `adm_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `adm_negocios`
--
ALTER TABLE `adm_negocios`
  ADD CONSTRAINT `fk_admnegocios_admusuarios_ucrea` FOREIGN KEY (`u_crea`) REFERENCES `adm_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `adm_registro_act`
--
ALTER TABLE `adm_registro_act`
  ADD CONSTRAINT `fk_admregistroact_admusuarios_ucrea` FOREIGN KEY (`u_crea`) REFERENCES `adm_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_admregistroactividad_admactividades_idactividad` FOREIGN KEY (`id_actividad`) REFERENCES `adm_actividades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `adm_registro_cancha`
--
ALTER TABLE `adm_registro_cancha`
  ADD CONSTRAINT `fk_admregistrocancha_admcanchas_idcancha` FOREIGN KEY (`id_cancha`) REFERENCES `adm_canchas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_admregistrocancha_admusuarios_ucrea` FOREIGN KEY (`u_crea`) REFERENCES `adm_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `adm_registro_neg`
--
ALTER TABLE `adm_registro_neg`
  ADD CONSTRAINT `fk_admregistroneg_admnegocios_idnegocio` FOREIGN KEY (`id_negocio`) REFERENCES `adm_negocios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_admregistroneg_admusuarios_ucrea` FOREIGN KEY (`u_crea`) REFERENCES `adm_usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `adm_usuarios`
--
ALTER TABLE `adm_usuarios`
  ADD CONSTRAINT `fk_admusuarios_segusuario_cedula` FOREIGN KEY (`cedula`) REFERENCES `seg_usuario` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `seg_alerta`
--
ALTER TABLE `seg_alerta`
  ADD CONSTRAINT `fk_segpanico_segusuario_cedula` FOREIGN KEY (`cedula`) REFERENCES `seg_usuario` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `seg_usuario_perfil`
--
ALTER TABLE `seg_usuario_perfil`
  ADD CONSTRAINT `fk_segusuarioperfil_segperfiles_idperfil` FOREIGN KEY (`id_perfil`) REFERENCES `seg_perfiles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_segusuarioperfil_segusuariorol_iduserrol` FOREIGN KEY (`id_user_rol`) REFERENCES `seg_usuario_rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `seg_usuario_rol`
--
ALTER TABLE `seg_usuario_rol`
  ADD CONSTRAINT `fk_segusuariorol_segrol_idrol` FOREIGN KEY (`id_rol`) REFERENCES `seg_rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_segusuariorol_segusuario_cedula` FOREIGN KEY (`cedula`) REFERENCES `seg_usuario` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
