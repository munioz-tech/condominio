<?php
require_once "../models/usuarios_modelo.php";
require_once "../controllers/actividades_controlador.php";
require_once "../models/actividades_modelo.php";
require_once "../controllers/actividades_controlador.php";
require_once "../models/actividades_modelo.php";
require_once "../controllers/negocios_controlador.php";
require_once "../models/negocios_modelo.php";
require_once "../controllers/buzon_controlador.php";
require_once "../models/buzon_modelo.php";
require_once "../models/publicaciones_modelo.php";
require_once "../controllers/publicaciones_controlador.php";
require_once "../controllers/reportes_controlador.php";
require_once "../controllers/reservas_controlador.php";
require_once "../models/reservas_modelo.php";
require_once "../models/reportes_modelo.php";

class AjaxUtils{ 
/*====================================
        TABLAS REPORTES
 ====================================*/
  public function ajaxPintarTablaInvitados(){
    $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablas">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Cedula</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Telefono</th>
                  <th>Correo</th>
                  <th>Ip</th>
                  <th>Creado</th>
                </tr> </thead> <tbody>';
    $alertas = ModeloUsuarios::mdlConsultarInvitados();
    $contador = 1;
    foreach($alertas as $value){
      $HTML.= '<tr>
                <td>'.$contador.'</td>
                <td>'.$value["cedula"].'</td>
                <td>'.$value["nombre"].'</td>
                <td>'.$value["apellidos"].'</td>
                <td>'.$value["telefono"].'</td>
                <td>'.$value["correo"].'</td>
                <td>'.$value["ip"].'</td>
                <td>'.date("d-m-Y H:i",strtotime($value["f_ingreso"])).'</td>
              </tr>';
      $contador++;
    }   
    $HTML.= '</tbody> </table>';
    echo json_encode($HTML);
  }

  public function ajaxPintarTablaLogin(){
    $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablas">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Cedula</th>
                  <th>Ip</th>
                  <th>Creado</th>
                  <th>Observación</th>
                  <th>Intentos</th>
                </tr> </thead> <tbody>';
    $id_rol = "";
    $cedula = "";
    $alertas = ModeloUsuarios::mdlConsultarIntentosLogin($id_rol,$cedula);
    $contador = 1;
    foreach($alertas as $value){   
      $HTML.= '<tr>
                <td>'.$contador.'</td>
                <td>'.$value["cedula"].'</td>
                <td>'.$value["ip_ingreso"].'</td>
                <td>'.date("d-m-Y H:i",strtotime($value["f_ingreso"])).'</td>
                <td>'.$value["observacion"].'</td>
                <td>'.$value["n_intentos"].'</td>
              </tr>';
      $contador++;
    }   
    $HTML.= '</tbody> </table>';
    echo json_encode($HTML);
  }

  public function ajaxPintarTablaAlertas(){
    $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablas">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Cedula</th>
                  <th>Detalle</th>
                  <th>Ip</th>
                  <th>Creado</th>
                </tr> </thead> <tbody>';
    $cedula = "";
    $alertas = ModeloUsuarios::mdlConsultarAlertasUsuario($cedula);
    $contador = 1;
    foreach($alertas as $value){   
      $HTML.= '<tr>
                <td>'.$contador.'</td>
                <td>'.$value["cedula"].'</td>
                <td>'.$value["detalle"].'</td>
                <td>'.$value["ip"].'</td>
                <td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
              </tr>';
    $contador++;
    }   
    $HTML.= '</tbody> </table>';
    echo json_encode($HTML);
  }

  public function ajaxPintarTablaActividades(){
    $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablas">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Cedula</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Actividad</th>
                  <th>Tipo</th>
                  <th>Detalle</th>
                  <th>Observación</th>
                  <th>Creado</th>
                </tr> </thead> <tbody>';
    $tablas = "ADM_REGISTRO_ACT ARA, ADM_USUARIOS AU, ADM_ACTIVIDADES AA";
    $actividades = ModeloReporte::mdlConsultarReporte($tablas, 4);
    $contador = 1;
    foreach($actividades as $value){
    $HTML.= '<tr>
              <td>'.$contador.'</td>
              <td>'.$value["CEDULA"].'</td>
              <td>'.$value["NOMBRE"].'</td>
              <td>'.$value["APELLIDOS"].'</td>';
              if($value["ID_ACT_PADRE"] == 1){$HTML.='<td>Mantenimiento</td>';}
              elseif($value["ID_ACT_PADRE"] == 2){$HTML.='<td>Extravío</td>';}
              elseif($value["ID_ACT_PADRE"] == 3){$HTML.='<td>Varios</td>';}
              $HTML.='<td>'.$value["TIPO_ACTIVIDAD"].'</td>
              <td>'.$value["DESCRIPCION"].'</td>
              <td>'.$value["OBSERVACION"].'</td>
              <td>'.date("d-m-Y H:i",strtotime($value["F_CREA"])).'</td>
            </tr>';
    $contador++;
    }   
    $HTML.= '</tbody> </table>';
    echo json_encode($HTML);
  }

  public function ajaxPintarTablaBuzón(){
    $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablas">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Cedula</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Tipo de Buzón</th>
                  <th>Detalle</th>
                  <th>Observación</th>
                  <th>Creado</th>
                </tr> </thead> <tbody>';
    $tablas = "ADM_BUZON AB, ADM_USUARIOS AU";
    $buzon = ModeloReporte::mdlConsultarReporte($tablas, 5);
    $contador = 1;
    foreach($buzon as $value){
      $HTML.= '<tr>
              <td>'.$contador.'</td>
              <td>'.$value["CEDULA"].'</td>
              <td>'.$value["NOMBRE"].'</td>
              <td>'.$value["APELLIDOS"].'</td>';
              if($value["QUEJA_IDEA"] == 1){$HTML.='<td>Queja</td>';}
              elseif($value["QUEJA_IDEA"] == 2){$HTML.='<td>Idea</td>';}
              $HTML.='<td>'.$value["DESCRIPCION"].'</td>
              <td>'.$value["OBSERVACION"].'</td>
              <td>'.date("d-m-Y H:i",strtotime($value["F_CREA"])).'</td>
            </tr>';
      $contador++;
    }   
    $HTML.= '</tbody> </table>';
    echo json_encode($HTML);
  }

  public function ajaxPintarTablaNegocios(){
    $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablas">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Cedula</th>
                  <th>Nombre</th>
                  <th>Apellidos</th>
                  <th>Negocio</th>
                  <th>Precio</th>
                  <th>Detalle</th>
                  <th>Observación</th>
                  <th>Creado</th>
                </tr> </thead> <tbody>';
    $tablas = "ADM_REGISTRO_NEG ARN, ADM_USUARIOS AU, ADM_NEGOCIOS AN";
    $actividades = ModeloReporte::mdlConsultarReporte($tablas, 6);
    $contador = 1;
    foreach($actividades as $value){
    $HTML.= '<tr>
              <td>'.$contador.'</td>
              <td>'.$value["CEDULA"].'</td>
              <td>'.$value["NOMBRE"].'</td>
              <td>'.$value["APELLIDOS"].'</td>';
              if($value["ID_NEGOCIO"] == 1){$HTML.='<td>'.$value["TIPO_NEGOCIO"].' de: '.$value["T_NEGOCIO"].'</td>';}
              elseif($value["ID_NEGOCIO"] == 2){$HTML.='<td>'.$value["TIPO_NEGOCIO"].' de: '.$value["T_NEGOCIO"].'</td>';}
              elseif($value["ID_NEGOCIO"] == 3){$HTML.='<td>'.$value["TIPO_NEGOCIO"].' de: '.$value["T_NEGOCIO"].'</td>';}
              $HTML.='<td>'.$value["PRECIO"].'</td>
              <td>'.$value["DESCRIPCION"].'</td>
              <td>'.$value["OBSERVACION"].'</td>
              <td>'.date("d-m-Y H:i",strtotime($value["F_CREA"])).'</td>
            </tr>';
      $contador++;
    }   
    $HTML.= '</tbody> </table>';
    echo json_encode($HTML);
  }

  public function ajaxPintarTablaReservas(){
    $HTML = '<table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablas">
    <thead>
      <tr>
        <th style="width: 10px">#</th>
        <th>Cedula</th>
        <th>Nombre</th>
        <th>Apellidos</th>
        <th>Cancha</th>
        <th>Desde</th>
        <th>Hasta</th>
        <th>Detalle</th>
        <th>Observación</th>
        <th>Creado</th>
      </tr> </thead> <tbody>';
    $tablas = "ADM_REGISTRO_CANCHA ARC, ADM_USUARIOS AU, ADM_CANCHAS AC";
    $actividades = ModeloReporte::mdlConsultarReporte($tablas, 7);
    $contador = 1;
    foreach($actividades as $value){
    $HTML.= '<tr>
        <td>'.$contador.'</td>
        <td>'.$value["CEDULA"].'</td>
        <td>'.$value["NOMBRE"].'</td>
        <td>'.$value["APELLIDOS"].'</td>';
        if($value["ID_CANCHA"] == 1){$HTML.='<td>'.$value["CANCHA"].'</td>';}
        if($value["ID_CANCHA"] == 2){$HTML.='<td>'.$value["CANCHA"].'</td>';}
        if($value["ID_CANCHA"] == 3){$HTML.='<td>'.$value["CANCHA"].'</td>';}
        if($value["ID_CANCHA"] == 4){$HTML.='<td>'.$value["CANCHA"].'</td>';}
        if($value["ID_CANCHA"] == 5){$HTML.='<td>'.$value["CANCHA"].'</td>';}
        $HTML.='<td>'.$value["FYH_DESDE"].'</td>
        <td>'.$value["FYH_HASTA"].'</td>
        <td>'.$value["DESCRIPCION"].'</td>
        <td>'.$value["OBSERVACION"].'</td>
        <td>'.date("d-m-Y H:i",strtotime($value["F_CREA"])).'</td>
      </tr>';
      $contador++;
    }   
    $HTML.= '</tbody> </table>';
    echo json_encode($HTML);
  }
/*====================================
            PUBLICACIONES
====================================*/
  public $valPublicaciones; 
  public function ajaxMostrarPublicaciones(){
      $valPublicacion = $this -> valPublicaciones;
      $publicacion = ControladorPublicacion::ctrConsultarPublicaciones($valPublicacion);
      $contador = 1;
      if($valPublicacion == 1){
      $HTML = '<div class="card-body">
                <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablaMostrarPublicacion">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Cedula</th>
                      <th>Nombre</th>
                      <th>Fecha</th>
                      <th>Descripción</th>
                      <th>Procesar</th>
                    </tr>
                  </thead>
                  <tbody>';
        foreach($publicacion as $rows[]=>$value){
          if($value["estado"] == 2){
            $HTML.= '<tr>
                      <td>'.$contador.'</td>
                      <td>'.$value["cedula"].'</td>
                      <td>'.$value["nombre"]." ".$value["apellidos"].'</td>
                      <td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
                      <td>
                        <div class="btn-group">
                          <button class="btn bg-maroon btnPublicacion" valPublica="'.$valPublicacion.'" idPublicacion="'.$value["id"].'" data-toggle="modal" data-target="#modalPublicarAct">Ver Actividad</button>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group">
                          <button class="btn btn-primary btnProcesar" valPublica="'.$valPublicacion.'" idProcesar="'.$value["id"].'" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
                        </div>
                      </td>
                    </tr>';
                    }$contador++;
                  }   
        $HTML.= '</tbody>
              </table>
            </div>';
        echo json_encode($HTML);
      }
      if($valPublicacion == 2){
      $HTML = '<div class="card-body">
                <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablaMostrarPublicacion">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Cedula</th>
                      <th>Nombre</th>
                      <th>Fecha</th>
                      <th>Descripción</th>
                      <th>Procesar</th>
                    </tr>
                  </thead>
                  <tbody>';
        foreach($publicacion as $rows[]=>$value){
          if($value["estado"] == 2){
            $HTML.= '<tr>
                      <td>'.$contador.'</td>
                      <td>'.$value["cedula"].'</td>
                      <td>'.$value["nombre"]." ".$value["apellidos"].'</td>
                      <td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
                      <td>
                        <div class="btn-group">
                          <button class="btn bg-maroon btnPublicacion" valPublica="'.$valPublicacion.'" idPublicacion="'.$value["id"].'" data-toggle="modal" data-target="#modalPublicarNeg">Ver Negocio</button>
                        </div>
                      </td>
                      <td>
                        <div class="btn-group">
                          <button class="btn btn-primary btnProcesar" valPublica="'.$valPublicacion.'" idProcesar="'.$value["id"].'" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
                        </div>
                      </td>
                    </tr>';
                    }$contador++;
                  }   
        $HTML.= '</tbody>
              </table>
            </div>';
        echo json_encode($HTML);
      }
      if($valPublicacion == 3){
        $HTML = '<div class="card-body">
                  <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablaMostrarPublicacion">
                    <thead>
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Cedula</th>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Descripción</th>
                        <th>Procesar</th>
                      </tr>
                    </thead>
                    <tbody>';
          foreach($publicacion as $rows[]=>$value){
            if($value["estado"] == 2){
              $HTML.= '<tr>
                        <td>'.$contador.'</td>
                        <td>'.$value["cedula"].'</td>
                        <td>'.$value["nombre"]." ".$value["apellidos"].'</td>
                        <td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn bg-maroon btnPublicacion" valPublica="'.$valPublicacion.'" idPublicacion="'.$value["id"].'" data-toggle="modal" data-target="#modalPublicarBuz">Buzon</button>
                          </div>
                        </td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-primary btnProcesar" valPublica="'.$valPublicacion.'" idProcesar="'.$value["id"].'" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
                          </div>
                        </td>
                      </tr>';
                      }$contador++;
                    }   
          $HTML.= '</tbody>
                </table>
              </div>';
          echo json_encode($HTML);
      }
      if($valPublicacion == 4){
        $HTML = '<div class="card-body">
                  <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto oculta" id="tablaMostrarPublicacion">
                    <thead>
                      <tr>
                        <th style="width: 10px">#</th>
                        <th>Cedula</th>
                        <th>Nombre</th>
                        <th>Fecha</th>
                        <th>Descripción</th>
                        <th>Procesar</th>
                      </tr>
                    </thead>
                    <tbody>';
          foreach($publicacion as $rows[]=>$value){
            if($value["estado"] == 2){
              $HTML.= '<tr>
                        <td>'.$contador.'</td>
                        <td>'.$value["cedula"].'</td>
                        <td>'.$value["nombre"]." ".$value["apellidos"].'</td>
                        <td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn bg-maroon btnPublicacion" valPublica="'.$valPublicacion.'" idPublicacion="'.$value["id"].'" data-toggle="modal" data-target="#modalPublicarRes">Ver Reserva</button>
                          </div>
                        </td>
                        <td>
                          <div class="btn-group">
                            <button class="btn btn-primary btnProcesar" valPublica="'.$valPublicacion.'" idProcesar="'.$value["id"].'" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
                          </div>
                        </td>
                      </tr>';
                      }$contador++;
                    }   
          $HTML.= '</tbody>
                </table>
              </div>';
          echo json_encode($HTML);
        }
  }
}

/*====================================
        PINTAR REPORTE ALERTAS
 ====================================*/
 if (isset($_POST["valReporte"])){
  if ($_POST["valReporte"] == 1){
    $reporte = new AjaxUtils();
    $reporte -> ajaxPintarTablaInvitados();
  }
  if ($_POST["valReporte"] == 2){
    $reporte = new AjaxUtils();
    $reporte -> ajaxPintarTablaLogin();
  } 
  if ($_POST["valReporte"] == 3){
    $reporte = new AjaxUtils();
    $reporte -> ajaxPintarTablaAlertas();
  }
  if ($_POST["valReporte"] == 4){
    $reporte = new AjaxUtils();
    $reporte -> ajaxPintarTablaActividades();
  }
  if ($_POST["valReporte"] == 5){
    $reporte = new AjaxUtils();
    $reporte -> ajaxPintarTablaBuzón();
  } 
  if ($_POST["valReporte"] == 6){
    $reporte = new AjaxUtils();
    $reporte -> ajaxPintarTablaNegocios();
  }
  if ($_POST["valReporte"] == 7){
    $reporte = new AjaxUtils();
    $reporte -> ajaxPintarTablaReservas();
  }
}

/*====================================
        MOSTRAR PUBLICACIONES
 ====================================*/
if (isset($_POST["valPublicacion"])){
  $publicacionAjax = new AjaxUtils();
  $publicacionAjax -> valPublicaciones = $_POST["valPublicacion"];
  $publicacionAjax -> ajaxMostrarPublicaciones();
}