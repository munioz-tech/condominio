<?php
require_once "../models/actividades_modelo.php";
require_once "../controllers/actividades_controlador.php";

class AjaxActividad{ 
    public $idPadre;
    public $idHijo;
    public $idActividad;
    public function ajaxConsultarActividad(){
        $idPadre = $this->idPadre;
        $tabla = "ADM_ACTIVIDADES";
        $item = "id_padre";
        $respuesta = ModeloActividades::mdlObtenerActividades($tabla,$item,$idPadre);
        $HTML = '<option value="0">Seleccione una Actividad</option>';
        foreach($respuesta as $row){$HTML.= '<option value="'.$row['id'].'">'.$row['tipo_actividad'].'</option>';}
        echo json_encode($HTML);
    }

    public function ajaxConsultarActividadHijo(){
        $idHijo = $this->idHijo;
        $tabla = "ADM_ACTIVIDADES";
        $item = "id_padre";
        $respuesta = ModeloActividades::mdlObtenerActividades($tabla,$item,$idHijo);
        echo json_encode($respuesta);
    }

    public function ajaxEditarActividad(){
        $item = "ID";
        $valor = $this->idActividad;
        $respuesta = ControladorActividades::ctrMostrarActividad($item,$valor);
        echo json_encode($respuesta);
    }
}

/*====================================
    LLAMADA DE ACTIVIDADES HIJAS
 ====================================*/
if (isset($_POST["idPadre"])){
    $actividad = new AjaxActividad();
    $actividad -> idPadre = $_POST["idPadre"];
    $actividad ->ajaxConsultarActividad();
}

if (isset($_POST["idHijo"])){
    $actividad = new AjaxActividad();
    $actividad -> idHijo = $_POST["idHijo"];
    $actividad ->ajaxConsultarActividadHijo();
}

/*====================================
        EDITAR ACTIVIDADES
 ====================================*/
if (isset($_POST["idActividad"])){
    $actividad = new AjaxActividad();
    $actividad -> idActividad = $_POST["idActividad"];
    $actividad ->ajaxEditarActividad();
}