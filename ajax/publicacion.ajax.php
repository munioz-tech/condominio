<?php
require_once "../models/reportes_modelo.php";
require_once "../models/publicaciones_modelo.php";
require_once "../controllers/publicaciones_controlador.php";

class AjaxPublicacion{ 
    public $valPublicacion;
    public $idPublicacion;
    public $valPublica;
    public $idProcesar;
    public $valPublicar;

    public function ajaxConsultarPublicacion(){
        $valPublicacion = $this->valPublicacion;
        if($valPublicacion == 1){
            $tabla = "adm_registro_act";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla, $valPublicacion);
            echo json_encode($respuesta);
        }
        if($valPublicacion == 2){
            $tabla = "adm_registro_neg";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla, $valPublicacion);
            echo json_encode($respuesta);
        }
        if($valPublicacion == 3){
            $tabla = "adm_buzon";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla, $valPublicacion);
            echo json_encode($respuesta);
        }
        if($valPublicacion == 4){
            $tabla = "adm_registro_cancha";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla, $valPublicacion);
            echo json_encode($respuesta);
        }
    }

/*====================================
            ACTIVIDAD
 ====================================*/
    public function ajaxMostrarPublicacion(){
        $item = $this->idPublicacion;
        $valPublica = $this->valPublica;
        if($valPublica == 1){
            $tablas = "adm_registro_act ara, adm_actividades aa, adm_usuarios au";
            $respuesta = ControladorPublicacion::ctrConsultarPublicacion($tablas,$item,$valPublica);
            echo json_encode($respuesta);
        }
        if($valPublica == 2){
            $tablas = "adm_negocios an, adm_registro_neg arn, adm_usuarios au";
            $respuesta = ControladorPublicacion::ctrConsultarPublicacion($tablas,$item,$valPublica);
            echo json_encode($respuesta);
        }
        if($valPublica == 3){
            $tablas = "adm_buzon ab, adm_usuarios au";
            $respuesta = ControladorPublicacion::ctrConsultarPublicacion($tablas,$item,$valPublica);
            echo json_encode($respuesta);
        }
        if($valPublica == 4){
            $tablas = "adm_canchas ac, adm_registro_cancha arc, adm_usuarios au";
            $respuesta = ControladorPublicacion::ctrConsultarPublicacion($tablas,$item,$valPublica);
            echo json_encode($respuesta);
        }
    }

    public function ajaxProcesarPublicacion(){
        $item = $this ->idProcesar;
        $valPublicar = $this ->valPublicar;
        if($valPublicar == 1){
            $tabla = "ADM_REGISTRO_ACT";
            $respuesta = ModeloPublicacion::mdlProcesarPublicacion($tabla,$item);
            echo json_encode($respuesta);
        }
        elseif($valPublicar == 2){
            $tabla = "ADM_REGISTRO_NEG";
            $respuesta = ModeloPublicacion::mdlProcesarPublicacion($tabla,$item);
            echo json_encode($respuesta);
        }
        elseif($valPublicar == 3){
            $tabla = "ADM_BUZON";
            $respuesta = ModeloPublicacion::mdlProcesarPublicacion($tabla,$item);
            echo json_encode($respuesta);
        }
        elseif($valPublicar == 4){
            $tabla = "ADM_REGISTRO_CANCHA";
            $respuesta = ModeloPublicacion::mdlProcesarPublicacion($tabla,$item);
            echo json_encode($respuesta);
        }
    }
}

/*====================================
        Selección de Publicación
 ====================================*/
if(isset($_POST["valPublicacion"])){
    $publicacion = new AjaxPublicacion();
    $publicacion -> valPublicacion = $_POST["valPublicacion"];
    $publicacion ->ajaxConsultarPublicacion();
}
/*====================================
        Mostrar Publicación
 ====================================*/
 if(isset($_POST["idPublicacion"])){
    if(isset($_POST["valPublica"])){
        $publicacion = new AjaxPublicacion();
        $publicacion -> idPublicacion = $_POST["idPublicacion"];
        $publicacion -> valPublica = $_POST["valPublica"];
        $publicacion ->ajaxMostrarPublicacion();
    }
}
/*====================================
        Procesar Publicación
 ====================================*/
 if(isset($_POST["idProcesar"])){
    if(isset($_POST["valPublicar"])){
        $publicacion = new AjaxPublicacion();
        $publicacion -> idProcesar = $_POST["idProcesar"];
        $publicacion -> valPublicar = $_POST["valPublicar"];
        $publicacion ->ajaxProcesarPublicacion();
    }
}