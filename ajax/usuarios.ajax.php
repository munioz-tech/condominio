<?php
require_once "../controllers/usuarios_controlador.php";
require_once "../models/usuarios_modelo.php";

class AjaxUsuario{ 

    public $idUsuario;   
    public function ajaxEditarUsuario(){
        $item = "SU.CEDULA";
        $valor = $this->idUsuario;
        $respuesta = ControladorUsuario::ctrMostrarUsuario($item, $valor);
        echo json_encode($respuesta);
    }

    public $id_rol;
    public $activarId;
    public $activarUsuario;  
    public $u_modifica;  
    public function ajaxActivarUsuario(){
        $tabla = "seg_usuario";
        $item1 = "estado";
        $valor1 =  $this->activarUsuario;
        $item2 = "cedula";
        $valor2 = $this->activarId;
        $id_rol = $this->id_rol;
        $u_modifica = $this->u_modifica;
        ModeloUsuarios::mdlActivarUsuarioLogin($valor2,$id_rol,"Desbloqueo", 0);
        ModeloUsuarios::mdlActivarRolUsuario($valor2,1,$u_modifica,"seg_usuario_rol");
        ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2, $this->u_modifica);
    }
    
    public $validarUsuario;
    public function ajaxValidarUsuario(){
        $item = "usuario";
        $valor = $this->validarUsuario;
        $respuesta = ControladorUsuario::ctrMostrarUsuario($item, $valor);
        if($respuesta["estado"] == 10){echo json_encode("");}
        else {echo json_encode($respuesta);}
        
    }
    
}
/*====================================
        EDITAR USUARIO
 ====================================*/
if (isset($_POST["idUsuario"])){
    $editar = new AjaxUsuario();
    $editar -> idUsuario = $_POST["idUsuario"];
    $editar ->ajaxEditarUsuario();
}
    
/*====================================
        ACTIVAR USUARIO
====================================*/
if (isset($_POST["activarUsuario"])){
    $editar = new AjaxUsuario();
    $editar -> activarUsuario = $_POST["activarUsuario"];
    $editar -> activarId = $_POST["activarId"];
    $editar -> u_modifica = $_POST["u_modifica"];
    $editar -> id_rol = $_POST["idRol"];
    $editar ->ajaxActivarUsuario();
}

/*====================================
    VAlIDAR USUARIO REPETIDO
====================================*/
if (isset($_POST["validarUsuario"])){
    $valUsuario = new AjaxUsuario();
    $valUsuario -> validarUsuario = $_POST["validarUsuario"];
    $valUsuario ->ajaxValidarUsuario();
}