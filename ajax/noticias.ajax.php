<?php
require_once "../models/noticias_modelo.php";
require_once "../controllers/noticias_controlador.php";

class AjaxNoticias{ 
    public $idMostrarNoticia;
    public function ajaxMostrarNoticiaActividad(){
        $idMostrarNoticia = $this->idMostrarNoticia;
        if($idMostrarNoticia == 0){
            $noticiaReunion = new ControladorNoticias();
            $noticiaReu = $noticiaReunion -> ctrMostrarNoticiaReunion();
            $noticiaActividades = new ControladorNoticias();
            $noticiaAct = $noticiaActividades -> ctrMostrarNoticiaActividades();
            $noticiaNegocios = new ControladorNoticias();
            $noticiaNeg = $noticiaNegocios -> ctrMostrarNoticiaNegocios();
            $noticiaReservas = new ControladorNoticias();
            $noticiaRes = $noticiaReservas -> ctrMostrarNoticiaReservas();
            $noticiaBuzon = new ControladorNoticias();
            $noticiaBuz = $noticiaBuzon -> ctrMostrarNoticiaBuzon();
            $DataCollection = array();

            foreach($noticiaReu as $val => $valor){$DataCollection[] = $valor;}
            foreach($noticiaAct as $val => $valor){$DataCollection[] = $valor;}
            foreach($noticiaNeg as $val => $valor){$DataCollection[] = $valor;}
            foreach($noticiaRes as $val => $valor){$DataCollection[] = $valor;}
            foreach($noticiaBuz as $val => $valor){$DataCollection[] = $valor;}
            
            usort($DataCollection, function($a, $b){return strcmp($b["f_crea"], $a["f_crea"]);});

            foreach($DataCollection as $key => $value){
            if($key <= 20){
            echo '<div class="column col-sm-6">
                    <div class="card card-outline card-dark">';
                    if(isset($value["idTipo"]) && $value["idTipo"] == 5){
                        echo '<div class="ribbon-wrapper ribbon-xl">
                                <div class="ribbon bg-maroon text-lg">
                                '.utf8_decode(utf8_encode($value["tipo_publicacion"])).'
                                </div>
                            </div>';
                    }
                    if(isset($value["idTipo"]) && $value["idTipo"] == 1){
                    echo '<div class="ribbon-wrapper ribbon-xl">
                            <div class="ribbon bg-info text-lg">
                                '.$value["tipo_publicacion"].'
                            </div>
                        </div>';
                    }
                    if(isset($value["idTipo"]) && $value["idTipo"] == 2){
                        echo '<div class="ribbon-wrapper ribbon-xl">
                                <div class="ribbon bg-navy text-lg">
                                    '.$value["tipo_publicacion"].'
                                </div>
                            </div>';
                    }
                    if(isset($value["idTipo"]) && $value["idTipo"] == 3){
                        echo '<div class="ribbon-wrapper ribbon-xl">
                                <div class="ribbon bg-purple text-lg">
                                    '.$value["tipo_publicacion"].'
                                </div>
                            </div>';
                    }
                    if(isset($value["queja_idea"]) && $value["idTipo"] == 4){
                        if($value["queja_idea"] == 1){
                            echo '<div class="ribbon-wrapper ribbon-lg">
                            <div class="ribbon bg-orange text-lg">
                            '.utf8_decode(utf8_encode($value["tipo_publicacion"])).'
                            </div></div>';
                        }
                        if($value["queja_idea"] == 2){
                            echo '<div class="ribbon-wrapper ribbon-lg">
                            <div class="ribbon bg-olive text-lg">
                            '.utf8_decode(utf8_encode($value["tipo_publicacion"])).'
                            </div></div>';
                        }
                    }

            if(isset($value["ruta_img"])){echo '<center><img src="'.$value["ruta_img"].'" alt="Sin imagen de Publicación" style="width: 35%; height: 100%"></center>';}
                echo '<div class="container">';
                if(isset($value["tema"])){
                echo '<h2>Convocatoria a Reunión</h2>
                        <p>El comité directivo del Condominio los convoca a una reunión mediante la plataforma Zoom con tema: </p>
                        <p>'.str_replace("\n", "<br>", $value["tema"]).'</p>
                        <p><a href="'.$value["url"].'">Enlace a reunión</a></p>
                        <p>Con fecha y hora: '.$value["fecha_inicio"].'</p>';
                }
                if(isset($value["id_act_padre"])){
                    if($value["id_act_padre"] == 1){echo '<h2>Mantenimiento - '.$value["tipo_actividad"].'</h2>';}
                    if($value["id_act_padre"] == 2){echo '<h2>Extravío - '.$value["tipo_actividad"].'</h2>';}
                    if($value["id_act_padre"] == 3){echo '<h2>Varios - '.$value["tipo_actividad"].'</h2>';}
                }
                if(isset($value["id_cancha"])){
                    if($value["id_cancha"] == 1){
                        echo '<h2>Cancha: Tenis</h2>';
                        echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                        echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                    }
                    if($value["id_cancha"] == 2){
                        echo '<h2>Cancha: Indor</h2>';
                        echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                        echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                    }
                    if($value["id_cancha"] == 3){
                        echo '<h2>Cancha: Voley</h2>';
                        echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                        echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                    }
                    if($value["id_cancha"] == 4){
                        echo '<h2>Cancha: Básquet</h2>';
                        echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                        echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                    }
                }
                if(isset($value["id_negocio"])){
                    if($value["id_negocio"] == 1){
                        echo '<h2>Venta: '.$value["t_negocio"].'</h2>';
                        echo '<p class="title">Precio: $'.$value["precio"].'</p>';
                    }
                    if($value["id_negocio"] == 2){
                        echo '<h2>Alquiler: '.$value["t_negocio"].'</h2>';
                        echo '<p class="title">Precio: $'.$value["precio"].'</p>';
                    }
                    if($value["id_negocio"] == 3){
                        echo '<h2>Curso: '.$value["t_negocio"].'</h2>';
                        echo '<p class="title">Precio: $'.$value["precio"].'</p>';
                    }
                }
                if(isset($value["queja_idea"])){
                    if($value["queja_idea"] == 1){echo '<h2>Queja</h2>';}
                    if($value["queja_idea"] == 2){echo '<h2>Idea</h2>';}
                }
                if(isset($value["descripcion"])){echo '<p>'.$value["descripcion"].'</p>';}
                if(isset($value["nombre"])){
                    echo '<div class="modal-footer d-flex justify-content-between">
                            <div class="d-flex align-items-start">
                                <p class="tittle">Autor: '.$value["nombre"].' '.$value["apellidos"].'</p>
                            </div>
                        </div>';
                }
                echo '</div>
                <div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>
                </div>
            </div>';
            }
        }
    } 

        if($idMostrarNoticia == 1){
            $noticiaActividades = new ControladorNoticias();
            $noticiaAct = $noticiaActividades -> ctrMostrarNoticiaActividades();
            usort($noticiaAct, function($a, $b){return strcmp($b["f_crea"], $a["f_crea"]);});
            foreach($noticiaAct as $actividades => $valueAct){
                echo '<div class="column col-sm-6">
                    <div class="card card-outline card-dark">
                    <div class="ribbon-wrapper ribbon-xl">
<div class="ribbon bg-info text-lg">
'.$valueAct["tipo_publicacion"].'
</div>
</div>
                        <center><img src="'.$valueAct["ruta_img"].'" alt="Actividad" style="width: 35%; height: 100%"></center>
                        <div class="container">';
                            if($valueAct["id_act_padre"] == 1){echo '<h2>Mantenimiento - '.$valueAct["tipo_actividad"].'</h2>';}
                            if($valueAct["id_act_padre"] == 2){echo '<h2>Extravío - '.$valueAct["tipo_actividad"].'</h2>';}
                            if($valueAct["id_act_padre"] == 3){echo'<h2>Varios - '.$valueAct["tipo_actividad"].'</h2>';}
                            echo '<p>'.$valueAct["descripcion"].'</p>
                            <div class="modal-footer d-flex justify-content-between">
                                <div class="d-flex align-items-start">
                                    <p class="tittle">Autor: '.$valueAct["nombre"].' '.$valueAct["apellidos"].'</p>
                                </div>
                                <div class="d-flex align-items-end">
                                    <input type="hidden" idMostrarNoticia="'.$valueAct["idTipo"].'">
                                </div>
                            </div>
                        </div>
                        <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
                    </div>
                </div>';
            }
        }
        if($idMostrarNoticia == 2){
            $noticiaReservas = new ControladorNoticias();
            $noticiaRes = $noticiaReservas -> ctrMostrarNoticiaReservas();
            usort($noticiaRes, function($a, $b){return strcmp($b["f_crea"], $a["f_crea"]);});
            foreach($noticiaRes as $reservas => $valueRes){
                echo '<div class="column col-sm-6">
                    <div class="card card-outline card-dark">
                    <div class="ribbon-wrapper ribbon-xl">
<div class="ribbon bg-navy text-lg">
'.$valueRes["tipo_publicacion"].'
</div>
</div>
                        <div class="container">';
                            if($valueRes["id_cancha"] == 1){
                                echo '<h2>Cancha: Tenis</h2>';
                                echo '<p class="title">Desde: '.$valueRes["fyh_desde"].'</p>';
                                echo '<p class="title">Hasta: '.$valueRes["fyh_hasta"].'</p>';
                            }
                            if($valueRes["id_cancha"] == 2){
                                echo '<h2>Cancha: Indor</h2>';
                                echo '<p class="title">Desde: '.$valueRes["fyh_desde"].'</p>';
                                echo '<p class="title">Hasta: '.$valueRes["fyh_hasta"].'</p>';
                            }
                            if($valueRes["id_cancha"] == 3){
                                echo '<h2>Cancha: Voley</h2>';
                                echo '<p class="title">Desde: '.$valueRes["fyh_desde"].'</p>';
                                echo '<p class="title">Hasta: '.$valueRes["fyh_hasta"].'</p>';
                            }
                            if($valueRes["id_cancha"] == 4){
                                echo '<h2>Cancha: Básquet</h2>';
                                echo '<p class="title">Desde: '.$valueRes["fyh_desde"].'</p>';
                                echo '<p class="title">Hasta: '.$valueRes["fyh_hasta"].'</p>';
                            }
                            echo '<p>'.$valueRes["descripcion"].'</p>
                            <div class="modal-footer d-flex justify-content-between">
                                <div class="d-flex align-items-start">
                                    <p class="tittle">Autor: '.$valueRes["nombre"].' '.$valueRes["apellidos"].'</p>
                                </div>
                                <div class="d-flex align-items-end">
                                    <input type="hidden" idMostrarNoticia="'.$valueRes["idTipo"].'">
                                </div>
                            </div>
                        </div>
                        <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
                    </div>
                </div>';
            }
        }
        if($idMostrarNoticia == 3){
            $noticiaNegocios = new ControladorNoticias();
            $noticiaNeg = $noticiaNegocios -> ctrMostrarNoticiaNegocios();
            usort($noticiaNeg, function($a, $b){return strcmp($b["f_crea"], $a["f_crea"]);});
            foreach($noticiaNeg as $negocios => $valueNeg){
                echo '<div class="column col-sm-6">
                    <div class="card card-outline card-dark">
                    <div class="ribbon-wrapper ribbon-xl">
                    <div class="ribbon bg-purple text-lg">
                    '.$valueNeg["tipo_publicacion"].'
                    </div>
                    </div>
                        <center><img src="'.$valueNeg["ruta_img"].'" alt="Negocio" style="width: 35%; height: 100%"></center>
                        <div class="container">';
                            if($valueNeg["id_negocio"] == 1){
                                echo '<h2>Venta: '.$valueNeg["t_negocio"].'</h2>';
                                echo '<p class="title">Precio: $'.$valueNeg["precio"].'</p>';
                            }
                            if($valueNeg["id_negocio"] == 2){
                                echo '<h2>Alquiler: '.$valueNeg["t_negocio"].'</h2>';
                                echo '<p class="title">Precio: $'.$valueNeg["precio"].'</p>';
                            }
                            if($valueNeg["id_negocio"] == 3){
                                echo '<h2>Curso: '.$valueNeg["t_negocio"].'</h2>';
                                echo '<p class="title">Precio: $'.$valueNeg["precio"].'</p>';
                            }
                            echo '<p>'.$valueNeg["descripcion"].'</p>
                            <div class="modal-footer d-flex justify-content-between">
                                <div class="d-flex align-items-start">
                                    <p class="tittle">Autor: '.$valueNeg["nombre"].' '.$valueNeg["apellidos"].'</p>
                                </div>
                                <div class="d-flex align-items-end">
                                    <input type="hidden" idMostrarNoticia="'.$valueNeg["idTipo"].'">
                                </div>
                            </div>
                        </div>
                        <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
                    </div>
                </div>';
            }
        }
        if($idMostrarNoticia == 4){
            $noticiaBuzon = new ControladorNoticias();
            $noticiaBuz = $noticiaBuzon -> ctrMostrarNoticiaBuzon();
            usort($noticiaBuz, function($a, $b){return strcmp($b["f_crea"], $a["f_crea"]);});
            foreach($noticiaBuz as $buzon => $valueBuz){
                echo '<div class="column col-sm-6">
                    <div class="card card-outline card-dark">';
                    if($valueBuz["queja_idea"] == 1){echo '<div class="ribbon-wrapper ribbon-lg">
<div class="ribbon bg-orange text-lg">
'.utf8_decode(utf8_encode($valueBuz["tipo_publicacion"])).'
</div>
</div>';}
if($valueBuz["queja_idea"] == 2){echo '<div class="ribbon-wrapper ribbon-lg">
    <div class="ribbon bg-olive text-lg">
    '.utf8_decode(utf8_encode($valueBuz["tipo_publicacion"])).'
    </div>
    </div>';}
                        echo '<div class="container">';
                            if($valueBuz["queja_idea"] == 1){echo '<h2>Queja</h2>';}
                            if($valueBuz["queja_idea"] == 2){echo'<h2>Idea</h2>';}
                            echo '<p>'.$valueBuz["descripcion"].'</p>
                            <div class="modal-footer">
                                <input type="hidden" idMostrarNoticia="'.$valueBuz["idTipo"].'">';
                            echo '</div>
                        </div>
                        <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
                    </div>
                </div>';
            }
        }
        if($idMostrarNoticia == 5){
            $noticiaReunion = new ControladorNoticias();
            $noticiaReu = $noticiaReunion -> ctrMostrarNoticiaReunion();
            usort($noticiaReu, function($a, $b){return strcmp($b["f_crea"], $a["f_crea"]);});
            foreach($noticiaReu as $reunion => $valueReu){
                echo '<div class="column col-sm-6">
                    <div class="card card-outline card-dark">
                        <div class="container">
                        <h2>Convocatoria a Reunión</h2>
                        <p>El comité directivo del Condominio los convoca a una reunión mediante la plataforma Zoom con tema: </p>
                        <p>'.str_replace("\n", "<br>", $valueReu["tema"]).'</p>
                        <p><a href="'.$valueReu["url"].'">Enlace a reunión</a></p>
                        <p>Con fecha y hora: '.$valueReu["fecha_inicio"].'</p>
                            <div class="modal-footer d-flex justify-content-between">
                                <div class="d-flex align-items-start">
                                    <p class="tittle">Autor: '.$valueReu["nombre"].' '.$valueReu["apellidos"].'</p>
                                </div>
                                <div class="d-flex align-items-end">
                                    <p><button class="btn bg-maroon">'.utf8_decode(utf8_encode($valueReu["tipo_publicacion"])).'</button></p>
                                </div>
                            </div>
                        </div>
                        <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
                    </div>
                </div>';
            }
        }
    }
}

/*====================================
    PARA MOSTRAR EN LA TABLA
 ====================================*/
if (isset($_POST["idMostrarNoticia"])){
    $ajaxNegocios = new AjaxNoticias();
    $ajaxNegocios -> idMostrarNoticia = $_POST["idMostrarNoticia"];
    $ajaxNegocios ->ajaxMostrarNoticiaActividad();
}