<?php
require_once "../controllers/reportes_controlador.php";
require_once "../models/reportes_modelo.php";

class AjaxReporte{ 
    public $valReporte;
    public function ajaxConsultarReporte(){
        $select = $this->valReporte;
        if($select == 1){
            $tabla = "ADM_INVITADO";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla,$select);
            echo json_encode($respuesta);
        }
        if($select == 2){
            $tabla = "GEN_LOGIN";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla,$select);
            echo json_encode($respuesta);
        }
        if($select == 3){
            $tabla = "SEG_ALERTA";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla,$select);
            echo json_encode($respuesta);
        }
        if($select == 4){
            $tabla = "ADM_REGISTRO_ACT ARA, ADM_ACTIVIDADES AA";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla,$select);
            echo json_encode($respuesta);
        }
        if($select == 5){
            $tabla = "ADM_BUZON";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla,$select);
            echo json_encode($respuesta);
        }
        if($select == 6){
            $tabla = "ADM_REGISTRO_NEG";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla,$select);
            echo json_encode($respuesta);
        }
        if($select == 7){
            $tabla = "ADM_REGISTRO_CANCHA";
            $respuesta = ModeloReporte::mdlConsultarReporte($tabla,$select);
            echo json_encode($respuesta);
        }
    }
}

/*====================================
        Selección de Reporte
 ====================================*/
 if (isset($_POST["valReporte"])){
    $reporte = new AjaxReporte();
    $reporte -> valReporte = $_POST["valReporte"];
    $reporte ->ajaxConsultarReporte();
}