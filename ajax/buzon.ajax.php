<?php
require_once "../models/buzon_modelo.php";
require_once "../controllers/buzon_controlador.php";

class AjaxBuzon{ 
    public $idBuzon;

    public function ajaxEditarBuzon(){
        $item = "id";
        $valor = $this->idBuzon;
        $respuesta = ControladorBuzon::ctrMostrarBuzon($item,$valor);
        echo json_encode($respuesta);
    }
}

/*====================================
            EDITAR BUZON
 ====================================*/
 if (isset($_POST["idBuzon"])){
    $actividad = new AjaxBuzon();
    $actividad -> idBuzon = $_POST["idBuzon"];
    $actividad ->ajaxEditarBuzon();
}