<?php
require_once "../models/reservas_modelo.php";
require_once "../controllers/reservas_controlador.php";

class AjaxReserva{ 
    public $reserva;
    public function ajaxEditarReserva(){
        $item = "ID";
        $valor = $this->reserva;
        $respuesta = ControladorReservas::ctrMostrarReserva($item,$valor);
        echo json_encode($respuesta);
    }
}

if (isset($_POST["reserva"])){
    $ajaxNegocios = new AjaxReserva();
    $ajaxNegocios -> reserva = $_POST["reserva"];
    $ajaxNegocios ->ajaxEditarReserva();
}