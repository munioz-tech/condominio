<?php
require_once "../models/negocios_modelo.php";
require_once "../controllers/negocios_controlador.php";

class AjaxNegocio{ 
    public $negocio;
    public $idNegocio;
    public function ajaxMostrarNegocio(){
        $negocio = $this->negocio;
        $HTML = '<div class="col-sm-6 form-inline d-flex justify-content-center ocultaNegocio">';
        if($negocio == 1){
            $HTML.= '<div class="col-xs-3 tipoNegocio">
                        <input type="text" class="form-control" id="tipoNegocio" name="tipoNegocio" placeholder="Venta de:" required>
                    </div>';
        }
        if($negocio == 2){
            $HTML.= '<div class="col-xs-3 tipoNegocio">
                        <input type="text" class="form-control" id="tipoNegocio" name="tipoNegocio" placeholder="Alquiler de:" required>
                    </div>';
        }
        if($negocio == 3){
            $HTML.= '<div class="col-xs-3 tipoNegocio">
                        <input type="text" class="form-control" id="tipoNegocio" name="tipoNegocio" placeholder="Curso de:" required>
                    </div>';
        }

        $HTML.= '<div class="col-sm-3 precioNegocio">
                    <input type="text" class="form-control"  id="precioNegocio" name="precioNegocio" placeholder="Ej.: 5.50" onkeypress="return filterFloat(event,this);" required>
                </div></div>';
        echo json_encode($HTML);
    }

    public function ajaxEditarNegocio(){
        $item = "ID";
        $valor = $this->idNegocio;
        $respuesta = ControladorNegocios::ctrMostrarNegocio($item,$valor);
        echo json_encode($respuesta);
    }
}

/*====================================
    PARA MOSTRAR EN LA TABLA
 ====================================*/
if (isset($_POST["negocio"])){
    $ajaxNegocios = new AjaxNegocio();
    $ajaxNegocios -> negocio = $_POST["negocio"];
    $ajaxNegocios ->ajaxMostrarNegocio();
}

/*====================================
            EDITAR NEGOCIOS
 ====================================*/
if (isset($_POST["idNegocio"])){
    $ajaxNegocios = new AjaxNegocio();
    $ajaxNegocios -> idNegocio = $_POST["idNegocio"];
    $ajaxNegocios ->ajaxEditarNegocio();
}