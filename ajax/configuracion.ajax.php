<?php
require_once "../models/configuracion_modelo.php";
require_once "../controllers/configuracion_controlador.php";

class AjaxConfiguracion{
    public $idConfiguracion;
    public function ajaxMostrarDatosUsuario(){
        $item = "id";
        $valor = $_POST["idConfiguracion"];
        $tabla = "adm_usuarios";
        $respuesta = ControladorConfiguraciones::ctrObtenerDatosUsuario($tabla,$item,$valor);
        echo json_encode($respuesta);
    }
}

if (isset($_POST["idConfiguracion"])){
    $actividad = new AjaxConfiguracion();
    $actividad -> idConfiguracion = $_POST["idConfiguracion"];
    $actividad ->ajaxMostrarDatosUsuario();
}