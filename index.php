<?php

//=== Controladores === 
require_once "controllers/plantilla_controlador.php";
require_once "controllers/noticias_controlador.php";
require_once "controllers/actividades_controlador.php";
require_once "controllers/negocios_controlador.php";
require_once "controllers/configuracion_controlador.php";
require_once "controllers/buzon_controlador.php";
require_once "controllers/usuarios_controlador.php";
require_once "controllers/reservas_controlador.php";
require_once "controllers/reportes_controlador.php";
require_once "controllers/publicaciones_controlador.php";
require_once "controllers/reuniones_controlador.php";

//=== Modelos ===
require_once "models/noticias_modelo.php";
require_once "models/actividades_modelo.php";
require_once "models/negocios_modelo.php";
require_once "models/configuracion_modelo.php";
require_once "models/buzon_modelo.php";
require_once "models/usuarios_modelo.php";
require_once "models/reservas_modelo.php";
require_once "models/reportes_modelo.php";
require_once "models/publicaciones_modelo.php";
require_once "models/reuniones_modelo.php";

//=== Herramientas - Utils ===
require_once "utils/Utils.php";
require_once "utils/src/class/Mailer.php";
require_once "view/plugins/zoom/create-meeting.php";


$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();