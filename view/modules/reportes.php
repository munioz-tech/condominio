<?php
  if(!isset($_SESSION["idRol"])){
    echo '<script>
              window.location = "inicio";
            </script>';
            return;
  }
  if($_SESSION['idRol'] != 1){
    echo '<script>
            window.location = "inicio";
          </script>';
          return;
  }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Reportes</h3> 
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
            <li class="breadcrumb-item active">Reportes</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card card-outline card-dark">
      <div class="card-header">
        <form method="post" class="formReporte">
          <select class="form-control select2 select2-hidden-accessible col-lg-5 selectReporte" valReporte="1" name="selectReporte" required>
            <option value="0">Seleccione tipo de reporte</option>
            <option value="1">Reporte de Usuarios Invitados</option>
            <option value="2">Reporte de Inicios de Sesión</option>
            <option value="3">Reporte de Envíos de Alertas</option>
            <option value="4">Reporte de Actividades</option>
            <option value="5">Reporte de Buzón</option>
            <option value="6">Reporte de Negocios</option>
            <option value="7">Reporte de Reservas</option>
          </select>
        </form>
      </div>
      <div class="card-body">
        
      </div>
      <div class="overlay dark">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
      </div>
    </div>
    
  </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->