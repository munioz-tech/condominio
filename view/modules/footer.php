<footer class="main-footer">
  <div class="d-flex justify-content-between align-items-center">
    <div class="col-lg-7">
      <strong>Síguenos en facebook: <a href="https://www.facebook.com/LosJardinesSur" target="_blank">
      Condominio Los Jardines Sur <i class="fab fa-facebook-square fa-lg"></i></a></strong>
    </div>
    <div class="col-xs-3">
      <div class="float-right d-none d-sm-inline-block">
        <b>Sistema de Administración de Condominio</b> 1.0.0
      </div>
    </div>
  </div>
</footer>