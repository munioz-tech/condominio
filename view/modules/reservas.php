<?php
  if(!isset($_SESSION["idRol"])){
    echo '<script>
              window.location = "inicio";
            </script>';
            return;
  }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3>Reservar Canchas</h3> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Reservar canchas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">
            <div class="md-3">
              <form name="formReservas" method="post" enctype="multipart/form-data">
                <div class="card card-outline card-dark">
                  <div class="card-header">
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i></button>
                    </div> 
                    <select id="selectReservas" name="selectReservas" class="form-control select2 select2-hidden-accessible selectReservas" required>
                      <option value="0">Cancha</option>
                      <?php
                        $reservas = new ControladorReservas();
                        $rows = $reservas -> ctrConsultarCanchas();
                        foreach($rows as $row){echo '<option value="'.$row['id'].'">'.$row['cancha'].'</option>';}
                      ?>
                    </select>
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <div class="input-group"> 
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fa fa-calendar"></span>
                          </div>
                        </div>  
                        <input type="text" id="date_desde" name="date_desde" class="form-control" value="" placeholder="Ingrese Fecha/Hora" disabled>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="input-group"> 
                        <textarea id="reservaDetalle" name="reservaDetalle" class="form-control rounded-0" rows="4" placeholder="Describa motivo de uso de la cancha..." disabled></textarea>
                        <input type="hidden" id="idReservaHidden" name="idReservaHidden" value="">
                      </div>
                    </div>
                    <?php
                      $negocio = new ControladorReservas();
                      $negocio -> ctrProcesarReserva();
                    ?>
                    <div class="btn-group col-sm-4">
                      <div class="col-xs-2">
                        <button id="btnReserva" type="submit" class="btn btn-primary swal" reservar="0" disabled>Enviar</button>
                      </div>
                      <div class="col-sm-2">
                        <button type="reset" class="btn btn-danger swal btncancelarReserva" disabled>Cancelar</button>
                      </div>
                    </div>
                  </div>
                  <div class="overlay dark">
                    <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="card card-outline card-dark">
              <div class="card-header">
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
                <h3>Calendario de Reservas</h3> 
              </div>
              <div class="card-body p-0">
                <div id="calendar"></div>
              </div>
              <div class="overlay dark">
                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

     <!-- Sección 2 -->
  <section class="content">
    <div class="card card-outline card-dark">
      <div class="card-header form-group" id="tablaPublica">
        <table class="table table-bordered table-hover nowrap dt-responsive dataTable_width-margin_auto" id="tablas">
          <thead>
            <tr>
              <th>#</th>
              <th>Cancha</th>
              <th>Descripcion</th>
              <th>Desde</th>
              <th>Hasta</th>
              <th>Creado</th>
              <th>Observación</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $tablas = "";
              $regReservas = ModeloReservas::mdlObtenerRegistroReserva($tablas);
              $contador = 1;
              foreach($regReservas as $value){
                if($value["estado"] == 2 || $value["estado"] == 5 || $value["estado"] == 10){
                  echo '<tr>
                          <td>'.$contador.'</td>
                          <td>'.$value["cancha"].'</td>
                          <td>'.$value["descripcion"].'</td>
                          <td>'.date("Y-m-d H:i",strtotime($value["fyh_desde"])).'</td>
                          <td>'.date("Y-m-d H:i",strtotime($value["fyh_hasta"])).'</td>
                          <td>'.date("Y-m-d H:i",strtotime($value["f_crea"])).'</td>
                          <td>'.$value["observacion"].'</td>';
                          if($value["estado"] == 2){echo'<td><button class="btn btn-warning btn-sm">EN PROCESO</button></td>';}
                          elseif($value["estado"] == 10){echo'<td><button class="btn btn-success btn-sm">PUBLICADO</button></td>';}
                          elseif($value["estado"] == 5){echo'<td><button class="btn btn-danger btn-sm">RECHAZADO</button></td>';}
                          if($value["estado"] == 10){
                            echo  '<td>
                            <div class="btn-group">
                              <button class="btn btn-dark" disabled><i class="fa fa-cog" aria-hidden="true"></i></button>
                              <button class="btn btn-danger" disabled><i class="fa fa-times"></i></button> 
                            </div>
                          </td>
                        </tr>';
                          }
                      elseif($value["estado"] == 2 || $value["estado"] == 5){
                        echo  '<td>
                        <div class="btn-group">
                          <button class="btn btn-dark btnEditarReserva" reserva="'.$value["id"].'"><i class="fa fa-cog" aria-hidden="true"></i></button>
                          <button class="btn btn-danger btnEliminarReserva" idReserva="'.$value["id"].'"><i class="fa fa-times"></i></button> 
                        </div>
                      </td>
                    </tr>';
                      } 
                } $contador++;
              }
            ?>
          </tbody>
        </table>
      </div>
      <div class="overlay dark">
            <i class="fas fa-2x fa-sync-alt fa-spin"></i>
          </div>
    </div>
  </section>
</div>
<?php
  $borrarReserva = new ControladorReservas();
  $borrarReserva ->ctrBorrarReserva();
?>

<?php
  $mostrarReserva = new ControladorReservas();
  $respuesta = $mostrarReserva ->ctrReservar();
?>

<script>
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        lang: 'es',
        locale: 'es',
        initialView: 'dayGridMonth',
        navLinks: true,
        nowIndicator: true,
        expandRows: true,
        weekNumbers: true,
        handleWindowResize: false,
        dayMaxEventRows: true,
        weekNumberFormat: {
          week: 'numeric'
        },
        views: {
            timeGrid: {
            dayMaxEventRows: 12 
            }
        },
        eventClick: function(info) {
          var start = info.event.start;
          var end = info.event.end;
          Swal.fire({
            icon: "info",
            title: "Reserva de Cancha de "+ info.event.title,
            html: "<br><p>Inicio de reserva: " + start +"</p><br><p>Fin de reserva: " + end +"</p>",
            confirmButtonColor: '#3028d6',
          });
        },
        displayEventTime: false,
        themeSystem: 'bootstrap',
        navLinks: true,
        buttonText:{
            today: 'hoy',
            month: 'mes',
            week: 'semana',
            day: 'día'
        },
        headerToolbar:{
            start: 'today prev,next',
            center: 'title',
            end: 'dayGridMonth,timeGridWeek,timeGridDay list'
        },
        businessHours: [ // specify an array instead
            {
              daysOfWeek: [ 0, 1, 2, 3, 4, 5, 6], // Todos los días
              startTime: '07:00', // 7am
              endTime: '20:00' // 8pm
            }
          ]
    });
    calendar.setOption('locale', 'es');
    calendar.render();
    calendar.updateSize();
    <?php 
    if($respuesta != null){
      foreach($respuesta as $value => $Reservar){
        if($Reservar["id"] == 1){
          echo "calendar.addEvent({ title: '".$Reservar["cancha"]."', start: '".$Reservar["fyh_desde"]."', end: '".$Reservar["fyh_hasta"]."', color: '#28a745', textColor: 'white' });";
        }
        if($Reservar["id"] == 2){
          echo "calendar.addEvent({ title: '".$Reservar["cancha"]."', start: '".$Reservar["fyh_desde"]."', end: '".$Reservar["fyh_hasta"]."', color: '#17a2b8', textColor: 'white' });";
        }
        if($Reservar["id"] == 3){
          echo "calendar.addEvent({ title: '".$Reservar["cancha"]."', start: '".$Reservar["fyh_desde"]."', end: '".$Reservar["fyh_hasta"]."', color: '#ffc107', textColor: 'white' });";
        }
        if($Reservar["id"] == 4){
          echo "calendar.addEvent({ title: '".$Reservar["cancha"]."', start: '".$Reservar["fyh_desde"]."', end: '".$Reservar["fyh_hasta"]."', color: '#dc3545', textColor: 'white' });";
        }
      }
    }
    ?>
});
</script>