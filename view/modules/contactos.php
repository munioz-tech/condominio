<style>
    /* Three columns side by side */
.column {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 0 8px;
}

/* Display the columns below each other instead of side by side on small screens */
@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

/* Add some shadows to create a card effect */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

/* Some left and right padding inside the container */
.container {
  padding: 0 16px;
}

/* Clear floats */
.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}
</style>
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Contáctanos</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Contactos</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="card">
        <div class="card-header"> 
          <div class="row">
              <div class="column">
                  <div class="card card-outline card-dark">
                    <center><img src="public/img/admin.jpg" alt="Jane" style="width:25%"></center>
                    <div class="container">
                        <h2>Antony Canga Ortega</h2>
                        <p class="title">Dirigente &amp; Administrador</p>
                        <p>Teléfono: 0962799286</p>
                        <p>Correo: antony.cangao@ug.edu.ec</p>
                        <p><button class="button">Contact</button></p>
                    </div>
                    <div class="overlay dark">
                      <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                  </div>
              </div>

              <div class="column">
                  <div class="card card-outline card-dark">
                    <center><img src="public/img/colaborador.jpg" alt="Mike" style="width:25%"></center>
                    <div class="container">
                        <h2>Paula Dora Bonilla</h2>
                        <p class="title">Dirigente &amp; Colaborador</p>
                        <p>Teléfono: 0962799286</p>
                        <p>Correo: pauladorabonilla@gmail.com</p>
                        <p><button class="button">Contact</button></p>
                    </div>
                    <div class="overlay dark">
                      <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                  </div>
              </div>

              <div class="column">
                  <div class="card card-outline card-dark">
                    <center><img src="public/img/seguridad.jpg" alt="John" style="width:25%"></center>
                    <div class="container">
                        <h2>Edith García Tumbaco</h2>
                        <p class="title">Dirigente &amp; Colaborador</p>
                        <p>Teléfono: 0991234578</p>
                        <p>Correo: edithgarcía70@gmail.com</p>
                        <p><button class="button">Contact</button></p>
                    </div>
                    <div class="overlay dark">
                      <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                    </div>
                  </div>
              </div>
          </div>              
        </div>
        <div class="overlay dark">
          <i class="fas fa-2x fa-sync-alt fa-spin"></i>
        </div>
      </div>
    </section>
  </div>