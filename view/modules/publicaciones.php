<?php
  if(!isset($_SESSION["idRol"])){
    echo '<script>
              window.location = "inicio";
            </script>';
            return;
  }
  if($_SESSION['idRol'] != 1){
    echo '<script>
            window.location = "inicio";
          </script>';
          return;
  }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Publicaciones</h3> 
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
            <li class="breadcrumb-item active">Publicaciones</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="card card-outline card-dark">
      <div class="card-header">
        <form method="post" id="formPublicaciones" enctype="multipart/form-data">
          <select class="form-control select2 select2-hidden-accessible col-lg-5 selectPublicacion" valPublicacion="1" name="selectPublicacion" required>
            <option value="0">Seleccione tipo de publicación</option>
            <option value="1">Actividades</option>
            <option value="2">Negocios</option>
            <option value="3">Buzón</option>
            <option value="4">Reservas</option>
          </select>
        </form>
      </div>
      <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
    </div>   
  </section>
  <!-- /.content -->
</div>
<!-- ========================================
      MODAL VER PUBLICACIONES ACTIVIDAD
===========================================-->
<div class="modal fade" id="modalPublicarAct" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Detalles de Actividad</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group mb-3">
                <label for="nombresAct" class="col-sm-2 col-form-label">Nombres</label>
                <input type="text" id="nombresAct" name="nombresAct" class="form-control select2 select2-hidden-accessible nombresAct" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="telefonoAct" class="col-sm-2 col-form-label">Teléfono</label>
                <input type="text" id="telefonoAct" name="telefonoAct" class="form-control select2 select2-hidden-accessible telefonoAct" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="correoAct" class="col-sm-2 col-form-label">Correo</label>
                <input type="text" id="correoAct" name="correoAct" class="form-control select2 select2-hidden-accessible correoAct" value="" disabled> 
              </div>
            </div> 
            <div class="form-group">
              <div class="form-inline d-flex justify-content-between">
                <div class="col-sm-6 input-group d-flex justify-content-start">
                  <label for="actividadPadre" class="col-sm-4 col-form-label">Actividad</label>
                  <input type="text" name="actividadPadre" class="form-control select2 select2-hidden-accessible actividadPadre" value="" disabled> 
                </div>
                <div class="col-sm-6 input-group d-flex justify-content-end">
                  <label for="actividadHijo" class="col-sm-4 col-form-label">Tipo</label>
                  <input type="text" name="actividadHijo" class="form-control select2 select2-hidden-accessible actividadHijo" value="" disabled>
                </div>
              </div>
            </div>
            <div class="form-group">
                <label for="actividadDetalle" class="col-form-label">Descripción</label>
                <div class="input-group">
                    <textarea name="actividadDetalle" id="actividadDetalle" class="form-control rounded-0 actividadDetalle" disabled></textarea>
                </div>
            </div>
            <div class="form-group"> 
                <label for="previsualizarAct" class="col-form-label">Archivo Adjunto</label>
                <div class="input-group"> 
                    <img src="" id="previsualizarAct" class="mx-auto d-block img-thumbnail previsualizarAct" width="225px" alt="" style="border: 1px solid black;" width="225px"/>
                </div>
            </div>                      
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary btnProcesar" data-dismiss="modal" valPublica="" idProcesar="" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- ========================================
      MODAL VER PUBLICACIONES NEGOCIO
===========================================-->
<div class="modal fade" id="modalPublicarNeg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Detalles de Negocio</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group mb-3">
                <label for="nombresNeg" class="col-sm-2 col-form-label">Nombres</label>
                <input type="text" id="nombresNeg" name="nombresNeg" class="form-control select2 select2-hidden-accessible nombresNeg" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="telefonoNeg" class="col-sm-2 col-form-label">Teléfono</label>
                <input type="text" id="telefonoNeg" name="telefonoNeg" class="form-control select2 select2-hidden-accessible telefonoNeg" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="correoNeg" class="col-sm-2 col-form-label">Correo</label>
                <input type="text" id="correoNeg" name="correoNeg" class="form-control select2 select2-hidden-accessible correoNeg" value="" disabled> 
              </div>
            </div> 
            <div class="form-group">
              <label for="tipoNegocio" class="col-form-label">Negocio</label>
              <div class="input-group">
                <input type="text" name="tipoNegocio" class="form-control select2 select2-hidden-accessible tipoNegocio" value="" disabled> 
              </div>
            </div>
            <div class="form-group">
              <div class="form-inline d-flex justify-content-between">
                <div class="col-sm-6 input-group d-flex justify-content-start">
                  <label for="t_negocio" class="col-sm-2 col-form-label">De</label>
                  <input type="text" name="t_negocio" class="form-control select2 select2-hidden-accessible t_negocio" value="" disabled> 
                </div>
                <div class="col-sm-6 input-group d-flex justify-content-end">
                  <label for="precio" class="col-sm-3 col-form-label">Precio</label>
                  <input type="text" name="precio" class="form-control select2 select2-hidden-accessible precio" value="" disabled>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="negocioDetalle" class="col-form-label">Descripción</label>
              <div class="input-group"> 
                <textarea name="negocioDetalle" id="negocioDetalle" class="form-control rounded-0 negocioDetalle" disabled></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="previsualizarNeg" class="col-form-label">Archivo Adjunto</label>
              <div class="input-group"> 
                <img src="" id="previsualizarNeg" class="mx-auto d-block img-thumbnail previsualizarNeg" width="225px" alt=""/>
              </div>
            </div>                      
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary btnProcesar" data-dismiss="modal" valPublica="" idProcesar="" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- ========================================
      MODAL VER PUBLICACIONES BUZON
===========================================-->
<div class="modal fade" id="modalPublicarBuz" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Detalles del Buzon</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group mb-3">
                <label for="nombresBuz" class="col-sm-2 col-form-label">Nombres</label>
                <input type="text" id="nombresBuz" name="nombresBuz" class="form-control select2 select2-hidden-accessible nombresBuz" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="telefonoBuz" class="col-sm-2 col-form-label">Teléfono</label>
                <input type="text" id="telefonoBuz" name="telefonoBuz" class="form-control select2 select2-hidden-accessible telefonoBuz" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="correoBuz" class="col-sm-2 col-form-label">Correo</label>
                <input type="text" id="correoBuz" name="correoBuz" class="form-control select2 select2-hidden-accessible correoBuz" value="" disabled> 
              </div>
            </div> 
            <div class="form-group">
              <label for="previsualizarNeg" class="col-form-label">Tipo</label>
              <div class="input-group">
                <input type="text" name="buzon" class="form-control select2 select2-hidden-accessible buzon" value="" disabled> 
              </div>
            </div>
            <div class="form-group">
              <label for="previsualizarNeg" class="col-form-label">Descripción</label>
              <div class="input-group"> 
                <textarea name="buzonDetalle" id="buzonDetalle" class="form-control rounded-0 buzonDetalle" disabled></textarea>
              </div>
            </div>                    
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary btnProcesar" data-dismiss="modal" valPublica="" idProcesar="" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- ========================================
      MODAL VER PUBLICACIONES RESERVAS
===========================================-->
<div class="modal fade" id="modalPublicarRes" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Detalles de Reserva</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <div class="input-group mb-3">
                <label for="nombresRes" class="col-sm-2 col-form-label">Nombres</label>
                <input type="text" id="nombresRes" name="nombresRes" class="form-control select2 select2-hidden-accessible nombresRes" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="telefonoRes" class="col-sm-2 col-form-label">Teléfono</label>
                <input type="text" id="telefonoRes" name="telefonoRes" class="form-control select2 select2-hidden-accessible telefonoRes" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="correoRes" class="col-sm-2 col-form-label">Correo</label>
                <input type="text" id="correoRes" name="correoRes" class="form-control select2 select2-hidden-accessible correoRes" value="" disabled> 
              </div>
            </div> 
            <div class="form-group">
              <div class="input-group mb-3">
                <label for="reserva" class="col-sm-2 col-form-label">Cancha</label>
                <input type="text" name="reserva" class="form-control select2 select2-hidden-accessible reserva" value="" disabled> 
              </div>
              <div class="input-group mb-3">
                <label for="fecha_reserva" class="col-sm-2 col-form-label">Fecha</label>
                <input type="text" name="fecha_reserva" class="form-control fecha_reserva" value="" disabled>
              </div>
            </div>
            <div class="form-group"> 
              <label for="reservaDetalle" class="col-form-label">Descripcion</label>
              <div class="input-group"> 
                <textarea name="reservaDetalle" id="reservaDetalle" class="form-control rounded-0 reservaDetalle" disabled></textarea>
              </div>
            </div>                    
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary btnProcesar" data-dismiss="modal" valPublica="" idProcesar="" data-toggle="modal" data-target="#modalProcesar">Procesar <i class="fa fa-cog" aria-hidden="true"></i></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- ========================================
        MODAL PROCESAR PUBLICACIONES
===========================================-->
<div class="modal fade" id="modalProcesar" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Procesar Publicación</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group"> 
              <div class="input-group">
              <p class="text-secondary">Usted se responsabiliza de los datos que se mostrarán en la pestaña de Noticias, lea bien las descripciones y observe con cuidado las imágenes que se desean publicar.</p>
                <textarea name="observacion" class="form-control rounded-0 observacion" placeholder="Ingrese motivo de aceptación o rechazo de la publicación"></textarea>
              </div>
            </div>                     
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger btnRechazar" name="btnRechazar" idPublicacion="" valPublicar="" procesar="1" data-dismiss="modal">Rechazar</button>
          <button class="btn btn-primary btnAceptar" name="btnAceptar" idPublicacion="" valPublicar="" procesar="2" data-dismiss="modal">Aceptar</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
  $procesarActividad = new ControladorPublicacion();
  $procesarActividad ->ctrProcesarPublicacionAct();
?>
