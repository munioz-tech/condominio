<?php
  if(!isset($_SESSION["idRol"])){
    echo '<script>
              window.location = "inicio";
            </script>';
            return;
  }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3>Publicidad de Negocios</h3>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar negocios</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form name="formNegocios" method="post" enctype="multipart/form-data">
        <div class="card card-outline card-dark">
          <div class="card-header">
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
            <div class="d-flex justify-content-between col-sm-10">
              <div class="d-flex justify-content-start col-sm-3">
                <select id="selectNegocio" name="selectNegocio" class="form-control select2 select2-hidden-accessible selectNegocio negocioInputs" required>
                  <option value="0">Seleccione negocio</option>
                  <?php
                    $negocios = new ControladorNegocios();
                    $rows = $negocios -> ctrConsultarNegocios();
                    foreach($rows as $row)
                    {  echo '<option value="'.$row['id'].'">'.$row['tipo_negocio'].'</option>';
                    }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <div class="input-group">
                <textarea id="negocioDetalle" name="negocioDetalle" class="form-control rounded-0" rows="4" placeholder="Describa bien su negocio..." disabled></textarea>
              </div>
            </div>
            <div class="form-group d-flex justify-content-between align-items-center row">
              <div class="custom-file col-md-4 d-flex justify-content-start">
                <input type="file" class="custom-file-input fotoNegocio" id="fotoNegocio" name="fotoNegocio" disabled>
                <label class="custom-file-label labelNegocio" for="customFile">Seleccione un archivo</label>
              </div>
              <div class="col-md-4 d-flex justify-content-center">
                <img src="" class="img-thumbnail previsualizarNeg" width="75px" alt=""/>
                <input type="hidden" name="fotoNegocioActual" id="fotoNegocioActual">
                <input type="hidden" id="idNegocioHidden" name="idNegocioHidden" value="">
              </div>
                <?php
                  $negocio = new ControladorNegocios();
                  $negocio -> ctrProcesarNegocio();
                ?>
              <div class="btn-group col-md-4 d-flex justify-content-end">
                <div class="col-sm-6">
                  <button type="reset" class="btn btn-danger swal btncancelarNegocio" disabled>Cancelar</button>
                </div>
                <div class="col-xs-6">
                  <button id="btnNegocio" type="submit" class="btn btn-primary swal" disabled>Enviar</button>
                </div>
              </div>
            </div>
          </div>
          <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
        </div>
      </form>
    </section>

     <!-- Sección 2 -->
  <section class="content">
  <div class="card card-outline card-dark">
    <div class="card-header form-group" id="tablaPublica">
      <table class="table table-bordered table-hover nowrap dt-responsive dataTable_width-margin_auto" id="tablas">
        <thead>
          <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Detalle</th>
            <th>Negocio</th>
            <th>Precio</th>
            <th>Fecha</th>
            <th>Observación</th>
            <th>Estado</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $tabla = "";
            $regNegocios = ModeloNegocios::mdlObtenerRegistroNegocio($tabla);
            $contador = 1;
            foreach($regNegocios as $value){
              if($value["estado"] == 2 || $value["estado"] == 5 || $value["estado"] == 10){
                echo '<tr>
                        <td>'.$contador.'</td>
                        <td>
                          <div class="btn-group">
                            <button class="btn bg-olive" rutaImg="'.$value["ruta_img"].'" data-toggle="modal" data-target="#modalImgNegocio" id="btnFotoNeg"><i class="fas fa-images" aria-hidden="true"></i></button>
                          </div>
                        </td>
                        <td>'.$value["descripcion"].'</td>
                        <td>'.$value["t_negocio"].'</td>
                        <td>'.$value["precio"].'</td>
                        <td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
                        <td>'.$value["observacion"].'</td>';
                        if($value["estado"] == 2){echo'<td><button class="btn btn-warning btn-sm">EN PROCESO</button></td>';}
                        elseif($value["estado"] == 10){echo'<td><button class="btn btn-success btn-sm">PUBLICADO</button></td>';}
                        elseif($value["estado"] == 5){echo'<td><button class="btn btn-danger btn-sm">RECHAZADO</button></td>';}
                        if($value["estado"] == 10){
                          echo  '<td>
                          <div class="btn-group">
                            <button class="btn btn-dark btnEditarNegocio" disabled><i class="fa fa-cog" aria-hidden="true"></i></button>
                            <button class="btn btn-danger btnEliminarNegocio" disabled><i class="fa fa-times"></i></button> 
                          </div>
                        </td>
                      </tr>';
                        }
                    elseif($value["estado"] == 2 || $value["estado"] == 5){
                      echo  '<td>
                      <div class="btn-group">
                        <button class="btn btn-dark btnEditarNegocio" idNegocio="'.$value["id"].'"><i class="fa fa-cog" aria-hidden="true"></i></button>
                        <button class="btn btn-danger btnEliminarNegocio" idNegocio="'.$value["id"].'" eliminarImgNeg="'.$value["ruta_img"].'"><i class="fa fa-times"></i></button> 
                      </div>
                    </td>
                  </tr>';
                    } 
              } $contador++;
            }
          ?>
        </tbody>
      </table>
</div>
<div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
    </div>
  </section>
</div>

<!--========================================
            MODAL VER IMAGEN
===========================================-->
<div class="modal fade" id="modalImgNegocio" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #3c8dbc; color:white ;">
        <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <img src="" class="img-thumbnail modalFotoNeg" alt="Imagen Negocio"/>
        </div>
      </div>       
    </div>
  </div>
</div>

<?php
    $borrarNegocio = new ControladorNegocios();
    $borrarNegocio ->ctrBorrarNegocio();
?>