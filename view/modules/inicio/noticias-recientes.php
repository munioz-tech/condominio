<?php
    $actividades = ControladorNoticias::ctrMostrarNoticiaActividadesTop();
    $reservas = ControladorNoticias::ctrMostrarNoticiaReservasTop();
    $negocios = ControladorNoticias::ctrMostrarNoticiaNegociosTop();
    $buzon = ControladorNoticias::ctrMostrarNoticiaBuzonTop();
    $DataCollection = array();

    foreach($actividades as $key => $value)
    {
        $DataCollection[] = $value;
    }
    foreach($reservas as $key => $value)
    {
        $DataCollection[] = $value;
    }
    foreach($negocios as $key => $value)
    {
        $DataCollection[] = $value;
    }
    foreach($buzon as $key => $value)
    {
        $DataCollection[] = $value;
    }
    
    usort($DataCollection, function ($a, $b) {
        return strcmp($b["fecha"], $a["fecha"]);
    });
    
?>
<!-- Lista de Noticias -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Noticias Recientes</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <ul class="products-list product-list-in-card pl-2 pr-2">
            <?php
                foreach($DataCollection as $key => $value)
                {   if ($key < 4)
                    {   echo '  <li class="item">
                                <div class="product-img">';
                                if(isset($value["ruta_img"]))
                                {echo '<img src="'.$value["ruta_img"].'" alt="Product Image" class="img-size-50">';}
                                else{echo '<img src="public/img/no-img.png" alt="Product Image" class="img-size-50">';}
                        echo '  </div>
                                <div class="product-info">
                                    <a href="noticias" class="product-title">'.$value["tipo_publicacion"].'
                                    <span class="badge badge-warning float-center">'.$value["tipoNoticia"].'</span></a>
                                    <span class="product-description">
                                    '.$value["nombre"].' '.$value["apellidos"] .'
                                    </span>
                                </div>
                            </li>';
                    }
                }
            ?>
        </ul>
    </div>
    <div class="card-footer text-center">
        <a href="noticias" class="uppercase">Ver todas las noticias</a>
    </div>
</div>