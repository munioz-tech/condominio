<?php
    $actividades = ControladorActividades::ctrTotalActividades();
    $buzon = ControladorBuzon::ctrTotalBuzon();
    $reservas = ControladorReservas::ctrTotalReservas();
    $negocios = ControladorNegocios::ctrTotalNegocios();
    $total = $actividades["total"] + $buzon["total"] + $reservas["total"] + $negocios["total"];
?>
<div class="card">
    <div class="card-header">
    <h3 class="card-title">Publicaciones</h3>

    <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
        </button>
    </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <div class="row">
        <div class="col-md-8">
        <div class="chart-responsive">
            <canvas id="pieChart" height="150"></canvas>
        </div>
        <!-- ./chart-responsive -->
        </div>
        <!-- /.col -->
        <div class="col-md-4">
        <ul class="chart-legend clearfix">
            <li><i class="far fa-circle text-success"></i> Actividades</li>
            <li><i class="far fa-circle text-warning"></i> Reservas</li>
            <li><i class="far fa-circle text-primary"></i> Negocio</li>
            <li><i class="far fa-circle text-danger"></i> Buzón</li>
        </ul>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer bg-white p-0">
    <ul class="nav nav-pills flex-column">
        <li class="nav-item">
        <a href="reportes" class="nav-link">
            Total Publicaciones (En proceso, aceptadas y rechazadas)
            <span class="float-right text-success">
            <i class="fas fa-arrow-up text-sm"></i> <?php echo $total; ?>
            </span>
        </a>
        </li>
    </ul>
    </div>
    <!-- /.footer -->
</div>
<!-- /.card -->

