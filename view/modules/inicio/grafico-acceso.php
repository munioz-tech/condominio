<div class="col-md-12 card bg-gradient-info">
    <div class="card-header border-0 ui-sortable-handle">
        <h3 class="card-title">
            <i class="fas fa-th mr-1"></i>
            Accesos de Usuarios
        </h3>   
        <div class="card-tools">
            <button type="button" class="btn bg-info btn-sm" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn bg-info btn-sm" data-card-widget="remove">
            <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div id="chart" style="height: 250px;"></div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer bg-transparent">
    <div class="row">
    </div>
    <!-- /.row -->
    </div>
    <!-- /.card-footer -->
</div>
<!-- /.card -->