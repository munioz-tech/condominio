<?php $noticias = ControladorNoticias::ctrTotalNoticia();?>
<div class="col-lg-3 col-6">
    <div class="small-box bg-info">
        <div class="inner">
            <h3><?php echo number_format($noticias["total"]); ?></h3>
            <p>Noticias</p>
        </div>
        <div class="icon">
            <i class="far fa-newspaper"></i>
        </div>
        <a href="noticias" class="small-box-footer">Mas Información<i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

<?php $accesos = ControladorUsuario::ctrTotalAccesos();?>
<div class="col-lg-3 col-6">
    <div class="small-box bg-success">
        <div class="inner">
            <h3><?php echo number_format($accesos["total"]); ?></h3>
            <p>Reportes</p>
        </div>
        <div class="icon">
            <i class="ion ion-clipboard"></i>
        </div>
        <a href="reportes" class="small-box-footer">Mas Información<i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

<?php $usuarios = ControladorUsuario::ctrTotalUsuarios();?>
<div class="col-lg-3 col-6">
    <div class="small-box bg-warning">
        <div class="inner">
            <h3><?php echo number_format($usuarios["total"]); ?></h3>
            <p>Usuarios</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a href="usuarios" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>

<?php 
    $actividades = ControladorActividades::ctrTotalActividades();
    $buzon = ControladorBuzon::ctrTotalBuzon();
    $negocios = ControladorNegocios::ctrTotalNegocios();
    $reservas = ControladorReservas::ctrTotalReservas();

    $TotalPublicacion = number_format($actividades["total"]) + number_format($buzon["total"]) + number_format($negocios["total"]) + number_format($reservas["total"]);
?>
<div class="col-lg-3 col-6">
    <div class="small-box bg-danger">
        <div class="inner">
            <h3><?php echo number_format($TotalPublicacion); ?></h3>
            <p>Publicaciones</p>
        </div>
        <div class="icon">
            <i class="ion ion-pie-graph"></i>
        </div>
        <a href="publicaciones" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
    </div>
</div>