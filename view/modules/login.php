<style>
.login-page,
.register-page {
  -ms-flex-align: center;
  align-items: center;
  background: url(public/img/playa.jpg);
  background-size: cover;
  -ms-flex-direction: column;
  flex-direction: column;
  height: 100%;
  -ms-flex-pack: center;
  justify-content: center;
  width:100%;
}
</style>

<div id="back"></div>
<div class="login-box">
  <div class="login-logo" style="position: relative">
  <img src="view/img/plantilla/logo_condominio.png" alt="Condominio Los Jardines" class="img-responsive" style="padding:auto;" height="150">
  </div>

  <div class="card card-outline card-primary">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Ingresar al sistema</p>

      <form name="numeros" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Usuario" name="ingUsuario" id="ingUsuario" required>
          <div class="input-group-append input-group-addon">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>

        <div class="input-group mb-3" id="divPassword">
          <input type="password" class="form-control" placeholder="Contraseña" name="ingPassword" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fa fa-eye"></span>
            </div>
          </div>
        </div>

        <div class="form-group">
          <select class="form-control select2 select2-hidden-accessible" name="ingRol[]" required>
            <?php
              $perfiles = new ModeloUsuarios();
              $rows = $perfiles -> mdlObtenerRolesLogin('seg_rol');
              foreach($rows as $row) {
                echo '<option value="'.$row['id'].'">'.$row['descripcion'].'</option>'; 
              }
            ?>
          </select>            
        </div>
        <div id="btnIngresarDiv">
            <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
        </div>

        <?php
            $login = new ControladorUsuario();
            $login -> ctrIngresoUsuario();
        ?>

      </form>
      <div id="btns" class="row">
          <div class="col-6">
            <button class="btn btn-danger btn-block" data-toggle="modal" data-target="#modalAlertar">Alertar</button>
          </div>
          <div class="col-6">
            <button class="btn btn-success btn-block" data-toggle="modal" data-target="#modalInvitado">Invitado</button>
          </div>
      </div>
    </div>
    <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
  </div>
</div>

<!-- Popup Alerta-->
<div class="modal fade" id="modalAlertar" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Alertar de Peligro</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group"> 
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user"></span>
                  </div>
                </div>  
                <input type="text" class="form-control col-lg-7" id="alertUsuario" name="alertUsuario" placeholder="Ingresar cédula" required>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-key"></span>
                  </div>
                </div>  
                <input type="text" id="codigoSeguridad" class="form-control col-sm-3" name="codigoSeguridad" placeholder="Código">
              </div>
            </div>
            <div class="form-group"> 
              <div class="input-group"> 
                <textarea id="alertDetalle" class="form-control rounded-0" rows="4" name="alertDetalle" placeholder="Detalles (Opcional) - Mínimo 6 palabras"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary swal">Guardar</button>
        </div>
        <?php
            $crearAlerta = new ControladorUsuario();
            $crearAlerta -> ctrCrearAlertaUsuario();
        ?>            
      </form>
    </div>
  </div>
</div>

<!-- Popup Invitado-->
<div class="modal fade" id="modalInvitado" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Ingrese sus datos</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group"> 
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user-circle"></span>
                  </div>
                </div>
                <input type="text" class="form-control col-lg" id="invCedula" name="invCedula" placeholder="Ingresar cédula" required>
              </div>
            </div>
            <div class="form-group"> 
              <div class="input-group">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fa fa-id-card"></span>
                  </div>
                </div>  
                <input type="text" id="invNombre" class="form-control col-lg" name="invNombre" placeholder="Ingresar Nombre" required>
              </div>
            </div>
            <div class="form-group"> 
              <div class="input-group">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-address-card"></span>
                  </div>
                </div>  
                <input type="text" id="invApellidos" class="form-control col-lg" name="invApellidos" placeholder="Ingresar Apellidos" required>
              </div>
            </div>
            <div class="form-group"> 
              <div class="input-group">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa fa-phone"></span>
                  </div>
                </div>  
                <input type="text" id="invTelefono" class="form-control col-lg" name="invTelefono" placeholder="Ingresar Teléfono" required>
              </div>
            </div>
            <div class="form-group"> 
              <div class="input-group">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>  
                <input type="text" id="invCorreo" class="form-control col-lg-10" name="invCorreo" placeholder="Ingresar Correo" required>
                <div id="divcorreo" class="btn btn-danger col-xs-2">
                  <span id="spancorreo" class="fa fa-window-close"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary swal">Ingresar</button>
        </div>
        <?php
            $ingresaInvitado = new ControladorUsuario();
            $ingresaInvitado -> ctrUsuarioInvitado();
        ?>            
      </form>
    </div>
  </div>
</div>