<?php
  if(!isset($_SESSION["idRol"])){
    echo '<script>
              window.location = "inicio";
            </script>';
            return;
  }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Actividades</h3> 
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
            <li class="breadcrumb-item active">Administrar actividades</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card card-outline card-dark">
      <div class="card-header">
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
        <form name="selectActividades" method="post" enctype="multipart/form-data">
            <div class="form-inline d-flex justify-content-start col-xs-10">
              <div class="col-xs-5">
                <select id="actividadPadre" name="actividadPadre" class="form-control select2 select2-hidden-accessible" required> 
                  <option value="0">Seleccione actividad</option>
                  <?php
                    $actividades = new ControladorActividades();
                    $rows = $actividades -> ctrConsultarActividades();
                    foreach($rows as $row)
                    {  echo '<option value="'.$row['id'].'">'.$row['tipo_actividad'].'</option>';  
                    }
                  ?>
                </select>
              </div>
              <div class="col-xs-5">
                <select id="actividadHijo" name="actividadHijo" class="form-control select2 select2-hidden-accessible" required>
                  <option value="">Seleccione Tipo de Actividad</option>
                </select>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group"> 
              <div class="input-group"> 
                <textarea id="actividadDetalle" name="actividadDetalle" class="form-control rounded-0" rows="3" placeholder="Describa la actividad que se llevará a cabo..." disabled></textarea>
              </div>
            </div>
            <div class="form-group d-flex justify-content-between align-items-center row">
              <div class="custom-file col-md-4 d-flex justify-content-start">
                <input type="file" class="custom-file-input fotoActividad" id="fotoActividad" name="fotoActividad" disabled>
                <label class="custom-file-label col-sm control-label labelActividad" for="customFile">Seleccione un archivo</label>
              </div>
              <div class="col-md-4 d-flex justify-content-center">
                <img src="" class="img-thumbnail previsualizarAct" width="75px" alt=""/>
                <input type="hidden" name="fotoActividadActual" id="fotoActividadActual">
                <input type="hidden" id="idActividadHidden" name="idActividadHidden" value="">
              </div>
              <?php
                $actividad = new ControladorActividades();
                $actividad -> ctrProcesarActividad();
              ?>
              <div class="btn-group col-md-4 d-flex justify-content-end">
                <div class="col-sm-6">
                  <button type="reset" class="btn btn-danger swal btncancelarActividad" disabled>Cancelar</button>
                </div>
                <div class="col-xs-6">
                  <button id="btnActividad" type="submit" class="btn btn-primary swal" estado="" disabled>Enviar</button>
                </div>
              </div>
            </div>       
        </form>
      </div>
      <div class="overlay dark">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
      </div>
    </div>    
  </section>

 <!-- Sección 2 -->
  <section class="content">
    <div class="card card-outline card-dark">
      <div class="card-header form-group" id="tablaPublica"> 
        <table class="table table-bordered table-hover dt-responsive nowrap dataTable_width-margin_auto" id="tablas">
          <thead>
            <tr>
              <th>#</th>
              <th>Foto</th>
              <th>Detalle</th>
              <th>Fecha</th>
              <th>Observación</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $tablas = "";
              $regActividades = ModeloActividades::mdlObtenerRegistroActividades($tablas);
              $contador = 1;
              foreach($regActividades as $value){
                if($value['estado'] == 2 || $value['estado'] == 5 || $value['estado'] == 10){
                  echo '<tr>
                          <td>'.$contador.'</td>
                          <td>
                            <div class="btn-group">
                              <button class="btn bg-olive" rutaImg="'.$value["ruta_img"].'" data-toggle="modal" data-target="#modalImgActividad" id="btnFotoAct"><i class="fas fa-images" aria-hidden="true"></i></button>
                            </div>
                          </td>
                          <td>'.$value["descripcion"].'</td>
                          <td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
                          <td>'.$value["observacion"].'</td>';
                          if($value["estado"] == 2){echo'<td><button class="btn btn-warning btn-sm">EN PROCESO</button></td>';}
                          elseif($value["estado"] == 10){echo'<td><button class="btn btn-success btn-sm">PUBLICADO</button></td>';}
                          elseif($value["estado"] == 5){echo'<td><button class="btn btn-danger btn-sm">RECHAZADO</button></td>';}
                          if($value["estado"] == 10){
                            echo  '<td>
                            <div class="btn-group">
                              <button class="btn btn-dark btnEditarActividad" disabled><i class="fa fa-cog" aria-hidden="true"></i></button>
                              <button class="btn btn-danger btnEliminarActividad" disabled><i class="fa fa-times"></i></button> 
                            </div>
                          </td>
                        </tr>';
                      }
                      elseif($value["estado"] == 2 || $value["estado"] == 5){
                        echo  '<td>
                        <div class="btn-group">
                          <button class="btn btn-dark btnEditarActividad" idActividad="'.$value["id"].'"><i class="fa fa-cog" aria-hidden="true"></i></button>
                          <button class="btn btn-danger btnEliminarActividad" idActividad="'.$value["id"].'" eliminarImgAct="'.$value["ruta_img"].'"><i class="fa fa-times"></i></button> 
                        </div>
                      </td>
                    </tr>';
                    }
                } $contador++;
              }
            ?>
          </tbody>
        </table>
      </div>
      <div class="overlay dark">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
      </div>
    </div>
  </section>
</div>

<!--========================================
            MODAL VER IMAGEN
===========================================-->
<div class="modal fade" id="modalImgActividad" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #3c8dbc; color:white ;">
        <h5 class="modal-title" id="exampleModalLabel">Imagen de la Actividad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <img src="" class="img-thumbnail modalFotoAct" alt="Imagen Actividad"/>
        </div>
      </div>       
    </div>
  </div>
</div>

<?php
  $borrarActividad = new ControladorActividades();
  $borrarActividad ->ctrBorrarActividad();
?>