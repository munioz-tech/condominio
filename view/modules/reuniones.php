<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reuniones Zoom</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar Reuniones</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <button class="btn btn-primary btnCrearReunion" data-toggle="modal" data-target="#modalCrearReunion">Crear Reunion</button> 

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body ">
          <section>
            <div class="form-group" id="tablaReuniones"> 
              <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
                <thead>
                  <tr>
                    <th>Fecha</th>
                    <th>Detalle</th>
                    <th>Estado</th>
                    <th>Acción</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $regReuniones = ModeloReuniones::mdlObtenerRegistroReuniones();
                    foreach($regReuniones as $value){
                      if($value["estado"] == 10){
                      echo '<tr>
                              <td>'.date("d-m-Y",strtotime($value["f_crea"])).'</td>
                              <td>'.str_replace("\n", "<br>", $value["tema"]).'</td>';
                              if($value["estado"] == 10){echo'<td><button class="btn btn-success btn-sm">PUBLICADO</button></td>
                          <td>
                            <div class="btn-group">
                              <button class="btn btn-danger btnEliminarReunion" idReunion="'.$value["id"].'"><i class="fa fa-times"></i></button> 
                            </div>
                          </td>
                        </tr>';
                        }  
                      }
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </section>
        </div>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- MODAL CREAR REUNON -->
<!--<div class="modal fade" id="modalCrearReunion" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #3c8dbc; color:white ;">
        <h5 class="modal-title" id="exampleModalLabel">Crear nueva reunión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <form name="FormReuniones" method="post">
          <div class="modal-body">
          <div class="box-body">
            <div class="form-group"> 
              <div class="input-group">
                <textarea name="reunionDetalle" class="form-control rounded-0 reunionDetalle" placeholder="Ingrese los datos obtenidos de la reunión creada" rows="7"></textarea>
              </div>
            </div>                     
          </div>
        </div>
            <?php
                $reuniones = new ControladorReuniones();
                $reuniones -> ctrRegistrarReunion();
            ?>
            <div class="modal-footer col-sm-12 justify-content-center">
              <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cerrar</button>
              <button type="submit" class="btn btn-primary swal" id="reunionesEnviar">Enviar</button>
            </div>
          </form>
        </div>
      </div>       
    </div>
  </div>
</div>-->




  <!-- MODAL CREAR REUNON -->
  <div class="modal fade" id="modalCrearReunion" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background: #3c8dbc; color:white ;">
        <h5 class="modal-title" id="exampleModalLabel">Crear nueva reunión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="box-body">
          <form name="FormReuniones" method="post">
            <div class="form-group"> 
              <div class="col-sm-12">
                <div class="input-group"> 
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="far fa-comments"></span>
                    </div>
                  </div>  
                  <input type="text" class="form-control input-lg" name="nuevoTema" id="nuevoTema" placeholder="Ingrese Tema" required>
                </div>
              </div>
            </div>

            <div class="form-group row center-items"> 
              <div class="col-sm-5">
                <div class="input-group"> 
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="far fa-calendar-times"></span>
                    </div>
                  </div>  
                  <input type="date" class="form-control input-lg" name="nuevoDia" id="nuevoDia" value="" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}" placeholder="Ingrese Fecha/Hora" required>
                </div>
              </div>
              <div class="col-sm-5">
                <div class="input-group"> 
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="far fa-calendar-times"></span>
                    </div>
                  </div>  
                  <input type="time" class="form-control input-lg" min="09:00" max="22:00" name="nuevaHora" id="nuevaHora" value="00:00" required>
                </div>
              </div>
            </div>

            <div class="form-group input">
              <div class="col-sm-12">
                <div class="input-group" id="reuPass"> 
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-unlock"></span>
                    </div>
                  </div>  
                  <input type="password" class="form-control input-lg" name="nuevoPassword" id="nuevoPassword" placeholder="Ingresar Contraseña" required>
                  <div class="input-group-append">
                    <div class="btn btn-danger">
                      <span class="fa fa-eye" id="confEyes"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php
                $reuniones = new ControladorReuniones();
                $reuniones -> ctrCrearReunion();
            ?>
            <div class="modal-footer col-sm-12 justify-content-center">
              <button type="submit" class="btn btn-primary btn-lg swal" id="reunionesEnviar" disabled>Enviar</button>
            </div>
          </form>
        </div>
      </div>       
    </div>
  </div>
</div>

<?php
  $borrarReunion = new ControladorReuniones();
  $borrarReunion ->ctrBorrarReunion();
?>