<?php

    if(!isset($_SESSION["idRol"])){
      echo '<script>
                window.location = "inicio";
              </script>';
              return;
    }
  if($_SESSION['idRol'] != 1){
    echo '<script>
            window.location = "inicio";
          </script>';
          return;
  }
?>
<div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3>Administrar usuarios</h3> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar usuarios</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="card card-outline card-dark">
        <div class="card-header">
          <button class="btn btn-primary btnAgregarUsuario" data-toggle="modal" data-target="#modalAgregarUsuario">Agregar Usuario</button>      
        </div>
        <div class="card-body">
          <table class="table table-bordered table-hover dt-responsive dataTable_width-margin_auto" id="tablas">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Cédula</th>
                <th>Nombre Inquilino</th>
                <th>Foto</th>
                <th>Rol</th>
                <th>Edificio</th>
                <th>Hab</th>
                <th>Estado</th>
                <th>Acciones</th>
              </tr>
              </thead>
              <tbody> 
                  <?php
                    $count = 1;
                    $item = "";
                    $valor = "";
                    $usuarios = ControladorUsuario::ctrMostrarUsuario($item, $valor);
                    foreach ($usuarios as $key => $value){
                        if($value["estado"] == 1 || $value["estado"] == 3 || $value["estado"] == 9){
                            echo '  <tr>
                                    <td>'.$count.'</td>
                                    <td>'.$value["cedula"].'</td>
                                    <td>'.$value["nombre"].' '.$value["apellidos"].'</td>';

                            if($value["foto"] != ""){
                                echo '<td><img src="'.$value["foto"].'" class="img-thumbnail" width="40px" alt="imagen usuario"></td>';
                            }else{
                                echo '<td><img src="view/img/usuarios/default/default_user.png" class="img-thumbnail" width="40px" alt="imagen usuario"></td>';
                            }

                            echo'   <td>'.$value["rol"].'</td>';
                            echo'   <td>'.$value["n_edificio"].'</td>';
                            echo'   <td>'.$value["n_habitacion"].'</td>';
                            if($value["estado"] == 1){            echo '<td><button class="btn btn-success btn-xs btnActivar" idUsuario="'.$value["cedula"].'" idUsuarioModifica="'.$_SESSION["idUsuario"].'" estadoUsuario="3" estado="1">Activado</button></td>';   }
                            else if($value["estado"] == 3){       echo '<td><button class="btn btn-danger btn-xs btnActivar" idUsuario="'.$value["cedula"].'" idUsuarioModifica="'.$_SESSION["idUsuario"].'" estadoUsuario="1" estado="3">Desactivado</button></td>';   }
                            else if($value["estado"] == 9){       echo '<td><button class="btn btn-warning btn-xs btnActivar" idRol="'.$value["id_rol"].'" idUsuario="'.$value["cedula"].'" idUsuarioModifica="'.$_SESSION["idUsuario"].'" estadoUsuario="1" estado="9">Bloqueado</button></td>';   }

                            echo'   <td>
                                      <div class="btn-group">
                                        <button class="btn bg-orange btnEditarUsuario" cedula="'.$value["cedula"].'" idUsuario="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarUsuario"><i class="fa fa-cog" aria-hidden="true"></i></button>
                                        <button class="btn btn-danger btnEliminarUsuario" cedula="'.$value["cedula"].'" idUsuario="'.$value["id"].'" fotoUsuario="'.$value["foto"].'"><i class="fa fa-times"></i></button> 
                                      </div>
                                    </td>
                                  </tr>';
                                  $count++;
                        }
                    }
                  ?>
              </tbody>
          </table>
        </div>
        <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
      </div>
    </section>
  </div>

<!-- ========================================
            MODAL CREAR USUARIOS
===========================================-->

<div class="modal fade" id="modalAgregarUsuario" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color:white ;">
              <h5 class="modal-title" id="exampleModalLabel">Agregar Usuario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="box-body">
                  <div class="form-group row"> 
                        <div class="col-7 row">
                          <div class="input-group"> 
                            <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-users"></span>
                              </div>
                            </div>  
                            <select name="nuevoPerfil" class="form-control input-lg">
                              <option value="1">Administrador</option>
                              <option value="2">Inquilino</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-5">
                          <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-key"></span>
                                </div>
                            </div>  
                            <input type="text" class="form-control input-lg" name="nuevaCedula" id="nuevaCedula" placeholder="Ingresar Cedula" required>
                          </div>
                        </div>
                  </div>

                  <div class="form-group row"> 
                        <div class="col-5">
                            <div class="input-group"> 
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-user"></span>
                                  </div>
                              </div>  
                            <input type="text" class="form-control input-lg" name="nuevoNombre" id="nuevoNombre" placeholder="Ingrese Nombre" required>
                          </div>
                        </div>
                        
                        <div class="col-7 row">
                          <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>  
                          <input type="text" class="form-control input-lg" name="nuevoApellido" id="nuevoApellido" placeholder="Ingresar Dos Apellidos" required>
                        </div>
                        </div>
                  </div>

                  <div class="form-group" id="Password"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                          </div>  
                          <input type="password" class="form-control input-lg" name="nuevoPassword" id="nuevoPassword" placeholder="Ingresar Contraseña" required>
                          <div class="input-group-append">
                            <div class="btn btn-danger col-xs-2">
                              <span class="fa fa-eye" id="eyes"></span>
                            </div>
                          </div>
                      </div>
                  </div>

                  <div class="form-group row"> 
                        <div class="col-md-3">
                            <div class="input-group"> 
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-building"></span>
                                    </div>
                                </div>  
                                <input type="text" class="form-control input-lg" name="nuevoEdificio" id="nuevoEdificio" placeholder="Edificio" required>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="input-group"> 
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-door-closed"></span>
                                    </div>
                                </div>  
                                <input type="text" class="form-control input-lg" name="nuevaHabitacion" id="nuevaHabitacion" min="0" placeholder="Hab." required>
                            </div>
                        </div>

                        <div class="col-md-6">
                              <div class="input-group"> 
                                  <div class="input-group-append">
                                      <div class="input-group-text">
                                          <span class="fas fa-phone-alt"></span>
                                      </div>
                                  </div>  
                                  <input type="text" class="form-control input-lg" name="nuevoTelefono" id="nuevoTelefono" min="0" placeholder="Ingrese Telefono" required>
                              </div>
                          </div>
                  </div>

                  <div class="form-group"> 
                        <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-at"></span>
                              </div>
                          </div>  
                          <input type="email" class="form-control input-lg" name="nuevoEmail" id="nuevoEmail" placeholder="Ingresar Email" required>
                          <div id="nuevoDivCorreo" class="btn btn-danger col-xs-2">
                            <span id="nuevoSpanCorreo" class="fa fa-window-close"></span>
                          </div>
                        </div>
                  </div>

                  <div class="form-group"> 
                      <div class="panel">Subir foto</div>
                      <div class="custom-file">
                          <input type="file" class="custom-file-input nuevaFoto" id="nuevaFoto" name="nuevaFoto">
                          <label class="custom-file-label" for="customFile">Seleccione un archivo</label>
                      </div>
                      <small class="form-text text-muted">Peso máximo de la foto 2mb</small>
                      <img src="view/img/usuarios/default/default_user.png" class="img-thumbnail previsualizar" width="100px" alt="Imagen Usuario"/>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary swal">Guardar</button>
            </div>
            <?php
                $crearUsuario = new ControladorUsuario();
                $crearUsuario -> ctrCrearUsuario();
            ?>
        </form>
    </div>
  </div>
</div>

<!--========================================
            MODAL EDITAR USUARIOS
===========================================-->

<div class="modal fade" id="modalEditarUsuario" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <form role="form" method="post" enctype="multipart/form-data">
            <div class="modal-header" style="background: #3c8dbc; color:white ;">
              <h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="box-body">
                  <div class="form-group row"> 
                        <div class="col-7 row">
                          <div class="input-group"> 
                            <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-users"></span>
                              </div>
                            </div>  
                            <select name="editarPerfil" id="editarPerfil" class="form-control input-lg">
                              <option value="1">Administrador</option>
                              <option value="2">Inquilino</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-5">
                          <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-key"></span>
                                </div>
                            </div>  
                            <input type="text" class="form-control input-lg" name="editarCedula" id="editarCedula" placeholder="Ingresar Cedula" required>
                            <input  type="hidden" id="cedulaActual" name="cedulaActual" value="">
                          </div>
                        </div>
                  </div>

                  <div class="form-group row"> 
                        <div class="col-5">
                            <div class="input-group"> 
                              <div class="input-group-append">
                                  <div class="input-group-text">
                                      <span class="fas fa-user"></span>
                                  </div>
                              </div>  
                            <input type="text" class="form-control input-lg" name="editarNombre" id="editarNombre" placeholder="Ingresar Nombre" required>
                          </div>
                        </div>
                        
                        <div class="col-7 row">
                          <div class="input-group"> 
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                </div>
                            </div>  
                          <input type="text" class="form-control input-lg" name="editarApellido" id="editarApellidos" placeholder="Ingresar Apellidos" required>
                        </div>
                        </div>
                  </div>

                  <div class="form-group" id="PasswordEditar"> 
                      <div class="input-group"> 
                          <div class="input-group-append">
                              <div class="input-group-text">
                                  <span class="fas fa-lock"></span>
                              </div>
                          </div>  
                          <input type="password" class="form-control input-lg" name="editarPassword" id="editarPassword" placeholder="Ingresar Nueva Contraseña">
                          <div class="input-group-append">
                            <div class="btn btn-danger col-xs-2">
                              <span class="fa fa-eye" id="eyesEditar"></span>
                            </div>
                          </div>
                          <input type="hidden" id="passwordActual" name="passwordActual">
                    </div>
                  </div>

                  <div class="form-group row"> 
                        <div class="col-md-3">
                            <div class="input-group"> 
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-building"></span>
                                    </div>
                                </div>  
                                <input type="text" class="form-control input-lg" name="editarEdificio" id="editarEdificio" placeholder="Ingrese Edificio" required>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="input-group"> 
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-door-closed"></span>
                                    </div>
                                </div>  
                                <input type="text" class="form-control input-lg" name="editarHabitacion" id="editarHabitacion" min="0" placeholder="Ingrese Habitacion" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group"> 
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-phone-alt"></span>
                                    </div>
                                </div>  
                                <input type="text" class="form-control input-lg" name="editarTelefono" id="editarTelefono" min="0" placeholder="Ingrese Telefono" required>
                            </div>
                        </div>
                  </div>

                  <div class="form-group"> 
                    <div class="input-group"> 
                      <div class="input-group-append">
                          <div class="input-group-text">
                              <span class="fas fa-at"></span>
                          </div>
                      </div>  
                      <input type="email" class="form-control input-lg" name="editarEmail" id="editarEmail" placeholder="Ingresar Email" required>
                      <div id="editarDivCorreo" class="btn btn-success col-xs-2">
                        <span id="editarSpanCorreo" class="fa fa-check-square"></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group"> 
                      <div class="panel">Subir foto</div>
                      <div class="custom-file">
                          <input type="file" class="custom-file-input nuevaFoto" id="editarFoto" name="editarFoto">
                          <label class="custom-file-label" for="customFile">Seleccione un archivo</label>
                      </div>
                      <small class="form-text text-muted">Peso máximo de la foto 2mb</small>
                      <img src="view/img/usuarios/default/default_user.png" class="img-thumbnail previsualizar" width="100px" alt="Imagen Usuario"/>
                      <input type="hidden" name="fotoActual" id="fotoActual">
                  </div>

               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-primary swal">Guardar</button>
            </div>       
            <?php
                $editarUsuario = new ControladorUsuario();
                $editarUsuario -> ctrEditarUsuario();
            ?>
        </form>
    </div>
  </div>
</div>

<?php
    $borrarUsuario = new ControladorUsuario();
    $borrarUsuario ->ctrBorrarUsuario();
?>