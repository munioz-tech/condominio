<?php
  if(!isset($_SESSION["idRol"])){
    echo '<script>
              window.location = "inicio";
            </script>';
            return;
  }
?>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h3>Configuración</h3>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
            <li class="breadcrumb-item active">Configurar usuario</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="card card-outline card-dark">
      <div class="card-header">
        <h3 class="card-title">Datos del Usuario</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
        </div>
      </div>
      <div class="card-body d-inline-flex justify-content-center">
        <form method="post" id="configuracion" idConfiguracion="<?php echo $_SESSION["idUsuario"]; ?>" enctype="multipart/form-data">
          <div class="form-group row"> 
            <div class="col-sm-4">
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user"></span>
                  </div>
                </div>  
                <input type="text" class="form-control input-lg" name="confNombre" id="confNombre" placeholder="Ingrese Nombre">
              </div>
            </div>
            <div class="col-sm-6 row">
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user"></span>
                  </div>
                </div>  
                <input type="text" class="form-control input-lg" name="confApellido" id="confApellido" placeholder="Ingresar Dos Apellidos">
              </div>
            </div>
          </div>

          <div class="form-group row"> 
            <div class="col-sm-3">
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-building"></span>
                  </div>
                </div>  
                <input type="text" class="form-control input-lg" name="confEdificio" id="confEdificio" placeholder="Edificio">
              </div>
            </div>
            <div class="col-sm-3">
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-door-closed"></span>
                  </div>
                </div>  
                <input type="text" class="form-control input-lg" name="confHabitacion" id="confHabitacion" min="0" placeholder="Habitación">
              </div>
            </div>
            <div class="col-sm-4">
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-phone-alt"></span>
                  </div>
                </div>
                <input type="text" class="form-control input-lg" name="confTelefono" id="confTelefono" min="0" placeholder="Ingrese Telefono">
              </div>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-10">
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-at"></span>
                  </div>
                </div>  
                <input type="email" class="form-control input-lg" name="confEmail" id="confEmail" placeholder="Ingresar Email">
                <div id="confDivCorreo" class="btn btn-danger col-xs-2">
                  <span id="confSpanCorreo" class="fa fa-window-close"></span>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group row"> 
            <div class="panel col-sm-3">Subir foto</div>
            <div class="custom-file col-sm-10">
              <div class="input-group"> 
                <input type="file" class="custom-file-input confFoto" id="confFoto" name="confFoto">
                <label class="custom-file-label" for="customFile">Seleccione un archivo</label>
              </div>
            </div>
          </div>
            <div class="form-group row">
              <div class="panel col-sm-5 d-flex justify-content-center">
                <small class="form-text text-muted">Peso máximo de la foto 2mb</small>
              </div>
              <div class="col-sm-10 d-flex justify-content-center">
                <img class="img-thumbnail previsualizarconf d-flex justify-content-center" src="" alt="Imagen Usuario" width="200px"/>
                <input type="hidden" name="conffotoActual" id="conffotoActual">
              </div>
            </div>
            <?php
              $configuracion = new ControladorConfiguraciones();
              $configuracion -> ctrEditarDatosUsuario();
            ?>
          <div class="modal-footer col-sm-10">
            <button type="button" class="btn btn-warning btnconfClave" data-toggle="modal" data-target="#modalClave">Cambiar Clave</button>
            <button type="submit" class="btn btn-primary swal" id="confEditar">Guardar</button>
          </div>
        </form>
      </div>
      <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
    </div>
  </section>
</div>

<!-- ========================================
          MODAL CAMBIO DE CLAVE
===========================================-->
<div class="modal fade" id="modalClave" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Cambiar Contraseña</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group"> 
              <div class="input-group">
                <p class="text-secondary">Su contraseña debe tener mínimo 7 caracteres, una letra mayúscula, una minúscula y un número.</p>
                <div class="form-group col-md-12 row" id="confPass"> 
                  <div class="col-sm-12">
                    <div class="input-group"> 
                      <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-lock"></span>
                        </div>
                      </div>  
                      <input type="password" class="form-control input-lg" name="confPassword" id="confPassword" placeholder="Ingresar Nueva Contraseña">
                      <div class="input-group-append">
                        <div class="btn btn-danger col-xs-2">
                          <span class="fa fa-eye" id="confEyes"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>                     
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary btnconfAceptar" name="btnconfAceptar">Aceptar</button>
        </div>
        <?php
            $clave = new ControladorConfiguraciones();
            $clave -> ctrCambiarClaveUsuario();
        ?>
      </form>
    </div>
  </div>
</div>
