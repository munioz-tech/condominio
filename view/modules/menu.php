<aside class="main-sidebar sidebar-dark-primary elevation-4 position-fixed">
    <!-- Brand Logo -->
    <a href="inicio" class="brand-link">
      <img src="view/img/plantilla/condominio_blanco.png" alt="Los Jardines" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Los Jardines</span>
    </a>

    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview">
                    <a href="inicio" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Inicio
                        </p>
                    </a>
                <li>
                <li class="nav-item">
                    <a href="noticias" class="nav-link">
                        <i class="nav-icon fas fa-newspaper"></i>
                        <p>
                            Noticias
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="contactos" class="nav-link">
                        <i class="nav-icon fas fa-id-card-alt"></i>
                        <p>
                            Contactos
                        </p>
                    </a>
                </li>
                <?php 
                    if (isset($_SESSION["idRol"])){
                        if($_SESSION['idRol'] == 1 || $_SESSION['idRol'] == 2){
                            echo '  <li class="nav-item">
                                        <a href="actividades" class="nav-link">
                                            <i class="nav-icon fas fa-laugh-beam"></i>
                                            <p>
                                                Actividades
                                            </p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="reservas" class="nav-link">
                                            <i class="nav-icon fa fa-address-book"></i>
                                            <p>
                                                Reservas
                                            </p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="negocios" class="nav-link">
                                            <i class="nav-icon fab fa fa-briefcase"></i>
                                            <p>
                                                Negocios
                                            </p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="buzon" class="nav-link">
                                            <i class="nav-icon fab fa fa-archive"></i>
                                            <p>
                                                Buzón
                                            </p>
                                        </a>
                                    </li>';
                        }
                    }
                ?>
                <?php 
                if (isset($_SESSION["idRol"]) && $_SESSION['idRol'] == 1){
                echo '  <li class="nav-item has-treeview menu-close">
                            <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-list-ul"></i>
                            <p>
                                Administración
                                <i class="right fas fa-angle-left"></i>
                            </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="usuarios" class="nav-link">
                                    <i class="far fa fa-user-circle nav-icon"></i>
                                    <p>Usuarios</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="publicaciones" class="nav-link">
                                    <i class="far fa fa-check-square nav-icon"></i>
                                    <p>Publicaciones</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="reportes" class="nav-link">
                                    <i class="far fa fa-book nav-icon"></i>
                                    <p>Reportes</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="reuniones" class="nav-link">
                                    <i class="fas fa fa-users nav-icon"></i>
                                    <p>Reuniones</p>
                                    </a>
                                </li>
                            </ul>
                        </li>';
                    } 
                ?>
                 <?php 
                if (isset($_SESSION["idRol"])){
                    if($_SESSION['idRol'] == 1 || $_SESSION['idRol'] == 2){
                        echo '
                            <li class="nav-item">
                                <a href="configuracion" class="nav-link configuracion">
                                    <i class="nav-icon fab fa fa-cog"></i>
                                    <p>
                                        Configuración
                                    </p>
                                </a>
                            </li>';
                    }
                }
                ?>
            </ul>
        </nav>
    </div>
</aside>