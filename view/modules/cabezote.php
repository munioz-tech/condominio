<nav class="main-header navbar navbar-expand navbar-white navbar-light d-flex justify-content-between sticky-top md-6">
  <div class="col-md-5 d-flex justify-content-start">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="contactos" class="nav-link">Contactos</a>
      </li>
      <li class="nav-item d-none d-lg-inline-block">
        <a href="#" class="nav-link">
              <?php ini_set('date.timezone','America/Guayaquil'); $fcha = date("d/m/Y"); echo utf8_encode($fcha);?>
              <input class="reloj" type="text" id="reloj" size="5" onfocus="window.document.form_reloj.reloj.blur()" disabled>
        </a>
      </li>
    </ul>
    
  </div>
  
  <?php if(isset($_SESSION["idRol"])){
  echo '<div class="col-md-2 d-flex justify-content-center">
    <button class="btn btn-danger btn-block" data-toggle="modal" data-target="#modalAlertar">Alertar</button>
  </div>';}
  ?>

  <!-- Right navbar links -->
  <div class="col-md-5 d-flex justify-content-end">
    <ul class="nav navbar-nav ml-auto">
      <li class="dropdown user user-menu">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <?php 
               if(isset($_SESSION["foto"])){
                 if($_SESSION["foto"] != ""){ echo '<img src="'.$_SESSION["foto"].'" class="user-image">';} 
                 else {echo '<img src="view/img/usuarios/default/default_user.png" class="user-image">'; }
              }else {echo '<img src="view/img/usuarios/default/default_user.png" class="user-image">';}
            ?>
          <span class="d-none d-sm-inline-block">
            <?php 
              echo $_SESSION["nombreUsuario"].' '.$_SESSION["apellidosUsuario"];
            ?> 
          </span>
        </a>
        <!-- DROPDOWN TOGGLE -->
        <ul class="dropdown-menu">
            <?php 
                if(isset($_SESSION["idRol"])){
                  if($_SESSION['idRol'] == 1 || $_SESSION['idRol'] == 2){
                      echo '<div class="center-block dropdown-item">
                              <a href="configuracion" class="btn btn-default btn-flat configuracion" style="width:200px; margin:5px 25px 5px;">
                                <i class="nav-icon fab fa fa-cog"></i>
                                Configuración
                              </a>
                            </div>';
                  }
                }
            ?>
            <div class="center-block dropdown-item">
              <a href="salir" class="btn btn-default btn-flat" style="width:200px; margin:0px 25px 5px;">Salir</a>
            </div>
        </ul>
      </li>
    </ul>
  </div>
</nav>

<!-- Popup Alerta-->
<div class="modal fade" id="modalAlertar" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form role="form" method="post" enctype="multipart/form-data">
        <div class="modal-header" style="background: #3c8dbc; color:white ;">
          <h5 class="modal-title" id="exampleModalLabel">Alertar de Peligro</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group"> 
              <div class="input-group"> 
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-key"></span>
                  </div>
                </div>  
                <input type="text" id="codigoSeguridad" class="form-control col-sm-3" name="codigoSeguridad" placeholder="Código">
              </div>
            </div>
            <div class="form-group"> 
              <div class="input-group"> 
                <textarea id="alertDetalle" class="form-control rounded-0" rows="4" name="alertDetalle" placeholder="Detalles (Opcional) - Mínimo 6 palabras"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary swal">Guardar</button>
        </div>
        <?php
            $crearAlerta = new ControladorUsuario();
            $crearAlerta -> ctrCrearAlertaUsuario();
        ?>            
      </form>
    </div>
  </div>
</div>