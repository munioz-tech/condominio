<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3>Noticias</h3> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Noticias</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="card card-outline card-dark">
            <div class="card-header">
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
                <?php
                if (isset($_SESSION["idRol"])){
                if($_SESSION['idRol'] == 1 || $_SESSION['idRol'] == 2){
                echo '<div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="5">Reuniones</button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="4">Buzón</button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="1">Actividades</button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="2">Reservas</button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="3">Negocios</button>
                </div>
                <div class="btn-group">
                    <button id="mostrarNoticia" class="btn btn-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="0">All</button>
                </div>';
                }}else{
                echo '<div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="1">Actividades</button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="2">Reservas</button>
                </div>
                <div class="btn-group">
                    <button class="btn btn-outline-secondary col-xs-2 mostrarNoticia" idMostrarNoticia="3">Negocios</button>
                </div>';
                }
                ?>
            </div>
            <div class="card-body noticias row">
            <?php
            if (isset($_SESSION["idRol"])){
                if($_SESSION['idRol'] == 1 || $_SESSION['idRol'] == 2){
                    $noticiaReunion = new ControladorNoticias();
                    $noticiaReu = $noticiaReunion -> ctrMostrarNoticiaReunion();
                    $noticiaBuzon = new ControladorNoticias();
                    $noticiaBuz = $noticiaBuzon -> ctrMostrarNoticiaBuzon();
                    $noticiaReservas = new ControladorNoticias();
                    $noticiaRes = $noticiaReservas -> ctrMostrarNoticiaReservas();
                    $noticiaActividades = new ControladorNoticias();
                    $noticiaAct = $noticiaActividades -> ctrMostrarNoticiaActividades();
                    $noticiaNegocios = new ControladorNoticias();
                    $noticiaNeg = $noticiaNegocios -> ctrMostrarNoticiaNegocios();
                    $DataCollection = array();
                }
            }else{
                $noticiaReservas = new ControladorNoticias();
                $noticiaRes = $noticiaReservas -> ctrMostrarNoticiaReservas();
                $noticiaActividades = new ControladorNoticias();
                $noticiaAct = $noticiaActividades -> ctrMostrarNoticiaActividades();
                $noticiaNegocios = new ControladorNoticias();
                $noticiaNeg = $noticiaNegocios -> ctrMostrarNoticiaNegocios();
                $DataCollection = array();
            }

                if (isset($_SESSION["idRol"])){
                    if($_SESSION['idRol'] == 1 || $_SESSION['idRol'] == 2){
                        foreach($noticiaReu as $val => $valor){$DataCollection[] = $valor;}
                        foreach($noticiaBuz as $val => $valor){$DataCollection[] = $valor;}
                        foreach($noticiaRes as $val => $valor){$DataCollection[] = $valor;}
                        foreach($noticiaAct as $val => $valor){$DataCollection[] = $valor;}
                        foreach($noticiaNeg as $val => $valor){$DataCollection[] = $valor;}
                    }}else{
                        foreach($noticiaRes as $val => $valor){$DataCollection[] = $valor;}
                        foreach($noticiaAct as $val => $valor){$DataCollection[] = $valor;}
                        foreach($noticiaNeg as $val => $valor){$DataCollection[] = $valor;}
                    }

                usort($DataCollection, function($a, $b){return strcmp($b["f_crea"], $a["f_crea"]);});
                if (isset($_SESSION["idRol"])){
                    if($_SESSION['idRol'] == 1 || $_SESSION['idRol'] == 2){
                foreach($DataCollection as $key => $value){
                    if($key <= 20){
                        echo '<div class="column col-sm-6">
                                <div class="card card-outline card-dark">';
                                if(isset($value["idTipo"]) && $value["idTipo"] == 1){
                                echo '<div class="ribbon-wrapper ribbon-xl">
                                        <div class="ribbon bg-info text-lg">
                                            '.$value["tipo_publicacion"].'
                                        </div>
                                    </div>';
                                }
                                if(isset($value["idTipo"]) && $value["idTipo"] == 2){
                                    echo '<div class="ribbon-wrapper ribbon-xl">
                                            <div class="ribbon bg-navy text-lg">
                                                '.$value["tipo_publicacion"].'
                                            </div>
                                        </div>';
                                }
                                if(isset($value["idTipo"]) && $value["idTipo"] == 3){
                                    echo '<div class="ribbon-wrapper ribbon-xl">
                                            <div class="ribbon bg-purple text-lg">
                                                '.$value["tipo_publicacion"].'
                                            </div>
                                        </div>';
                                }
                                if(isset($value["queja_idea"]) && $value["idTipo"] == 4){
                                    if($value["queja_idea"] == 1){
                                        echo '<div class="ribbon-wrapper ribbon-lg">
                                        <div class="ribbon bg-orange text-lg">
                                        '.utf8_decode(utf8_encode($value["tipo_publicacion"])).'
                                        </div></div>';
                                    }
                                    if($value["queja_idea"] == 2){
                                        echo '<div class="ribbon-wrapper ribbon-lg">
                                        <div class="ribbon bg-olive text-lg">
                                        '.utf8_decode(utf8_encode($value["tipo_publicacion"])).'
                                        </div></div>';
                                    }
                                }
                                if(isset($value["idTipo"]) && $value["idTipo"] == 5){
                                    echo '<div class="ribbon-wrapper ribbon-xl">
                                            <div class="ribbon bg-maroon text-lg">
                                            '.utf8_decode(utf8_encode($value["tipo_publicacion"])).'
                                            </div>
                                        </div>';
                                }

                        if(isset($value["ruta_img"])){echo '<center><img src="'.$value["ruta_img"].'" alt="Sin imagen de publicación" style="width: 35%; height: 100%"></center>';}
                            echo '<div class="container">';
                            if(isset($value["tema"])){
                                echo '<h2>Convocatoria a Reunión</h2>
                                <p>El comité directivo del Condominio los convoca a una reunión mediante la plataforma Zoom con tema: </p>
                                <p>'.str_replace("\n", "<br>", $value["tema"]).'</p>
                                <p><a href="'.$value["url"].'">Enlace a reunión</a></p>
                                <p>Con fecha y hora: '.$value["fecha_inicio"].'</p>';
                            }
                            if(isset($value["id_act_padre"])){
                                if($value["id_act_padre"] == 1){echo '<h2>Mantenimiento - '.$value["tipo_actividad"].'</h2>';}
                                if($value["id_act_padre"] == 2){echo '<h2>Extravío - '.$value["tipo_actividad"].'</h2>';}
                                if($value["id_act_padre"] == 3){echo '<h2>Varios - '.$value["tipo_actividad"].'</h2>';}
                            }
                            if(isset($value["id_cancha"])){
                                if($value["id_cancha"] == 1){
                                    echo '<h2>Cancha: Tenis</h2>';
                                    echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                                    echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                                }
                                if($value["id_cancha"] == 2){
                                    echo '<h2>Cancha: Indor</h2>';
                                    echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                                    echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                                }
                                if($value["id_cancha"] == 3){
                                    echo '<h2>Cancha: Voley</h2>';
                                    echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                                    echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                                }
                                if($value["id_cancha"] == 4){
                                    echo '<h2>Cancha: Básquet</h2>';
                                    echo '<p class="title">Desde: '.$value["fyh_desde"].'</p>';
                                    echo '<p class="title">Hasta: '.$value["fyh_hasta"].'</p>';
                                }
                            }
                            if(isset($value["id_negocio"])){
                                if($value["id_negocio"] == 1){
                                    echo '<h2>Venta: '.$value["t_negocio"].'</h2>';
                                    echo '<p class="title">Precio: $'.$value["precio"].'</p>';
                                }
                                if($value["id_negocio"] == 2){
                                    echo '<h2>Alquiler: '.$value["t_negocio"].'</h2>';
                                    echo '<p class="title">Precio: $'.$value["precio"].'</p>';
                                }
                                if($value["id_negocio"] == 3){
                                    echo '<h2>Curso: '.$value["t_negocio"].'</h2>';
                                    echo '<p class="title">Precio: $'.$value["precio"].'</p>';
                                }
                            }
                            if(isset($value["queja_idea"])){
                                if($value["queja_idea"] == 1){echo '<h2>Queja</h2>';}
                                if($value["queja_idea"] == 2){echo '<h2>Idea</h2>';}
                            }
                            if(isset($value["descripcion"])){echo '<p>'.$value["descripcion"].'</p>';}
                            if(isset($value["nombre"])){
                                echo '<div class="modal-footer d-flex justify-content-between">
                                        <div class="d-flex align-items-start">
                                            <p class="tittle">Autor: '.$value["nombre"].' '.$value["apellidos"].'</p>
                                        </div>
                                    </div>';
                            }
                            echo '</div>
                            <div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>
                            </div>
                        </div>';
                    }
                }}}
                else{
                    $noticiaActividades = new ControladorNoticias();
            $noticiaAct = $noticiaActividades -> ctrMostrarNoticiaActividades();
            foreach($noticiaAct as $actividades => $valueAct){
                echo '<div class="column col-sm-6">
                    <div class="card card-outline card-dark">
                    <div class="ribbon-wrapper ribbon-xl">
<div class="ribbon bg-info text-lg">
'.$valueAct["tipo_publicacion"].'
</div>
</div>
                        <center><img src="'.$valueAct["ruta_img"].'" alt="Actividad" style="width: 35%; height: 100%"></center>
                        <div class="container">';
                            if($valueAct["id_act_padre"] == 1){echo '<h2>Mantenimiento - '.$valueAct["tipo_actividad"].'</h2>';}
                            if($valueAct["id_act_padre"] == 2){echo '<h2>Extravío - '.$valueAct["tipo_actividad"].'</h2>';}
                            if($valueAct["id_act_padre"] == 3){echo'<h2>Varios - '.$valueAct["tipo_actividad"].'</h2>';}
                            echo '<p>'.$valueAct["descripcion"].'</p>
                            <div class="modal-footer d-flex justify-content-between">
                                <div class="d-flex align-items-start">
                                    <p class="tittle">Autor: '.$valueAct["nombre"].' '.$valueAct["apellidos"].'</p>
                                </div>
                                <div class="d-flex align-items-end">
                                    <input type="hidden" idMostrarNoticia="'.$valueAct["idTipo"].'">
                                </div>
                            </div>
                        </div>
                        <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
                    </div>
                </div>';
            }
                }
            ?>
            </div>
            <div class="overlay dark">
                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
            </div>
        </div>
    </section>
</div>