<style>
    /* Three columns side by side */
.column {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 0 8px;
}

/* Display the columns below each other instead of side by side on small screens */
@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

/* Add some shadows to create a card effect */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

/* Some left and right padding inside the container */
.container {
  padding: 0 16px;
}

/* Clear floats */
.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}

.carousel-control-prev-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23f00' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
}

.carousel-control-next-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23f00' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3>Inicio <span style="font-size:15px;color:#A9A9A9;">Panel de Control</span></h3> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Tablero</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-outline card-dark">
        <div class="card-header">
          ¡Hola, <?php 
          echo $_SESSION["nombreUsuario"];
          ?> &#128522; es un placer saludarte!
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <?php 
            if (isset($_SESSION["idRol"])){
              if($_SESSION['idRol'] == 1){
                echo '<section class="content"> 
                        <div class="row">';
                          include "inicio/cajas-superiores.php";
                echo '  </div>
                        <div class="col-lg-12">';
                          include "inicio/grafico-acceso.php";
                echo '  </div>
                        <div class="row">
                          <div class="col-lg-6">';
                          include "inicio/grafico-publicaciones.php";
                echo '    </div>
                          <div class="col-lg-6">';
                          include "inicio/noticias-recientes.php";
                echo '    </div>
                        </div>
                      </section>';
              }  
              if($_SESSION['idRol'] == 2){
                  $actividad = "'actividades'"; $reserva = "'reservas'";$negocio = "'negocios'"; $buzon = "'buzon'";
                  echo '<h4>¿QUÉ DESEAS HACER? 🤔</h4>
                        <section class="content"> 
                          <div class="row">
                              <div class="column col-md-3">
                                  <div class="card card-outline card-dark">
                                    <center><img src="public/img/actividades.png" alt="Actividades" style="width:60%"></center>
                                    <div class="container">
                                        <h2 align="center">Actividad</h2>
                                        <p><button class="button" onclick="location.href='.$actividad.'">Ir a Actividades</button></p>
                                    </div>
                                    <div class="overlay dark">
                                      <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                    </div>
                                  </div>
                              </div>

                              <div class="column col-md-3">
                                  <div class="card card-outline card-dark">
                                    <center><img src="public/img/reserva.png" alt="Reserva" style="width:60%"></center>
                                    <div class="container">
                                        <h2 align="center">Reserva</h2>
                                        <p><button class="button" onclick="location.href='.$reserva.'">Ir a Reserva</button></p>
                                    </div>
                                    <div class="overlay dark">
                                      <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                    </div>
                                  </div>
                              </div>

                              <div class="column col-md-3">
                                  <div class="card card-outline card-dark">
                                    <center><img src="public/img/negocio.png" alt="Negocio" style="width:60%"></center>
                                    <div class="container">
                                        <h2 align="center">Negocio</h2>
                                        <p><button class="button" onclick="location.href='.$negocio.'">Ir a Negocio</button></p>
                                    </div>
                                    <div class="overlay dark">
                                      <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                    </div>
                                  </div>
                              </div>
                              <div class="column col-md-3">
                                  <div class="card card-outline card-dark">
                                    <center><img src="public/img/buzon.png" alt="Buzón" style="width:60%"></center>
                                    <div class="container">
                                        <h2 align="center">Buzón</h2>
                                        <p><button class="button" onclick="location.href='.$buzon.'">Ir a Buzón</button></p>
                                    </div>
                                    <div class="overlay dark">
                                      <i class="fas fa-2x fa-sync-alt fa-spin"></i>
                                    </div>
                                  </div>
                              </div>
                          </div>
                    </section>';
              }
            }
            else{ $noticia = "'noticias'"; $contacto = "'contactos'";
              echo '<center><h2>Nuestras Instalaciones</h2></center>
              <div class="card card-outline card-dark">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                </ol>
                <div class="carousel-inner">
                  <center>
                    <div class="carousel-item active">
                      <img class="d-block w-50" style="height: 500px;" src="view/img/carousell/condominio1.webp" alt="Primer slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-50" style="height: 500px;" src="view/img/carousell/condominio2.jpg" alt="Segundo slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-50" style="height: 500px;" src="view/img/carousell/condominio3.jpg" alt="Tercer slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-50" style="height: 500px;" src="view/img/carousell/condominio4.png" alt="Cuarto slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block w-50" style="height: 500px;" src="view/img/carousell/condominio5.webp" alt="Quinto slide">
                    </div>
                  </center>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              <div class="overlay dark">
                <i class="fas fa-2x fa-sync-alt fa-spin"></i>
              </div>
            </div>';
            }
          ?>
        </div>
        <!-- /.card-body -->
        <div class="overlay dark">
          <i class="fas fa-2x fa-sync-alt fa-spin"></i>
        </div>
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->