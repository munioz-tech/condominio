<?php
  if(!isset($_SESSION["idRol"])){
    echo '<script>
              window.location = "inicio";
            </script>';
            return;
  }
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3>Buzón</h3> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="inicio"><i class="fas fa-tachometer-alt"></i> Inicio</a></li>
              <li class="breadcrumb-item active">Administrar buzón</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form method="post" class="radios">
        <div class="card card-outline card-dark">
          <div class="card-header">
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
            <div class="d-flex align-items-center">
              <div class="form-check-inline">
                <label class="form-check-label" for="radioQueja">Quejas <input type="radio" class="form-check-input" id="radioQueja" name="radioBuzon" value="1"></label>
              </div>
              <div class="form-check-inline">
                <label class="form-check-label" for="radioIdea">Ideas <input type="radio" class="form-check-input" id="radioIdea" name="radioBuzon" value="2"></label>
              </div> 
            </div>
          </div>
          <div class="card-body">
            <div class="form-group"> 
              <div class="input-group"> 
                <textarea id="buzonDetalle" name="buzonDetalle" class="form-control rounded-0" rows="4" placeholder="Describa su queja o idea..." disabled></textarea>
                <input type="hidden" id="idBuzonHidden" name="idBuzonHidden" value="">
              </div>
            </div>
            <?php
              $buzon = new ControladorBuzon();
              $buzon -> ctrProcesarBuzon();
            ?>
            <div class="btn-group col-sm-5">
              <div class="col-sm-2">
                <button id="btnBuzon" type="submit" class="btn btn-primary swal" disabled>Enviar</button>
              </div>
              <div class="col-sm-3">
                <button type="reset" class="btn btn-danger swal btncancelarBuzon" disabled>Cancelar</button>
              </div>
            </div>
          </div>
          <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
        </div>
      </form>
    </section>

  <!-- Sección 2 -->
  <section class="content">
  <div class="card card-outline card-dark">
    <div class="card-header form-group" id="tablaPublica">
      <table class="table table-bordered table-hover nowrap dt-responsive dataTable_width-margin_auto" id="tablas">
        <thead>
          <tr>
            <th>#</th>
            <th>Detalle</th>
            <th>Tipo</th>
            <th>Fecha</th>
            <th>Observación</th>
            <th>Estado</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $tablas = "";
            $alertas = ModeloBuzon::mdlObtenerRegistroBuzon($tablas);
            $contador = 1;
            foreach($alertas as $value){
              if($value["estado"] == 2 || $value["estado"] == 5 || $value["estado"] == 10){
                echo '<tr>
                        <td>'.$contador.'</td>
                        <td>'.$value["descripcion"].'</td>';
                        if($value["queja_idea"] == 1){echo'<td><button class="btn btn-warning btn-sm">Queja</button></td>';}
                        else if($value["queja_idea"] == 2){echo'<td><button class="btn btn-success btn-sm">Idea</button></td>';}
                  echo '<td>'.date("d-m-Y H:i",strtotime($value["f_crea"])).'</td>
                        <td>'.$value["observacion"].'</td>';
                        if($value["estado"] == 2){echo'<td><button class="btn btn-warning btn-sm">EN PROCESO</button></td>';}
                        elseif($value["estado"] == 10){echo'<td><button class="btn btn-success btn-sm">PUBLICADO</button></td>';}
                        elseif($value["estado"] == 5){echo'<td><button class="btn btn-danger btn-sm">RECHAZADO</button></td>';}
                        if($value["estado"] == 10){
                          echo  '<td>
                          <div class="btn-group">
                            <button class="btn btn-dark btnEditarBuzon" disabled><i class="fa fa-cog" aria-hidden="true"></i></button>
                            <button class="btn btn-danger btnEliminarBuzon" disabled><i class="fa fa-times"></i></button> 
                          </div>
                        </td>
                      </tr>';
                        }
                    elseif($value["estado"] == 2 || $value["estado"] == 5){
                      echo  '<td>
                      <div class="btn-group">
                        <button class="btn btn-dark btnEditarBuzon" idBuzon="'.$value["id"].'"><i class="fa fa-cog" aria-hidden="true"></i></button>
                        <button class="btn btn-danger btnEliminarBuzon" idBuzon="'.$value["id"].'"><i class="fa fa-times"></i></button> 
                      </div>
                    </td>
                  </tr>';
                    } 
              } $contador++;
            }
          ?>
        </tbody>
      </table>
    </div>
    <div class="overlay dark">
  <i class="fas fa-2x fa-sync-alt fa-spin"></i>
</div>
</div>
  </section>
  </div>

<?php
  $borrarBuzon = new ControladorBuzon();
  $borrarBuzon ->ctrBorrarBuzon();
?>