$(document).on("change","#actividadPadre",function(){
    if($(this).val() == 0){$("#actividadHijo").html("");}
    else{
        $("#actividadPadre option[value='0']").prop("disabled",true);
        $('#actividadDetalle').prop("disabled",true);
        $('#fotoActividad').prop("disabled",true);
        $('#btnActividad').prop("disabled",true); 
        $('.btncancelarActividad').prop("disabled",true);
        var idPadre = $(this).val();
        var datos = new FormData();
        datos.append("idPadre", idPadre);
        $.ajax({
            url:"ajax/actividad.ajax.php",
            method:"POST",
            data:datos,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success:function(respuesta)
            {   $("#actividadHijo").html(respuesta);    } 
        });
    }
});

$(document).on("change","#actividadHijo",function(){
    if($(this).val() == 0){
        $('#actividadDetalle').prop("disabled",true);
        $('#fotoActividad').prop("disabled",true);
        $('#btnActividad').prop("disabled",true); 
        $('.btncancelarActividad').prop("disabled",true);       
    }
    else{
        $("#actividadHijo option[value='0']").prop("disabled",true);
        var idHijo = $(this).val();
        var datos = new FormData();
        datos.append("idHijo", idHijo);
        $.ajax({
            url:"ajax/actividad.ajax.php",
            method:"POST",
            data:datos,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success:function()
            {   $('#actividadDetalle').prop("disabled",false);
                $('#fotoActividad').prop("disabled",false);
                $('#btnActividad').prop("disabled",false);
                $('.btncancelarActividad').prop("disabled",false);
            }
        });
    }
});

/*====================================
       SUBIENDO FOTO DE ACTIVIDAD
 ====================================*/
 $(".fotoActividad").change(function(){
    var imagen = this.files[0];
    if(imagen["type"]!== "image/jpeg" && imagen["type"]!== "image/png"){
        $(".fotoActividad").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen debe ser de formato JPG o PNG!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else if(imagen["size"] > 2000000){
        $(".fotoActividad").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen no debe pesar mas de 2MB!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else{
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);
        
        $(datosImagen).on("load",function(event){
            var rutaImagen = event.target.result;
            $(".previsualizarAct").attr("src", rutaImagen);
        });
    }
});

/*====================================
        MOSTRAR FOTO MODAL
 ====================================*/
 $(document).on("click","#btnFotoAct",function(){
    $(".modalFotoAct").attr("src", "");
    var rutaFoto = $(this).attr("rutaImg");
    if(rutaFoto != ""){$(".modalFotoAct").attr("src",rutaFoto);}
    else{$(".modalFotoAct").attr("src","view/img/actividades/default/no-img.png");}
 });

/*====================================
            EDITAR ACTIVIDAD
 ====================================*/
$(document).on("click",".btnEditarActividad",function(){
    var idActividad = $(this).attr("idActividad");
    var datos = new FormData();
    datos.append("idActividad", idActividad);
    $.ajax({
       url:"ajax/actividad.ajax.php",
       method: "POST",
       data: datos,
       cache: false,
       contentType: false,
       processData: false,
       dataType: "json",
       success: function(respuesta){
           $("#actividadPadre").val(respuesta["id_act_padre"]);
           $('#actividadPadre').prop("disabled",true);
           llenarActividadHijo(respuesta["id_act_padre"],respuesta["id_actividad"]);
           $("#actividadDetalle").val(respuesta["descripcion"]);
           $("#fotoActividadActual").val(respuesta["ruta_img"]);
           $("#idActividadHidden").val(respuesta["id"]);
           if(respuesta["ruta_img"] !== ""){
               $(".previsualizarAct").attr("src", respuesta["ruta_img"]);
           } else {$(".previsualizarAct").attr("src", "");}
       }
    }); 
});

function llenarActividadHijo(idPadre,idHijo){
    $("#actividadPadre").prop("disabled",true); 
    var datos = new FormData();
    datos.append("idPadre", idPadre);
    $.ajax({
        url:"ajax/actividad.ajax.php",
        method:"POST",
        data:datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success:function(respuesta)
        {   $("#actividadHijo").html(respuesta);
            $("#actividadHijo").val(idHijo);
            $('#actividadHijo').prop("disabled",true);
            $('#actividadDetalle').prop("disabled",false);
            $('#fotoActividad').prop("disabled",false);
            $('#btnActividad').prop("disabled",false);
            $('.btncancelarActividad').prop("disabled",false);
        } 
    });
}

/*====================================
        ELIMINAR ACTIVIDAD
 ====================================*/
$(document).on("click",".btnEliminarActividad",function(){
    var idActividad = $(this).attr("idActividad");
    var fotoActividad = $(this).attr("eliminarImgAct");
    Swal.fire({
        icon: "warning",
        title: "¿Está seguro que desea borrar esta actividad?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: '#3028d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí, borrar actividad!"
    }).then(function(result){
       if(result.value){
           window.location = "index.php?ruta=actividades&idActividad="+idActividad+"&fotoActividad="+fotoActividad;
       } 
    });     
});

/*====================================
            BOTÓN CANCELAR
 ====================================*/
$('.btncancelarActividad').on('click', function()
{   if($("#actividadPadre option[value='0']") != 0){
        $("#actividadPadre option[value='0']").prop("disabled",false);
        $("#actividadPadre").prop("disabled",false);
        $("#actividadHijo").html('<option value="">Seleccione Tipo de Actividad</option>');
        $("#actividadHijo").prop("disabled",false);
        $('#actividadDetalle').prop("disabled",true);
        $('#fotoActividad').val('');
        $('.labelActividad').html("");
        $('#fotoActividad').prop("disabled",true);
        $(".previsualizarAct").attr("src", "");
        $("#fotoActividadActual").val('');
        $("#idActividadHidden").val('');
        $('#btnActividad').prop("disabled",true);
        $('.btncancelarActividad').prop("disabled",true);
    }
});