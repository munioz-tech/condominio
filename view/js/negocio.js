$(document).on("change",".selectNegocio",function(){
    $(".ocultaNegocio").remove();
    $("#selectNegocio option[value='0']").prop("disabled",true);    
    var negocio = $(this).val();
    var datos = new FormData();
    datos.append("negocio", negocio);
    $.ajax({
        url:"ajax/negocio.ajax.php",
        method:"POST",
        data:datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success:function(respuesta)
        {   $(".negocioInputs").parent().after(respuesta);
            $('#negocioDetalle').prop("disabled",false);
            $('#fotoNegocio').prop("disabled",false);
            $('#btnNegocio').prop("disabled",false);
            $('.btncancelarNegocio').prop("disabled",false);
        }
    });
});

/*====================================
       SUBIENDO FOTO DE NEGOCIO
 ====================================*/
 $(".fotoNegocio").change(function(){
    var imagen = this.files[0];
    if(imagen["type"]!== "image/jpeg" && imagen["type"]!== "image/png"){
        $(".fotoNegocio").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen debe ser de formato JPG o PNG!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else if(imagen["size"] > 2000000){
        $(".fotoNegocio").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen no debe pesar mas de 2MB!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else{
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);
        
        $(datosImagen).on("load",function(event){
            var rutaImagen = event.target.result;
            $(".previsualizarNeg").attr("src", rutaImagen);
        });
    }
});

/*====================================
        MOSTRAR FOTO MODAL
 ====================================*/
 $(document).on("click","#btnFotoNeg",function(){
    $(".modalFotoNeg").attr("src", "");
    var rutaFoto = $(this).attr("rutaImg");
    if(rutaFoto != ""){$(".modalFotoNeg").attr("src", rutaFoto);}
    else{$(".modalFotoAct").attr("src","view/img/negocios/default/no-img.png");}
 });

/*====================================
        EDITAR NEGOCIO
 ====================================*/
$(document).on("click",".btnEditarNegocio",function(){
    var idNegocio = $(this).attr("idNegocio");
    var datos = new FormData();
    datos.append("idNegocio", idNegocio);
    $.ajax({
       url:"ajax/negocio.ajax.php",
       method: "POST",
       data: datos,
       cache: false,
       contentType: false,
       processData: false,
       dataType: "json",
       success: function(respuesta){
            $("#selectNegocio").val(respuesta["id_negocio"]);
            $('#selectNegocio').prop("disabled",true);
            llenarTipoPrecio(respuesta["id_negocio"],respuesta["t_negocio"],respuesta["precio"]);
            $("#negocioDetalle").val(respuesta["descripcion"]);
            $("#fotoNegocioActual").val(respuesta["ruta_img"]);
            $("#idNegocioHidden").val(respuesta["id"]);         
            if(respuesta["ruta_img"] !== ""){
                $(".previsualizarNeg").attr("src", respuesta["ruta_img"]);
            } else {$(".previsualizarNeg").attr("src", "");}
       }
    }); 
});

function llenarTipoPrecio(negocio,tipoNegocio,precio){
    $(".ocultaNegocio").remove();
    $("#selectNegocio").prop("disabled",true);  
    var datos = new FormData();
    datos.append("negocio", negocio);
    $.ajax({
        url:"ajax/negocio.ajax.php",
        method:"POST",
        data:datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success:function(respuesta)
        {   $(".negocioInputs").parent().after(respuesta);
            $("#tipoNegocio").val(tipoNegocio);
            $("#precioNegocio").val(precio);
            $('#tipoNegocio').prop("disabled",false);
            $('#precioNegocio').prop("disabled",false);
            $('#negocioDetalle').prop("disabled",false);
            $('#fotoNegocio').prop("disabled",false);
            $('#btnNegocio').prop("disabled",false);
            $('.btncancelarNegocio').prop("disabled",false);
        } 
    });
}

/*====================================
        ELIMINAR NEGOCIO
 ====================================*/
 $(document).on("click",".btnEliminarNegocio",function(){
    var idNegocio = $(this).attr("idNegocio");
    var fotoNegocio = $(this).attr("eliminarImgNeg");
    Swal.fire({
        icon: "warning",
        title: "¿Está seguro que desea borrar este negocio?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: '#3028d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí, borrar Negocio!"
    }).then(function(result){
       if(result.value){
           window.location = "index.php?ruta=negocios&idNegocio="+idNegocio+"&fotoNegocio="+fotoNegocio;
       } 
    });     
});

/*====================================
            BOTÓN CANCELAR
 ====================================*/
$('.btncancelarNegocio').on('click', function()
{   if($("#selectNegocio option[value='0']") != 0){
        $("#selectNegocio option[value='0']").prop("disabled",false);
        $("#selectNegocio").prop("disabled",false);
        $(".ocultaNegocio").remove();
        $('#negocioDetalle').prop("disabled",true);
        $('#fotoNegocio').val('');
        $('.labelNegocio').html("");
        $('#fotoNegocio').prop("disabled",true);
        $(".previsualizarNeg").attr("src", "");
        $("#fotoNegocioActual").val('');
        $("#idNegocioHidden").val('');
        $('#btnNegocio').prop("disabled",true);
        $('.btncancelarNegocio').prop("disabled",true);
    }
});

function filterFloat(evt,input){
    // Backspace = 8, Enter = 13, ‘0′ = 48, ‘9′ = 57, ‘.’ = 46, ‘-’ = 43
    var key = window.Event ? evt.which : evt.keyCode;    
    var chark = String.fromCharCode(key);
    var tempValue = input.value+chark;
    if(key >= 48 && key <= 57){
        if(filter(tempValue)=== false){
            return false;
        }else{       
            return true;
        }
    }else{
          if(key == 8 || key == 13 || key == 0) {     
              return true;              
          }else if(key == 46){
                if(filter(tempValue)=== false){
                    return false;
                }else{       
                    return true;
                }
          }else{
              return false;
          }
    }
}
function filter(__val__){
    var preg = /^([0-9]+\.?[0-9]{0,2})$/; 
    if(preg.test(__val__) === true){
        return true;
    }else{
       return false;
    }
    
}