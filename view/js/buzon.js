$("input[type=radio]").change(function(){ 
    if($("input[type=radio][value='1']").prop( "checked" )){
        $("#buzonDetalle").prop("disabled",false);
        $("#btnBuzon").prop("disabled",false);
        $(".btncancelarBuzon").prop("disabled",false);
    }if($("input[type=radio][value='2'").prop( "checked" )){
        $("#buzonDetalle").prop("disabled",false);
        $("#btnBuzon").prop("disabled",false);
        $(".btncancelarBuzon").prop("disabled",false);
    }
});

/*====================================
            EDITAR BUZON
 ====================================*/
$(document).on("click",".btnEditarBuzon",function(){
    var idBuzon = $(this).attr("idBuzon");
    var datos = new FormData();
    datos.append("idBuzon", idBuzon);
    $.ajax({
        url:"ajax/buzon.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            if(respuesta["queja_idea"] == 1)
            {   $("#radioQueja").prop("checked", true);
                $("#radioQueja").val(respuesta["queja_idea"]);
                $("#buzonDetalle").val(respuesta["descripcion"]);
                $("#idBuzonHidden").val(respuesta["id"]);
            } if(respuesta["queja_idea"] == 2) { 
                $("#radioIdea").prop("checked", true);
                $("#radioIdea").val(respuesta["queja_idea"]);
                $("#idBuzonHidden").val(respuesta["id"]);
                $("#buzonDetalle").val(respuesta["descripcion"]);
            }
            $("input[name='radioBuzon']").prop("disabled",true);
            $("#buzonDetalle").prop("disabled",false);
            $("#btnBuzon").prop("disabled",false);
            $(".btncancelarBuzon").prop("disabled",false);
        }
    });
});

/*====================================
            ELIMINAR BUZON
 ====================================*/
$(document).on("click",".btnEliminarBuzon",function(){
    var idBuzon = $(this).attr("idBuzon");
        Swal.fire({
            icon: "warning",
            title: "¿Está seguro que desea borrar este registro?",
            text: "¡Si no lo está puede cancelar la acción!",
            showCancelButton: true,
            confirmButtonColor: '#3028d6',
            cancelButtonColor: '#d33',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sí, borrar registro!"
        }).then(function(result){
            if(result.value){
                window.location = "index.php?ruta=buzon&idBuzon="+idBuzon;
            } 
        });  
});

/*====================================
            BOTÓN CANCELAR
 ====================================*/
 $('.btncancelarBuzon').on('click', function()
{   if($("input[name='radioBuzon']").val() == 1){
    $("input[name='radioBuzon']").prop("checked",false);
    $("#buzonDetalle").prop("disabled",true);
    $('#btnBuzon').prop("disabled",true);
    $('.btncancelarBuzon').prop("disabled",true);
    $("input[name='radioBuzon']").prop("disabled",false);
    $("#idBuzonHidden").val("");
    }
    if($("input[name='radioBuzon']").val() == 2){
    $("input[name='radioBuzon']").prop("checked",false);
    $("#buzonDetalle").prop("disabled",true);
    $('#btnBuzon').prop("disabled",true);
    $('.btncancelarBuzon').prop("disabled",true);
    $("input[name='radioBuzon']").prop("disabled",false);
    $("#idBuzonHidden").val("");
    }
 });