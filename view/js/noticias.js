$(document).on("click",".mostrarNoticia",function(){
    $(".column").remove();
    $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
    var idMostrarNoticia = $(this).attr("idMostrarNoticia");
    var datos = new FormData();
    datos.append("idMostrarNoticia", idMostrarNoticia);
    $.ajax({
        url:"ajax/noticias.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){
            if(idMostrarNoticia == 0){
                $(".noticias").prepend(respuesta);
                $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            }
            if(idMostrarNoticia == 1){
                $(".noticias").prepend(respuesta);
                $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            }
            if(idMostrarNoticia == 2){
                $(".noticias").prepend(respuesta);
                $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            }
            if(idMostrarNoticia == 3){
                $(".noticias").prepend(respuesta);
                $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            }
            if(idMostrarNoticia == 4){
                $(".noticias").prepend(respuesta);
                $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            }
            if(idMostrarNoticia == 5){
                $(".noticias").prepend(respuesta);
                $(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');
            }
            $(document).ready(function(){
                $(".overlay").remove();
            });
        }
    }); 
});