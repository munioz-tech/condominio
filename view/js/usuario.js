/*====================================
       SUBIENDO FOTO DE USUARIO
 ====================================*/
 $(".nuevaFoto").change(function(){
    var imagen = this.files[0];
    
    if(imagen["type"]!== "image/jpeg" && imagen["type"]!== "image/png"){
        $(".nuevaFoto").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen debe ser de formato JPG o PNG!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else if(imagen["size"] > 2000000){
        $(".nuevaFoto").val("");
        Swal.fire({
            icon: "error",
            title: "Error al subir imagen",
            text: "¡La imagen no debe pesar mas de 2MB!",
            showConfirmButton: true,
            confirmButtonText: "Cerrar"
        });
    }
    else{
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);
        
        $(datosImagen).on("load",function(event){
            var rutaImagen = event.target.result;
            $(".previsualizar").attr("src", rutaImagen);
        });
    }
});

/*====================================
            EDITAR USUARIO
 ====================================*/
$(document).on("click",".btnEditarUsuario",function(){
    var idUsuario = $(this).attr("cedula");
    var datos = new FormData();
    datos.append("idUsuario", idUsuario);
    
    $.ajax({
       url:"ajax/usuarios.ajax.php",
       method: "POST",
       data: datos,
       cache: false,
       contentType: false,
       processData: false,
       dataType: "json",
       success: function(respuesta){
            $("#editarPerfil").val(respuesta["ID_ROL"]);
            $("#editarCedula").val(respuesta["CEDULA"]);
            $("#cedulaActual").val(respuesta["CEDULA"]);
            $("#editarNombre").val(respuesta["NOMBRE"]);
            $("#editarApellidos").val(respuesta["APELLIDOS"]);
            $("#passwordActual").val(respuesta["CLAVE"]);
            $("#editarEdificio").val(respuesta["N_EDIFICIO"]);
            $("#editarHabitacion").val(respuesta["N_HABITACION"]);
            $("#editarEmail").val(respuesta["CORREO"]);
            $("#editarTelefono").val(respuesta["TELEFONO"]);
            $("#fotoActual").val(respuesta["FOTO"]);
            if(respuesta["FOTO"] !== ""){
                $(".previsualizar").attr("src", respuesta["FOTO"]);
            } else {$(".previsualizar").attr("src", "view/img/usuarios/default/default_user.png");}
           
       }
    }); 
});

/*====================================
    FOTO DEFAULT AGREGAR USUARIOS
 ====================================*/
$(".btnAgregarUsuario").click(function(){
    $(".alert").remove();
    $(".previsualizar").attr("src", "view/img/usuarios/default/default_user.png"); 
});

/*====================================
            ACTIVAR USUARIOS
 ====================================*/
$(document).on("click",".btnActivar",function(){
    var idRol = $(this).attr("idRol");
    var idUsuario = $(this).attr("idUsuario");
    var estadoUsuario = $(this).attr("estadoUsuario");
    var estado = $(this).attr("estado");
    var u_modifica = $(this).attr("idUsuarioModifica");

    var datos = new FormData();
    datos.append("idRol", idRol);
    datos.append("activarId", idUsuario);
    datos.append("activarUsuario", estadoUsuario);
    datos.append("u_modifica", u_modifica);
    $.ajax({
       url:"ajax/usuarios.ajax.php",
       method:"POST",
       data:datos,
       cache:false,
       contentType:false,
       processData:false,
       success:function(respuesta){
           if(window.matchMedia("(max-width:767px)").matches){
               Swal.fire({
                    icon: "success",
                    title: "Excelente",
                    text: "¡El usuario se actualizó correctamente!",
                    showConfirmButton: true,
                    confirmButtonText: "Cerrar"
                }).then(function(result){
                    if (result.value) {
                        window.location = "usuarios";
                    }
                });
           }
           
       }
    });
    
    //Estado Activado
    if(estado == 1){
           $(this).removeClass('btn-success');
           $(this).addClass('btn-danger');
           $(this).html('Desactivado');
           $(this).attr('estadoUsuario',1);
           $(this).attr('estado',3);
    }//Estado Bloqueado
    else if(estado == 9){
           $(this).removeClass('btn-danger');
           $(this).removeClass('btn-warning');
           $(this).addClass('btn-success');
           $(this).html('Activado');
           $(this).attr('estadoUsuario',3);
           $(this).attr('estado',1);
    }//Estado Desactivado
    else if(estado == 3){
           $(this).removeClass('btn-danger');
           $(this).removeClass('btn-warning');
           $(this).addClass('btn-success');
           $(this).html('Activado');
           $(this).attr('estadoUsuario',3);
           $(this).attr('estado',1);
    }
});

/*====================================
    VALIDAR USUARIOS REPETIDOS
 ====================================*/
$('#nuevoUsuario').change(function(){
   $(".alert").remove();
   var usuario = $(this).val();
   var datos = new FormData();
   datos.append("validarUsuario", usuario);
   $.ajax({
       url:"ajax/usuarios.ajax.php",
       method:"POST",
       data:datos,
       cache:false,
       contentType:false,
       processData:false,
       dataType: "json",
       success:function(respuesta){
           if(respuesta){
               $('#nuevoUsuario').parent().after('<div class="alert alert-warning">Este usuario no disponible.</div>');
               $('#nuevoUsuario').val("");
           }
           else{
               $('#nuevoUsuario').parent().after('<div class="alert alert-success">Usuario disponible.</div>');
           }
       }
   });   
});

/*====================================
        ELIMINAR USUARIOS
 ====================================*/
$(document).on("click",".btnEliminarUsuario",function(){
    var idUsuario = $(this).attr("cedula");
    //var fotoUsuario = $(this).attr("fotoUsuario");
    //var usuario = $(this).attr("cedula");
    
    Swal.fire({
        icon: "warning",
        title: "¿Está seguro que desea borrar este usuario?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: '#3028d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí, borrar usuario!"
    }).then(function(result){
       if(result.value){
           window.location = "index.php?ruta=usuarios&idUsuario="+idUsuario;
       } 
    });     
});

/*====================================
        BOTON MOSTRAR PASSWORD
 ====================================*/
 $("#Password .btn-danger").on('click', function() {
    if($('#nuevoPassword').attr("type") === "password"){
        $('#nuevoPassword').attr("type", "text");
        $('#eyes').removeClass("fa fa-eye");
        $('#eyes').addClass("fa fa-eye-slash");            
    }else{  $('#nuevoPassword').attr("type", "password");
            $('#eyes').removeClass("fa fa-eye-slash");
            $('#eyes').addClass("fa fa-eye");
        }
});

$("#PasswordEditar .btn-danger").on('click', function() {
    if($('#editarPassword').attr("type") === "password"){
        $('#editarPassword').attr("type", "text");
        $('#eyesEditar').removeClass("fa fa-eye");
        $('#eyesEditar').addClass("fa fa-eye-slash");            
    }else{  $('#editarPassword').attr("type", "password");
            $('#eyesEditar').removeClass("fa fa-eye-slash");
            $('#eyesEditar').addClass("fa fa-eye");
        }
});

/*====================================
            VALIDACIONES
 ====================================*/
$('#nuevaCedula,#editarCedula').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 10 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
      return false;
    }   
});

$('#nuevoNombre,#editarNombre').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 15 && isNaN(e.key))
        {   letras = $(this).val().replace(/ /g, "");
            $(this).val(letras);
            $(this).val($(this).val() + e.key); 
        }
        return false;
    }
});

$('#nuevoApellido,#editarApellidos').bind('keypress',function(event){
    var regex = new RegExp("^[a-zA-Z ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)){
        event.preventDefault();
        return false;
    }
});

$('#nuevoPassword').on('keypress keydown keyup',function(e){
    var password = $("#nuevoPassword").val();
    if(e.keyCode != 16){
        if(e.keyCode != 0){
            if(password != undefined){
                passwordSegura = validarPass(password);
            }
        }
    } 
    if(passwordSegura == "ok"){
        $("#Password .btn").attr("class", "btn btn-success col-xs-2");
    }
    else{
        $("#Password .btn").attr("class", "btn btn-danger col-xs-2");
    }
});

$('#editarPassword').on('keypress keydown keyup',function(e){
    var password = $("#editarPassword").val();
    if(e.keyCode != 16){
        if(e.keyCode != 0){
            if(password != undefined){
                passwordSegura = validarPass(password);
            }
        }
    } 
    if(passwordSegura == "ok"){
        $("#PasswordEditar .btn").attr("class", "btn btn-success col-xs-2");
    }
    else{
        $("#PasswordEditar .btn").attr("class", "btn btn-danger col-xs-2");
    }
});

function validarPass(password){
    arrayPass = password.split(''); 
    if(arrayPass.length >= 6){
        var mayus = 0;
        var minus = 0;
        var num = 0;
        arrayPass.forEach(Caracter =>{
            var regexMin = /^[a-z]+$/.test(Caracter);
            if (regexMin) {minus++;}

            var regexMayus = /^[A-Z]+$/.test(Caracter);
            if (regexMayus) {mayus++;}

            var regexNum = /^[0-9]+$/.test(Caracter);
            if (regexNum) {num++;}
        });
        if(mayus > 0 && minus > 0 && num > 0){return "ok";}
        else{return "error";}
    }
};

$('#nuevoEdificio,#editarEdificio').bind('keydown keyup',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 3)
        { $(this).val($(this).val() + e.key);   }
      return false;
    }
});

$('#nuevaHabitacion,#editarHabitacion').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 2 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
        return false;
    }
});

$('#nuevoTelefono,#editarTelefono').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 10 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
        return false;
    }
});

$('#nuevoEmail').bind('keydown keyup',function(event)
{   var correo = $("#nuevoEmail").val();
    var regex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(correo);
    if(regex)
    {   $("#nuevoDivCorreo").attr("class", "btn btn-success col-xs-2");
        $("#nuevoSpanCorreo").attr("class", "fa fa-check-square");
    }else{  $("#nuevoDivCorreo").attr("class", "btn btn-danger col-xs-2");
            $("#nuevoSpanCorreo").attr("class", "fa fa-window-close");
        }
});

$('#editarEmail').on("keydown keyup",function(event){
    var correo = $("#editarEmail").val();
    var regex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(correo);
    if(correo == ""){
        $("#editarDivCorreo").attr("class", "btn btn-danger col-xs-2");
        $("#editarSpanCorreo").attr("class", "fa fa-window-close");
    }
    else if(regex)
    {   $("#editarDivCorreo").attr("class", "btn btn-success col-xs-2");
        $("#editarSpanCorreo").attr("class", "fa fa-check-square");
    }else{  $("#editarDivCorreo").attr("class", "btn btn-danger col-xs-2");
            $("#editarSpanCorreo").attr("class", "fa fa-window-close");
        }
});