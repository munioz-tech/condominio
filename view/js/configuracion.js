$(window).on("load",function(){
    var idConfiguracion = $("#configuracion").attr("idConfiguracion");
    var datos = new FormData();
    datos.append("idConfiguracion", idConfiguracion);
    $.ajax({
        url:"ajax/configuracion.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            $("#confNombre").val(respuesta["nombre"]);
            $("#confApellido").val(respuesta["apellidos"]);
            $("#confEdificio").val(respuesta["n_edificio"]);
            $("#confHabitacion").val(respuesta["n_habitacion"]);
            $("#confTelefono").val(respuesta["telefono"]);
            $("#confEmail").val(respuesta["correo"]);
            $("#conffotoActual").val(respuesta["foto"]);
            $(".previsualizarconf").attr("src", respuesta["foto"]);
            var correo = $("#confEmail").val();
            var regex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(correo);
            if(regex)
            {   $("#confDivCorreo").attr("class", "btn btn-success col-xs-2");
                $("#confSpanCorreo").attr("class", "fa fa-check-square");
            }else{  $("#confDivCorreo").attr("class", "btn btn-danger col-xs-2");
                    $("#confSpanCorreo").attr("class", "fa fa-window-close");
                }
        }
    }); 
});

/*====================================
        BOTON MOSTRAR PASSWORD
 ====================================*/
 $("#confPass .btn-danger").on('click', function() {
    if($('#confPassword').attr("type") === "password"){
        $('#confPassword').attr("type", "text");
        $('#confEyes').removeClass("fa fa-eye");
        $('#confEyes').addClass("fa fa-eye-slash");            
    }else{  $('#confPassword').attr("type", "password");
            $('#confEyes').removeClass("fa fa-eye-slash");
            $('#confEyes').addClass("fa fa-eye");
        }
});
/*====================================
            VALIDACIONES
 ====================================*/
$('#confNombre').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 15 && isNaN(e.key))
        {   letras = $(this).val().replace(/ /g, "");
            $(this).val(letras);
            $(this).val($(this).val() + e.key); 
        }
        return false;
    }
});

$('#confApellido').bind('keypress',function(event){
    var regex = new RegExp("^[a-zA-Z ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)){
        event.preventDefault();
        return false;
    }
});

$('#confEdificio').bind('keydown keyup',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 3)
        { $(this).val($(this).val() + e.key);   }
      return false;
    }
});

$('#confHabitacion').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 2 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
        return false;
    }
});

$('#confTelefono').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 10 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
        return false;
    }
});

$('#confEmail').bind('keydown keyup',function(event)
{   var correo = $("#confEmail").val();
    var regex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(correo);
    if(regex)
    {   $("#confDivCorreo").attr("class", "btn btn-success col-xs-2");
        $("#confSpanCorreo").attr("class", "fa fa-check-square");
    }else{  $("#confDivCorreo").attr("class", "btn btn-danger col-xs-2");
            $("#confSpanCorreo").attr("class", "fa fa-window-close");
        }
});

$('#confPassword').on('keypress keydown keyup',function(e){
    var password = $("#confPassword").val();
    if(e.keyCode != 16){
        if(e.keyCode != 0){
            if(password != undefined){
                passwordSegura = validarPass(password);
            }
        }
    } 
    if(passwordSegura == "ok"){
        $("#confPass .btn").attr("class", "btn btn-success col-xs-2");
    }
    else{
        $("#confPass .btn").attr("class", "btn btn-danger col-xs-2");
    }
});

function validarPass(password){
    arrayPass = password.split(''); 
    if(arrayPass.length >= 6){
        var mayus = 0;
        var minus = 0;
        var num = 0;
        arrayPass.forEach(Caracter =>{
            var regexMin = /^[a-z]+$/.test(Caracter);
            if (regexMin) {minus++;}

            var regexMayus = /^[A-Z]+$/.test(Caracter);
            if (regexMayus) {mayus++;}

            var regexNum = /^[0-9]+$/.test(Caracter);
            if (regexNum) {num++;}
        });
        if(mayus > 0 && minus > 0 && num > 0){return "ok";}
        else{return "error";}
    }
};