$(function() {
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yyyy = hoy.getFullYear();
    hoy = yyyy+'-'+mm+'-'+dd;
    $('input[name="date_desde"]').daterangepicker({
        timePicker: true,
        timePicker24Hour : true,
        timePickerIncrement: 5,
        locale: {
            "applyLabel": "Guardar",
            "cancelLabel": "Cancelar",
            format: 'YYYY-MM-DD H:mm',
            "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Setiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
        },
        "startDate": hoy,
        "endDate": hoy,
        "opens": "center"
    });
});

$(document).on("change","#selectReservas",function(){
    $("#selectReservas option[value='0']").prop("disabled",true);
    var selectReservas = $(this).val();
    $('#reservaDetalle').prop("disabled",false);
    $('#date_desde').prop("disabled",false);
    $('#btnReserva').prop("disabled",false);
    $('#btnReserva').attr("reservar",selectReservas);
    $('.btncancelarReserva').prop("disabled",false);
});

/*====================================
            EDITAR RESERVA
 ====================================*/
 $(document).on("click",".btnEditarReserva",function(){
    var reserva = $(this).attr("reserva");
    var datos = new FormData();
    datos.append("reserva", reserva);
    $.ajax({
        url:"ajax/reservas.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            var fecha = respuesta["fyh_desde"]+" - "+respuesta["fyh_hasta"];
            $("#selectReservas").val(respuesta["id_cancha"]);
            $('#selectReservas').prop("disabled",true);
            $("#date_desde").val(fecha);
            $('#date_desde').prop("disabled",true);
            $("#reservaDetalle").val(respuesta["descripcion"]);
            $("#idReservaHidden").val(respuesta["id"]);
            $("#reservaDetalle").prop("disabled",false);
            $("#btnReserva").prop("disabled",false);
            $('#btnReserva').attr("reservar",reserva);
            $(".btncancelarReserva").prop("disabled",false);
        }
    });
});

/*====================================
        ELIMINAR RESERVA
 ====================================*/
 $(document).on("click",".btnEliminarReserva",function(){
    var idReserva = $(this).attr("idReserva");
    Swal.fire({
        icon: "warning",
        title: "¿Está seguro que desea borrar esta reserva?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: '#3028d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí, borrar Reserva!"
    }).then(function(result){
       if(result.value){
           window.location = "index.php?ruta=reservas&idReserva="+idReserva;
       } 
    });     
});

/*====================================
            BOTÓN CANCELAR
 ====================================*/
$('.btncancelarReserva').on('click', function()
{   if($("#selectReservas option[value='0']")){
        $("#selectReservas option[value='0']").prop("disabled",false);
        $("#selectReservas").prop("disabled",false);
        $(function() {
            var hoy = new Date();
            var dd = hoy.getDate();
            var mm = hoy.getMonth()+1;
            var yyyy = hoy.getFullYear();
            hoy = yyyy+'-'+mm+'-'+dd;
            $('input[name="date_desde"]').daterangepicker({
                timePicker: true,
                timePicker24Hour : true,
                timePickerIncrement: 5,
                locale: {
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    format: 'YYYY-MM-DD H:mm',
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                },
                "startDate": hoy,
                "endDate": hoy,
                "opens": "center"
            });
        });
        $('#date_desde').prop("disabled",true);
        $('#reservaDetalle').prop("disabled",true);
        $("#idReservaHidden").val('');
        $('#btnReserva').prop("disabled",true);
        $('#btnReserva').attr("reservar",0);
        $('.btncancelarReserva').prop("disabled",true);
    }
});


/*====================================
                CALENDARIO
 ====================================
 $("#btnReserva").on('submit',function(){
    var fecha = $("#date_desde").val();
    var desde = str.substring(0, 15);
    var hasta = str.substring(18);
    if($("#selectReservas option[value='1']")){
        var nuevoEvento = {
            title  : 'Tenis',
            start  : desde,
            end    : hasta,
            color : '#d81b60',
            textColor : 'white'
        };
    }
    if($("#selectReservas option[value='2']")){
        var nuevoEvento = {
            title  : 'Indor',
            start  : desde,
            end    : hasta,
            color : '#17a2b8',
            textColor : 'white'
        };
    }
    if($("#selectReservas option[value='3']")){
        var nuevoEvento = {
            title  : 'Voley',
            start  : desde,
            end    : hasta,
            color : '#6f42c1',
            textColor : 'white'
        };
    }
    if($("#selectReservas option[value='4']")){
        var nuevoEvento = {
            title  : 'Básquet',
            start  : desde,
            end    : hasta,
            color : '#3d9970',
            textColor : 'white'
        };
    }
    var calendar = new FullCalendar.Calendar();
});*/

