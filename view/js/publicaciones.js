function DatosTabla(Atributo_Tabla) {
    $(Atributo_Tabla).DataTable({
      "responsive": true,
      "autoWidth": false,
      "iDisplayLength": 5,
      "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
	    }
    });
};
/*====================================
    MOSTRAR LISTADO DE PUBLICACIONES
====================================*/
$(document).on("change",".selectPublicacion",function(){
    $(".card-body").remove();
    if($(this).val() != 0){$(".card").prepend('<div class="overlay dark"><i class="fas fa-2x fa-sync-alt fa-spin"></i></div>');}
    else{$(".overlay").remove();}
    var valPublicacion = $(this).val();
    var datos = new FormData();
    datos.append("valPublicacion", valPublicacion);
    $.ajax({
        url:"ajax/utils.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            $('#formPublicaciones').parent().after(respuesta);
            DatosTabla("#tablaMostrarPublicacion");
            if (valPublicacion == 1){
                $(".btnPublicacion").attr("valPublica", valPublicacion);
                $(".btnProcesar").attr("valPublica", valPublicacion);
                $(document).ready(function(){
                    $(".overlay").remove();
                  });
            }
            if (valPublicacion == 2){
                $(".btnPublicacion").attr("valPublica", valPublicacion);
                $(".btnProcesar").attr("valPublica", valPublicacion);
                $(document).ready(function(){
                    $(".overlay").remove();
                  });
            }
            if (valPublicacion == 3){
                $(".btnPublicacion").attr("valPublica", valPublicacion);
                $(".btnProcesar").attr("valPublica", valPublicacion);
                $(document).ready(function(){
                    $(".overlay").remove();
                  });
            }
            if (valPublicacion == 4){
                $(".btnPublicacion").attr("valPublica", valPublicacion);
                $(".btnProcesar").attr("valPublica", valPublicacion);
                $(document).ready(function(){
                    $(".overlay").remove();
                  });
            }
        }
    });
});
/*====================================
        MOSTRAR PUBLICACIÓN
====================================*/
$(document).on("click",".btnPublicacion",function(){
    var idPublicacion = $(this).attr("idPublicacion");
    var valPublica = $(this).attr("valPublica");
    var datos = new FormData();
    datos.append("idPublicacion", idPublicacion);
    datos.append("valPublica", valPublica);
    $.ajax({
        url:"ajax/publicacion.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            if(valPublica == 1){
                $(".nombresAct").val(respuesta["nombre"]+" "+respuesta["apellidos"]);
                $(".telefonoAct").val(respuesta["telefono"]);
                $(".correoAct").val(respuesta["correo"]);
                if(respuesta["id_act_padre"] == 1){$(".actividadPadre").val("Mantenimiento");}
                if(respuesta["id_act_padre"] == 2){$(".actividadPadre").val("Extravío");}
                if(respuesta["id_act_padre"] == 3){$(".actividadPadre").val("Varios");}
                $(".actividadHijo").val(respuesta["tipo_actividad"]);
                $(".actividadDetalle").val(respuesta["descripcion"]);
                $(".previsualizarAct").attr("src", respuesta["ruta_img"]);
                $(".btnProcesar").attr("idProcesar", respuesta["id"]);
                $(".btnProcesar").attr("valPublica", valPublica);
            }
            if(valPublica == 2){
                $(".nombresNeg").val(respuesta["nombre"]+" "+respuesta["apellidos"]);
                $(".telefonoNeg").val(respuesta["telefono"]);
                $(".correoNeg").val(respuesta["correo"]);
                if(respuesta["id_negocio"] == 1){$(".tipoNegocio").val(respuesta["tipo_negocio"]);}
                if(respuesta["id_negocio"] == 2){$(".tipoNegocio").val(respuesta["tipo_negocio"]);}
                if(respuesta["id_negocio"] == 3){$(".tipoNegocio").val(respuesta["tipo_negocio"]);}
                $(".t_negocio").val(respuesta["t_negocio"]);
                $(".precio").val(respuesta["precio"]);
                $(".negocioDetalle").val(respuesta["descripcion"]);
                $(".previsualizarNeg").attr("src", respuesta["ruta_img"]);
                $(".btnProcesar").attr("idProcesar", respuesta["id"]);
                $(".btnProcesar").attr("valPublica", valPublica);
            }
            if(valPublica == 3){
                $(".nombresBuz").val(respuesta["nombre"]+" "+respuesta["apellidos"]);
                $(".telefonoBuz").val(respuesta["telefono"]);
                $(".correoBuz").val(respuesta["correo"]);
                if(respuesta["queja_idea"] == 1){$(".buzon").val("Queja");}
                if(respuesta["queja_idea"] == 2){$(".buzon").val("Idea");}
                $(".buzonDetalle").val(respuesta["descripcion"]);
                $(".btnProcesar").attr("idProcesar", respuesta["id"]);
                $(".btnProcesar").attr("valPublica", valPublica);
            }
            if(valPublica == 4){
                $(".nombresRes").val(respuesta["nombre"]+" "+respuesta["apellidos"]);
                $(".telefonoRes").val(respuesta["telefono"]);
                $(".correoRes").val(respuesta["correo"]);
                $(".reserva").val(respuesta["cancha"]);
                $(".fecha_reserva").val(respuesta["fyh_desde"]+" - "+respuesta["fyh_hasta"]);
                $(".reservaDetalle").val(respuesta["descripcion"]);
                $(".btnProcesar").attr("idProcesar", respuesta["id"]);
                $(".btnProcesar").attr("valPublica", valPublica);
            }
        }
    }); 
});
/*====================================
        PROCESAR PUBLICACIÓN
====================================*/
$(document).on("click",".btnProcesar",function(){
    var idProcesar = $(this).attr("idProcesar");
    var valPublicar = $(this).attr("valPublica");
    var datos = new FormData();
    datos.append("idProcesar", idProcesar);
    datos.append("valPublicar", valPublicar);
    $.ajax({
        url:"ajax/publicacion.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            if(valPublicar == 1){
                $(".btnRechazar").attr("idPublicacion", respuesta["id"]);
                $(".btnRechazar").attr("valPublicar", valPublicar);
                $(".btnAceptar").attr("idPublicacion", respuesta["id"]);
                $(".btnAceptar").attr("valPublicar", valPublicar);
            }
            if(valPublicar == 2){
                $(".btnRechazar").attr("idPublicacion", respuesta["id"]);
                $(".btnRechazar").attr("valPublicar", valPublicar);
                $(".btnAceptar").attr("idPublicacion", respuesta["id"]);
                $(".btnAceptar").attr("valPublicar", valPublicar);
            }
            if(valPublicar == 3){
                $(".btnRechazar").attr("idPublicacion", respuesta["id"]);
                $(".btnRechazar").attr("valPublicar", valPublicar);
                $(".btnAceptar").attr("idPublicacion", respuesta["id"]);
                $(".btnAceptar").attr("valPublicar", valPublicar);
            }
            if(valPublicar == 4){
                $(".btnRechazar").attr("idPublicacion", respuesta["id"]);
                $(".btnRechazar").attr("valPublicar", valPublicar);
                $(".btnAceptar").attr("idPublicacion", respuesta["id"]);
                $(".btnAceptar").attr("valPublicar", valPublicar);
            }
        }
    }); 
});

$(document).on("click",".btnRechazar",function(){
    var idPublicacion = $(this).attr("idPublicacion");
    var valPublicar = $(this).attr("valPublicar");
    var procesar = $(this).attr("procesar");
    var observacion = $(".observacion").val();
    Swal.fire({
        icon: "warning",
        title: "¿Está seguro que desea rechazar esta publicación?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: '#3028d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí, rechazar publicación!"
    }).then(function(result){
       if(result.value){
           window.location = "index.php?ruta=publicaciones&idPublicacion="+idPublicacion+"&procesar="+procesar+"&valPublicar="+valPublicar+"&observacion="+observacion;
       } 
    });    
});

$(document).on("click",".btnAceptar",function(){
    var idPublicacion = $(this).attr("idPublicacion");
    var valPublicar = $(this).attr("valPublicar");
    var procesar = $(this).attr("procesar");
    var observacion = $(".observacion").val();
    Swal.fire({
        icon: "warning",
        title: "¿Está seguro que desea aceptar esta publicación?",
        text: "¡Si no lo está puede cancelar la acción!",
        showCancelButton: true,
        confirmButtonColor: '#3028d6',
        cancelButtonColor: '#d33',
        cancelButtonText: "Cancelar",
        confirmButtonText: "Sí, aceptar publicación!"
    }).then(function(result){
       if(result.value){
           window.location = "index.php?ruta=publicaciones&idPublicacion="+idPublicacion+"&procesar="+procesar+"&valPublicar="+valPublicar+"&observacion="+observacion;
       } 
    });    
});