$(document).on("click",".btnEliminarReunion",function(){
    var idReunion = $(this).attr("idReunion");
        Swal.fire({
            icon: "warning",
            title: "¿Está seguro que desea borrar este registro?",
            text: "¡Si no lo está puede cancelar la acción!",
            showCancelButton: true,
            confirmButtonColor: '#3028d6',
            cancelButtonColor: '#d33',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Sí, borrar registro!"
        }).then(function(result){
            if(result.value){
                window.location = "index.php?ruta=reuniones&idReunion="+idReunion;
            } 
        });  
});

/*====================================
                PASSWORD
 ====================================*/
 $("#reuPass .btn-danger").on('click', function() {
    if($('#nuevoPassword').attr("type") === "password"){
        $('#nuevoPassword').attr("type", "text");
        $('#confEyes').removeClass("fa fa-eye");
        $('#confEyes').addClass("fa fa-eye-slash");            
    }else{  $('#nuevoPassword').attr("type", "password");
            $('#confEyes').removeClass("fa fa-eye-slash");
            $('#confEyes').addClass("fa fa-eye");
        }
});

$('#nuevoPassword').on('keypress keydown keyup',function(e){
    var password = $("#nuevoPassword").val();
    $("#reunionesEnviar").attr("disabled", true);
    if(e.keyCode != 16){
        if(e.keyCode != 0){
            if(password != undefined){
                passwordSegura = validarPass(password);
            }
        }
    } 
    if(passwordSegura == "ok"){
        $("#reuPass .btn").attr("class", "btn btn-success col-xs-2");
        $("#reunionesEnviar").attr("disabled", false);
    }
    else{
        $("#reuPass .btn").attr("class", "btn btn-danger col-xs-2");
        $("#reunionesEnviar").attr("disabled", true);
    }
});

$('#nuevoPassword').focus(function(){
    $(".input .col-sm-12").after("<div class='popover-bottom'><p><em>La contraseña debe contener al menos una letra minúscula, una mayúscula, un número y debe ser mayor a 6 caracteres.</em></p></div>");
});

$('#nuevoPassword').blur(function(){
    $(".popover-bottom").remove();
});

function validarPass(password){
    arrayPass = password.split(''); 
    if(arrayPass.length >= 6){
        var mayus = 0;
        var minus = 0;
        var num = 0;
        arrayPass.forEach(Caracter =>{
            var regexMin = /^[a-z]+$/.test(Caracter);
            if (regexMin) {minus++;}

            var regexMayus = /^[A-Z]+$/.test(Caracter);
            if (regexMayus) {mayus++;}

            var regexNum = /^[0-9]+$/.test(Caracter);
            if (regexNum) {num++;}
        });
        if(mayus > 0 && minus > 0 && num > 0){return "ok";}
        else{return "error";}
    }
};