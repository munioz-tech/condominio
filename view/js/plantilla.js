$(document).ready(function() {
    $('#tablas').DataTable({
      autoWidth: false,
      language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        responsive: true,
    });
});

$('#ingUsuario').on('keydown keypress',function(e){
    if(e.key.length === 1){
        if($(this).val().length < 10 && !isNaN(parseFloat(e.key))){ 
            $(this).val($(this).val() + e.key); 
        }
        return false;
    }
});


$("#divPassword .input-group-text").on('click', function() {
    if($('#divPassword input').attr("type") === "password"){
        $('#divPassword input').attr("type", "text");
        $('#divPassword span').removeClass("fa fa-eye");
        $('#divPassword span').addClass("fa fa-eye-slash");            
    }else{  $('#divPassword input').attr("type", "password");
            $('#divPassword span').removeClass("fa fa-eye-slash");
            $('#divPassword span').addClass("fa fa-eye");
        }
});

//popup alerta
$('#alertUsuario').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 10 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
      return false;
    }
});

$('#codigoSeguridad').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 3)
        { $(this).val($(this).val() + e.key);   }
      return false;
    }
});

//popup invitado
$('#invCedula').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 10 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
        return false;
    }
});

$('#invNombre').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 15 && isNaN(e.key))
        {   letras = $(this).val().replace(/ /g, "");
            $(this).val(letras);
            $(this).val($(this).val() + e.key); 
        }
        return false;
    }
});

$('#invApellidos').bind('keypress',function(event)
{   var regex = new RegExp("^[a-zA-Z ]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key))
    {   event.preventDefault();
        return false;
    }
});

$('#invTelefono').on('keydown keypress',function(e)
{   if(e.key.length === 1)
    {   if($(this).val().length < 10 && !isNaN(parseFloat(e.key)))
        {   $(this).val($(this).val() + e.key); }
        return false;
    }
});

$('#invCorreo').bind('keydown keyup',function(event)
{   var correo = $("#invCorreo").val();
    var regex = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/.test(correo);
    if(regex)
    {   $("#divcorreo").attr("class", "btn btn-success col-xs-2");
        $("#spancorreo").attr("class", "fa fa-check-square");
    }else{  $("#divcorreo").attr("class", "btn btn-danger col-xs-2");
            $("#spancorreo").attr("class", "fa fa-window-close");
        }
});

function mueveReloj(){
    momentoActual = new Date();
    hora = momentoActual.getHours();
    minuto = momentoActual.getMinutes();
    segundo = momentoActual.getSeconds();
  
    str_segundo = new String (segundo);
    if (str_segundo.length == 1)
       segundo = "0" + segundo;
  
    str_minuto = new String (minuto);
    if (str_minuto.length == 1)
       minuto = "0" + minuto;
  
    str_hora = new String (hora);
    if (str_hora.length == 1)
       hora = "0" + hora;
  
    horaImprimible = hora + " : " + minuto + " : " + segundo;
    $(".reloj").attr("value", horaImprimible);
    setTimeout("mueveReloj()",1000);
};

$('.carousel').carousel({
    interval: 1500
  });
