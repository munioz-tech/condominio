<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta author="Antony Canga Ortega - Desarrollador Web Junior de MuniozTech">
  <meta description="Sistema web de control de proceso para el Condominio 'Los Jardines'" >
  <meta keywords="Sistema Web de control, Condominio los Jardines" >
  <link  rel="icon"   href="view/img/plantilla/fav.png" type="image/png" />
  <title>Condominio Los Jardines</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php require_once('utils/libs/css.php'); ?>
  <style>
        table.dataTable.dataTable_width-margin_auto {
            width: auto;
            margin: auto;
        }
    </style>
</head>
 <!--=================================
  CUERPO DOCUMENTO
  ==================================-->
<body class="hold-transition sidebar-collapse sidebar-mini login-page" onload="mueveReloj()">
    <?php
      if(isset($_SESSION["SesionActiva"]) && $_SESSION["SesionActiva"] == "ok")
      {
        echo '<div class="wrapper">';
              include "modules/cabezote.php";
              include "modules/menu.php";

              if(isset($_GET["ruta"])){
                if($_GET["ruta"] == "inicio"            ||
                  $_GET["ruta"] ==  "noticias"          ||
                  $_GET["ruta"] ==  "contactos"         ||
                  $_GET["ruta"] ==  "actividades"       ||
                  $_GET["ruta"] ==  "reservas"          ||
                  $_GET["ruta"] ==  "calendario"        ||
                  $_GET["ruta"] ==  "negocios"          ||
                  $_GET["ruta"] ==  "buzon"             ||
                  $_GET["ruta"] ==  "configuracion"     ||
                  $_GET["ruta"] ==  "usuarios"          ||
                  $_GET["ruta"] ==  "publicaciones"     ||
                  $_GET["ruta"] ==  "reportes"          ||
                  $_GET["ruta"] ==  "reuniones"         ||
                  $_GET["ruta"] ==  "salir"             ||
                  $_GET["ruta"] ==  "alerta"){
                  include "modules/".$_GET["ruta"].".php";
                }
                else  { include "modules/404.php";  }
              }
              else  { include "modules/inicio.php"; }

              include "modules/footer.php";
        echo '</div>';
      }
      else  { include "modules/login.php";  }
    ?>

  <?php require_once('utils/libs/js.php');  ?>
  <!--Function JS Carga de archivos-->
  <script>
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    if ( window.history.replaceState ){
      window.history.replaceState( null, null, window.location.href );
    }
  </script>
</body>
</html>
