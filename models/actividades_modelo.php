<?php

require_once "conexion.php";

class ModeloActividades{
    static public function mdlObtenerActividades($tabla,$item,$idPadre){
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where ESTADO = 1 AND  $item = :idPadre");
        $stmt -> bindParam(":idPadre", $idPadre, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlObtenerRegistroActividades($tablas){
        if($tablas == null){
            $stmt = Conexion::conectar()->prepare('SELECT * FROM ADM_REGISTRO_ACT WHERE u_crea = :u_crea ORDER BY f_crea DESC');
            $stmt -> bindParam(':u_crea', $_SESSION["idUsuario"], PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetchAll();
        }
        else{
            $stmt = Conexion::conectar()->prepare("SELECT AA.TIPO_ACTIVIDAD, AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, ARA.ID_ACT_PADRE, ARA.DESCRIPCION, ARA.F_CREA, ARA.OBSERVACION
                                                    FROM $tablas WHERE AA.ID = ARA.ID_ACTIVIDAD
                                                    AND AU.ID = ARA.U_CREA AND ARA.ESTADO = 10 ORDER BY F_CREA DESC");
                                                    print_r($_SESSION["idUsuario"]);
            $stmt -> bindParam(':u_crea', $_SESSION["idUsuario"], PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetchAll();
        }
    }

    static public function mdlRegistrarActividades($tabla, $datos){
        $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (ID_ACTIVIDAD, ID_ACT_PADRE, DESCRIPCION, 
                                                    RUTA_IMG, U_CREA, U_PROCESA, ESTADO)
                                                VALUES (:id_actividad, :id_act_padre, 
                                                    LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["DESCRIPCION"]))."')), 
                                                    :ruta_img, :u_crea ,0, 2)");
        $stmt->bindParam(":id_actividad", $datos["ID_ACTIVIDAD"], PDO::PARAM_STR);                                          
        $stmt->bindParam(":id_act_padre", $datos["ID_ACT_PADRE"], PDO::PARAM_STR);
        $stmt->bindParam(":ruta_img", $datos["RUTA_IMG"], PDO::PARAM_STR);
        $stmt->bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlEditarRegistroActividad($tabla, $datos){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET DESCRIPCION = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["DESCRIPCION"]))."')),
                                                RUTA_IMG = :ruta_img, U_PROCESA = :u_procesa, F_PROCESA = NOW(),
                                                OBSERVACION = :observacion, ESTADO = 2
                                                WHERE   id = :id");
		$stmt->bindParam(":ruta_img", $datos["RUTA_IMG"], PDO::PARAM_STR);
        $stmt->bindParam(":u_procesa", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":observacion", $datos["OBSERVACION"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["ID"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlMostrarActividades($tabla,$item,$valor)
    {   $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where $item = :valor");
        $stmt -> bindParam(":valor", $valor, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlBorrarActividad($tabla,$dato){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = 11 WHERE id = :idActividad");
        $stmt->bindParam(":idActividad", $dato, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlTotalActividades($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(id) as total FROM $tabla");
        $stmt -> execute();
        return $stmt ->fetch();
    }
}