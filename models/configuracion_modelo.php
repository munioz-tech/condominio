<?php
require_once "conexion.php";

class ModeloConfiguraciones{
    static public function mdlObtenerDatosUsuario($tabla,$item,$valor){
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where ESTADO = 1 AND  $item = :idUsuario");
        $stmt -> bindParam(":idUsuario", $valor, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlEditarDatosUsuario($tabla,$datos){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["nombre"]))."')),
                                                apellidos = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["apellidos"]))."')), n_edificio = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["edificio"]))."')), 
                                                n_habitacion = :habitacion, telefono = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["telefono"]))."')), 
                                                correo = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["email"]))."')), u_modifica = :u_modifica, f_modifica = NOW(), foto = :foto
                                                WHERE id = :id");
        $stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
        $stmt->bindParam(":habitacion", $datos["habitacion"], PDO::PARAM_STR);
        $stmt->bindParam(":u_modifica", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $_SESSION["idUsuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlCambiarClaveUsuario($tabla, $Clave){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET clave = :clave, u_modifica = :u_modifica, f_modifica = NOW()
                                                WHERE cedula = :cedula");
        $stmt->bindParam(":clave", $Clave, PDO::PARAM_STR);
        $stmt->bindParam(":u_modifica", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":cedula", $_SESSION["cedulaUsuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }
    
    static public function mdlCambiarNumSeg($tabla,$num_seg){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET num_seg = '$num_seg' WHERE cedula = :cedula");
        $stmt->bindParam(":cedula", $_SESSION["cedulaUsuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }
}