<?php

require_once "conexion.php";

class ModeloNoticias{
    static public function mdlRegistrarNoticia($datoNoticia){
        $stmt = Conexion::conectar()->prepare("INSERT INTO adm_noticia (id_publicacion, id_tipo_publicacion)
                                                VALUES (:id_publicacion, :id_tipo_publicacion)");
        $stmt->bindParam(":id_publicacion", $datoNoticia["id_publicacion"], PDO::PARAM_STR);                                          
        $stmt->bindParam(":id_tipo_publicacion", $datoNoticia["id_tipo_publicacion"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlMostrarNoticiaReuniones(){
        $stmt = Conexion::conectar()->prepare("SELECT tp.id as idTipo, tp.tipo_publicacion, n.id as idNoticia, n.id_publicacion, re.tema,
                                                re.f_crea, re.url, re.fecha_inicio, au.nombre, au.apellidos
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_reuniones re, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = re.id
                                                AND re.u_crea = au.id AND tp.id = 5 ORDER BY n.id DESC");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlMostrarNoticiaActividades(){
        $stmt = Conexion::conectar()->prepare("SELECT tp.id as idTipo, tp.tipo_publicacion, n.id as idNoticia, n.id_publicacion, ra.descripcion, 
                                                ra.f_crea, ra.ruta_img, ra.id_act_padre, aa.tipo_actividad, au.nombre, au.apellidos 
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_registro_act ra, adm_actividades aa, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = ra.id 
                                                AND ra.u_crea = au.id AND ra.id_actividad = aa.id AND tp.id = 1 ORDER BY n.id DESC");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlMostrarNoticiaNegocios(){
        $stmt = Conexion::conectar()->prepare("SELECT tp.id as idTipo, tp.tipo_publicacion, n.id as idNoticia, n.id_publicacion, rn.id_negocio,
                                                rn.descripcion, rn.f_crea, rn.ruta_img, rn.t_negocio, rn.precio, an.tipo_negocio, au.nombre, au.apellidos 
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_registro_neg rn, adm_negocios an, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = rn.id 
                                                AND rn.u_crea = au.id AND rn.id_negocio = an.id AND tp.id = 3 ORDER BY n.id DESC");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlMostrarNoticiaReservas(){
        $stmt = Conexion::conectar()->prepare("SELECT tp.id as idTipo, tp.tipo_publicacion, n.id as idNoticia, n.id_publicacion, rc.id_cancha, 
                                                rc.descripcion, rc.f_crea, rc.fyh_desde, rc.fyh_hasta, ac.cancha, au.nombre, au.apellidos 
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_registro_cancha rc, adm_canchas ac, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = rc.id 
                                                AND rc.u_crea = au.id AND rc.id_cancha = ac.id AND tp.id = 2 ORDER BY n.id DESC");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlMostrarNoticiaBuzon(){
        $stmt = Conexion::conectar()->prepare("SELECT tp.id as idTipo, tp.tipo_publicacion, n.id as idNoticia, n.id_publicacion, 
                                                ab.queja_idea, ab.descripcion, ab.f_crea, au.nombre, au.apellidos 
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_buzon ab, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = ab.id 
                                                AND ab.u_crea = au.id AND tp.id = 4 ORDER BY n.id DESC");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlBorrarNoticia($dato){
        $stmt = Conexion::conectar()->prepare("DELETE FROM adm_noticia WHERE id_publicacion = :id_publicacion");
        $stmt->bindParam(":id_publicacion", $dato, PDO::PARAM_STR);
        return $stmt->execute();
    }

    static public function mdlTotalNoticias($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(id) as total FROM $tabla");
        $stmt -> execute();
        return $stmt ->fetch();
    }

    //====================== Noticias Inicio ==================

    static public function mdlMostrarNoticiaActividadesTop(){
        $stmt = Conexion::conectar()->prepare("SELECT ra.ruta_img, tp.tipo_publicacion, aa.tipo_actividad AS tipoNoticia, au.nombre, au.apellidos, ra.f_crea as fecha
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_registro_act ra, adm_actividades aa, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = ra.id 
                                                AND ra.u_crea = au.id AND ra.id_actividad = aa.id AND tp.id = 1 ORDER BY n.id DESC LIMIT 3");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlMostrarNoticiaReservasTop(){
        $stmt = Conexion::conectar()->prepare("SELECT tp.tipo_publicacion, ac.cancha AS tipoNoticia, au.nombre, au.apellidos, rc.f_crea as fecha
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_registro_cancha rc, adm_canchas ac, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = rc.id 
                                                AND rc.u_crea = au.id AND rc.id_cancha = ac.id AND tp.id = 2 ORDER BY n.id DESC LIMIT 3");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlMostrarNoticiaNegociosTop(){
        $stmt = Conexion::conectar()->prepare("SELECT rn.ruta_img, tp.tipo_publicacion, an.tipo_negocio AS tipoNoticia, au.nombre, au.apellidos, rn.f_crea as fecha
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_registro_neg rn, adm_negocios an, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = rn.id 
                                                AND rn.u_crea = au.id AND rn.id_negocio = an.id AND tp.id = 3 ORDER BY n.id DESC LIMIT 3");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlMostrarNoticiaBuzonTop(){
        $stmt = Conexion::conectar()->prepare("SELECT tp.tipo_publicacion, IF(ab.queja_idea>1,'Idea','Queja') AS tipoNoticia, 
                                                        au.nombre, au.apellidos, ab.f_crea as fecha 
                                                FROM adm_tipo_publicacion tp, adm_noticia n, adm_buzon ab, adm_usuarios au 
                                                WHERE tp.id = n.id_tipo_publicacion AND n.id_publicacion = ab.id AND ab.u_crea = au.id 
                                                AND tp.id = 4 ORDER BY n.id DESC LIMIT 3 ");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }
}