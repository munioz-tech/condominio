<?php

require_once "conexion.php";

class ModeloReporte{
    static public function mdlConsultarReporte($tabla,$select){   
        if($select == 1 || $select == 2 || $select == 3){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
            $stmt -> execute();
            return $stmt -> fetchAll();
        } 
        elseif($select == 4){
            $stmt = Conexion::conectar()->prepare("SELECT AA.TIPO_ACTIVIDAD, AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, ARA.ID_ACT_PADRE, ARA.DESCRIPCION, ARA.F_CREA, ARA.OBSERVACION
                                                FROM $tabla WHERE AA.ID = ARA.ID_ACTIVIDAD
                                                AND AU.ID = ARA.U_CREA ORDER BY F_CREA DESC");
            $stmt -> execute();
            return $stmt -> fetchAll();
        } 
        elseif($select == 5){
            $stmt = Conexion::conectar()->prepare("SELECT AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, AB.DESCRIPCION, AB.QUEJA_IDEA, AB.F_CREA, AB.OBSERVACION
                                                    FROM $tabla WHERE AU.ID = AB.U_CREA 
                                                    ORDER BY F_CREA DESC");
            $stmt -> execute();
            return $stmt -> fetchAll();
        }
        elseif($select == 6){
            $stmt = Conexion::conectar()->prepare("SELECT AN.TIPO_NEGOCIO, AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, ARN.ID_NEGOCIO, ARN.DESCRIPCION, ARN.T_NEGOCIO, ARN.PRECIO, ARN.F_CREA, ARN.OBSERVACION
                                                    FROM $tabla WHERE AN.ID = ARN.ID_NEGOCIO 
                                                    AND AU.ID = ARN.U_CREA ORDER BY F_CREA DESC");
            $stmt -> execute();
            return $stmt -> fetchAll();
        }
        elseif($select == 7){
            $stmt = Conexion::conectar()->prepare("SELECT AC.CANCHA, AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, ARC.ID_CANCHA, ARC.DESCRIPCION, ARC.FYH_DESDE, ARC.FYH_HASTA, ARC.F_CREA, ARC.OBSERVACION
                                                    FROM $tabla WHERE AC.ID = ARC.ID_CANCHA 
                                                    AND AU.ID = ARC.U_CREA ORDER BY F_CREA DESC");
            $stmt -> execute();
            return $stmt -> fetchAll();
        }
    }
}