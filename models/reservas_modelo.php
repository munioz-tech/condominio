<?php

require_once "conexion.php";

class ModeloReservas{
    static public function mdlConsultarCanchas($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where ESTADO = 1");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlObtenerRegistroReserva($tablas){
        if($tablas == null){
            $stmt = Conexion::conectar()->prepare("SELECT ac.cancha, arc.id, arc.descripcion, arc.fyh_desde, arc.fyh_hasta, arc.f_crea, arc.observacion, arc.estado 
                                                    FROM adm_registro_cancha arc, adm_canchas ac where arc.u_crea = :u_crea AND ac.id = arc.id_cancha ORDER BY arc.f_crea DESC");
            $stmt -> bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetchAll();
        }else{
            $stmt = Conexion::conectar()->prepare("SELECT AC.CANCHA, AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, ARC.ID_CANCHA, ARC.DESCRIPCION, ARC.FYH_DESDE, ARC.FYH_HASTA, ARC.F_CREA, ARC.OBSERVACION
                                                    FROM $tablas WHERE AC.ID = ARC.ID_CANCHA 
                                                    AND AU.ID = ARC.U_CREA AND ARC.ESTADO = 10 ORDER BY F_CREA DESC");
            $stmt -> bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetchAll();
        }
    }

    static public function mdlValidarReservaDesde($cancha,$f_desde){
        $stmt = Conexion::conectar()->prepare("SELECT id_cancha, fyh_desde, fyh_hasta FROM adm_registro_cancha 
                                                WHERE id_cancha = $cancha AND fyh_desde = DATE_FORMAT('".$f_desde."', '%Y-%m-%d %H:%i') AND ESTADO = 10");
        $stmt -> execute();
        if($stmt ->fetch()){
            return "ok";
        }else{return "error";}
    }

    static public function mdlValidarF_Desde($cancha,$f_desde){
        $stmt = Conexion::conectar()->prepare("SELECT id_cancha, fyh_desde, fyh_hasta FROM adm_registro_cancha 
                                                WHERE id_cancha = $cancha AND fyh_hasta = DATE_FORMAT('".$f_desde."', '%Y-%m-%d %H:%i') AND ESTADO = 10");
        $stmt -> execute();
        if($stmt ->fetch()){
            return "ok";
        }else{return "error";}
    }

    static public function mdlValidarReservaHasta($cancha,$f_hasta){
        $stmt = Conexion::conectar()->prepare("SELECT id_cancha, fyh_desde, fyh_hasta FROM adm_registro_cancha 
                                                WHERE id_cancha = $cancha AND fyh_hasta = DATE_FORMAT('".$f_hasta."', '%Y-%m-%d %H:%i') AND ESTADO = 10");
        $stmt -> execute();
        if($stmt ->fetch()){
            return "ok";
        }else{return "error";}
    }

    static public function mdlValidarF_Hasta($cancha,$f_hasta){
        $stmt = Conexion::conectar()->prepare("SELECT id_cancha, fyh_desde, fyh_hasta FROM adm_registro_cancha 
                                                WHERE id_cancha = $cancha AND fyh_desde = DATE_FORMAT('".$f_hasta."', '%Y-%m-%d %H:%i') AND ESTADO = 10");
        $stmt -> execute();
        if($stmt ->fetch()){
            return "ok";
        }else{return "error";}
    }

    static public function mdlRegistrarReserva($datos){
        $descripcion = utf8_encode($datos["descripcion"]);
        $stmt = Conexion::conectar()->prepare('INSERT INTO ADM_REGISTRO_CANCHA (id_cancha, separa, descripcion, fyh_desde, fyh_hasta,
                                                            U_CREA, U_PROCESA, ESTADO)
                                                VALUES (:id_cancha, :separa, LTRIM(RTRIM("'.utf8_decode($descripcion).'")), 
                                                DATE_FORMAT("'.$datos["fyh_desde"].'", "%Y-%m-%d %H:%i"), DATE_FORMAT("'.$datos["fyh_hasta"].'", "%Y-%m-%d %H:%i"), :u_crea, 0, 2)');
        $stmt -> bindParam(":id_cancha", $datos["id_cancha"], PDO::PARAM_STR);
        $stmt -> bindParam(":separa", $datos["separa"], PDO::PARAM_STR);    
        //$stmt -> bindParam(":fyh_desde", $desde, PDO::PARAM_STR);
        //$stmt -> bindParam(":fyh_hasta", $hasta, PDO::PARAM_STR);
        $stmt -> bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlEditarRegistroReserva($tabla, $datos){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET descripcion = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["descripcion"]))."')),
                                                        U_PROCESA = :u_procesa, F_PROCESA = NOW(), estado = 2, observacion = :observacion
                                                        WHERE id = :id");
        $stmt -> bindParam(":observacion", $datos["observacion"], PDO::PARAM_STR);
        $stmt -> bindParam(":u_procesa", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["id"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlMostrarReserva($tabla,$item,$valor)
    {   $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where $item = :valor");
        $stmt -> bindParam(":valor", $valor, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlBorrarReserva($tabla,$dato){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = 11 WHERE id = :idReserva");
        $stmt->bindParam(":idReserva", $dato, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlTotalReservas($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(id) as total FROM $tabla");
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlReservar($tablas){
        $stmt = Conexion::conectar()->prepare("SELECT ac.id, ac.cancha, arc.fyh_desde, arc.fyh_hasta
                                                FROM $tablas WHERE ac.id = arc.id_cancha AND arc.estado = 10");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }
}
