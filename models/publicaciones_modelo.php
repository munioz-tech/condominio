<?php

require_once "conexion.php";

class ModeloPublicacion{
    static public function mdlConsultarPublicacion($tablas,$valPublicacion){
        if($valPublicacion == 1){
            $stmt = Conexion::conectar()->prepare("SELECT au.cedula, au.nombre, au.apellidos, au.correo, 
                                                    ara.id, ara.f_crea, ara.estado
                                                    FROM $tablas
                                                    WHERE aa.id = ara.id_actividad
                                                    AND au.id = ara.u_crea
                                                    AND ara.estado = 2
                                                    ORDER BY ara.f_crea DESC");                      
        $stmt -> execute();
        return $stmt -> fetchAll();
        }
        elseif($valPublicacion == 2){
            $stmt = Conexion::conectar()->prepare("SELECT au.cedula, au.nombre, au.apellidos, arn.id, 
                                                    arn.f_crea, arn.estado
                                                    FROM $tablas
                                                    WHERE an.id = arn.id_negocio
                                                    AND au.id = arn.u_crea
                                                    AND arn.estado = 2
                                                    ORDER BY arn.f_crea DESC");
        $stmt -> execute();
        return $stmt -> fetchAll();
        }
        elseif($valPublicacion == 3){
            $stmt = Conexion::conectar()->prepare("SELECT au.cedula, au.nombre, au.apellidos,  
                                                    ab.id, ab.u_crea, ab.f_crea, ab.estado
                                                    FROM $tablas
                                                    WHERE au.id = ab.u_crea
                                                    AND ab.estado = 2
                                                    ORDER BY ab.f_crea DESC");
        $stmt -> execute();
        return $stmt -> fetchAll();
        }
        if($valPublicacion == 4){
            $stmt = Conexion::conectar()->prepare("SELECT au.cedula, au.nombre, au.apellidos, au.correo, 
                                                    arc.id, arc.f_crea, arc.estado
                                                    FROM $tablas
                                                    WHERE ac.id = arc.id_cancha
                                                    AND au.id = arc.u_crea
                                                    AND arc.estado = 2
                                                    ORDER BY arc.f_crea DESC");         
        $stmt -> execute();
        return $stmt -> fetchAll();
        }
    }

    static public function mdlObtenerPublicacion($tablas,$item,$valPublicacion){
        if($valPublicacion == 1){
            $stmt = Conexion::conectar()->prepare("SELECT aa.tipo_actividad, ara.id_actividad, ara.id, ara.id_act_padre, 
                                                    ara.estado, ara.descripcion, ara.ruta_img, au.nombre, au.apellidos, au.telefono, au.correo
                                                    FROM $tablas
                                                    WHERE aa.id = ara.id_actividad
                                                    AND au.id = ara.u_crea
                                                    AND ara.id = :idPublicacion
                                                    AND ara.estado = 2");
            $stmt -> bindParam(":idPublicacion", $item, PDO::PARAM_STR);
            $stmt -> execute(); 
            return $stmt -> fetch();
        }
        if($valPublicacion == 2){
            $stmt = Conexion::conectar()->prepare("SELECT an.tipo_negocio, arn.id_negocio, arn.id, arn.t_negocio, arn.precio, 
                                                    arn.estado, arn.descripcion, arn.ruta_img, au.nombre, au.apellidos, au.telefono, au.correo
                                                    FROM $tablas
                                                    WHERE an.id = arn.id_negocio
                                                    AND au.id = arn.u_crea
                                                    AND arn.id = :idPublicacion
                                                    AND arn.estado = 2");
            $stmt -> bindParam(":idPublicacion", $item, PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt -> fetch();
        }
        if($valPublicacion == 3){
            $stmt = Conexion::conectar()->prepare("SELECT ab.id, ab.descripcion, ab.queja_idea, ab.u_crea, ab.estado, au.nombre, au.apellidos, au.telefono, au.correo
                                                    FROM $tablas
                                                    WHERE au.id = ab.u_crea
                                                    AND ab.id = :idPublicacion
                                                    AND ab.estado = 2");
            $stmt -> bindParam(":idPublicacion", $item, PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt -> fetch();
        }
        if($valPublicacion == 4){
            $stmt = Conexion::conectar()->prepare("SELECT ac.cancha, arc.id_cancha, arc.id, arc.fyh_desde, arc.fyh_hasta,
                                                    arc.estado, arc.descripcion, au.nombre, au.apellidos, au.telefono, au.correo
                                                    FROM $tablas
                                                    WHERE ac.id = arc.id_cancha
                                                    AND au.id = arc.u_crea
                                                    AND arc.id = :idPublicacion
                                                    AND arc.estado = 2");
            $stmt -> bindParam(":idPublicacion", $item, PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt -> fetch();
        }
    }

    static public function mdlProcesarPublicacion($tabla,$item){
        $stmt = Conexion::conectar()->prepare("SELECT id, observacion, estado
                                                FROM $tabla
                                                WHERE id = :idProcesar");
        $stmt -> bindParam(":idProcesar", $item, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt -> fetch();
    }

    static public function mdlEnviarPublicacion($tabla,$datos){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET OBSERVACION = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["OBSERVACION"]))."')),
                                                U_PROCESA = :u_procesa, F_PROCESA = NOW(), ESTADO = :estado WHERE id = :id");
        $stmt->bindParam(":u_procesa", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":estado", $datos["ESTADO"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["ID"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }
}