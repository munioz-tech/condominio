<?php

require_once "conexion.php";

class ModeloBuzon{
    static public function mdlObtenerRegistroBuzon($tablas){
        if($tablas == null){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM ADM_BUZON WHERE u_crea = :u_crea ORDER BY f_crea DESC");
            $stmt -> bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetchAll();
        }else{
            $stmt = Conexion::conectar()->prepare("SELECT AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, AB.DESCRIPCION, AB.QUEJA_IDEA, AB.F_CREA, AB.OBSERVACION
                                                    FROM $tablas WHERE AU.ID = AB.U_CREA 
                                                    AND AB.ESTADO = 10 ORDER BY F_CREA DESC");
            $stmt -> execute();
            return $stmt ->fetchAll();
        }
    }

    static public function mdlRegistrarBuzon($datos){
        $descripcion = utf8_encode($datos["DESCRIPCION"]);
        $buzon = $datos["QUEJA_IDEA"];
        $idUsuario = $_SESSION["idUsuario"];
        $stmt = Conexion::conectar()->prepare('INSERT INTO ADM_BUZON (DESCRIPCION, QUEJA_IDEA,
                                                            U_CREA, U_PROCESA, ESTADO)
                                                VALUES (LTRIM(RTRIM("'.utf8_decode($descripcion).'")), 
                                                '.$buzon.', '.$idUsuario.',0, 2)');
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlMostrarBuzon($tabla,$item,$valor)
    {   $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where $item = :valor");
        $stmt -> bindParam(":valor", $valor, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlEditarRegistroBuzon($tabla, $datos){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET DESCRIPCION = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["DESCRIPCION"]))."')),
                                                        U_PROCESA = :u_procesa, F_PROCESA = NOW(), estado = 2, OBSERVACION = :observacion
                                                    WHERE   ID = :id");
        $stmt->bindParam(":u_procesa", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":observacion", $datos["OBSERVACION"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["ID"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlBorrarBuzon($tabla,$dato){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = 11 WHERE id = :idBuzon");
        $stmt->bindParam(":idBuzon", $dato, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlTotalBuzon($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(id) as total FROM $tabla");
        $stmt -> execute();
        return $stmt ->fetch();
    }
}