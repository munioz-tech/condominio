<?php

require_once "conexion.php";

class ModeloReuniones{
    static public function mdlObtenerRegistroReuniones(){
        $stmt = Conexion::conectar()->prepare("SELECT * FROM ADM_REUNIONES WHERE u_crea = :u_crea");
        $stmt -> bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlObtenerIdReuniones(){
        $stmt = Conexion::conectar()->prepare("SELECT ID FROM ADM_REUNIONES
                                                ORDER BY ID DESC LIMIT 1");
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlRegistrarReunion($Datos){
        print_r($Datos);
        $idUsuario = $_SESSION["idUsuario"];
        $stmt = Conexion::conectar()->prepare('INSERT INTO ADM_REUNIONES (ID_REUNION, URL, CODIGO_ACCESO, FECHA_INICIO, TEMA,U_CREA)
                                                VALUES (:idReunion, :url, :clave, :fecha, :tema, :ucrea)');
        $stmt->bindParam(":idReunion", $Datos["idReunion"], PDO::PARAM_STR);
        $stmt->bindParam(":url", $Datos["url"], PDO::PARAM_STR);
        $stmt->bindParam(":clave", $Datos["clave"], PDO::PARAM_STR);
        $stmt->bindParam(":fecha", $Datos["fecha"], PDO::PARAM_STR);
        $stmt->bindParam(":tema", $Datos["tema"], PDO::PARAM_STR);
        $stmt->bindParam(":ucrea", $idUsuario, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlBorrarReunion($tabla,$dato){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = 11 WHERE id = :idReunion");
        $stmt->bindParam(":idReunion", $dato, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }
}