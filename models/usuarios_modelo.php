<?php

require_once "conexion.php";

class ModeloUsuarios{
    static public function mdlMostrarUsuarios($tablas,$item,$valor)
    {   if($item != null){
            $stmt = Conexion::conectar()->prepare("SELECT AU.ID,SU.CEDULA, SU.CLAVE, SUR.ID_ROL, AU.NOMBRE, AU.APELLIDOS,AU.FOTO,
                                                    AU.N_EDIFICIO, AU.N_HABITACION, AU.TELEFONO, AU.CORREO, SR.DESCRIPCION AS ROL
                                                    FROM $tablas
                                                    WHERE AU.CEDULA = SU.CEDULA  
                                                    AND SU.CEDULA = SUR.CEDULA
                                                    AND SUR.ID_ROL = SR.ID
                                                    AND $item = :usuario
                                                    AND SU.CONFIRMACION = 'N'");
            $stmt -> bindParam(":usuario", $valor, PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt -> fetch();
        }
        else {
            $stmt = Conexion::conectar()->prepare("SELECT   AU.id, AU.cedula, AU.nombre, AU.foto, AU.apellidos, 
                                                            AU.n_edificio, AU.n_habitacion, AU.correo, 
                                                            SU.estado, SU.clave, SU.num_seg, SR.descripcion as rol, SUR.id_rol 
                                                    FROM $tablas 
                                                    WHERE SU.CEDULA = SUR.CEDULA 
                                                    AND SU.CEDULA = AU.CEDULA
                                                    AND SUR.ID_ROL = SR.ID");
            $stmt -> execute();
            return $stmt ->fetchAll();
        }
    }

    static public function mdlInsertarUsuarioLogin($respuesta,$observacion,$estado)
    {   $consultarLogin = ModeloUsuarios::mdlConsultarIntentosLogin($respuesta["ID_ROL"],$respuesta["CEDULA"]);
        $obtenerIp = Utils::obtenerIp();
        $idrol = $respuesta["ID_ROL"];
        $cedula = $respuesta["CEDULA"];
        if(isset($consultarLogin[7]) || !empty($consultarLogin[7])){$n_intentos = $consultarLogin[7];}
        else{$n_intentos = 0; } 
        if($estado == 8){$n_intentos = $n_intentos + 1;}
        else{$n_intentos = 0;}
        $stmt = Conexion::conectar()->prepare('INSERT INTO GEN_LOGIN (ID_ROL, CEDULA, IP_INGRESO, F_INGRESO, OBSERVACION, IDESTADO, N_INTENTOS)
                                                VALUES ("'.$idrol.'", "'.$cedula.'", "'.$obtenerIp.'", NOW(), "'.$observacion.'", '.$estado.', '.$n_intentos.')'); 
        $stmt -> execute();
        return $stmt -> fetch();
    }

    static public function mdlActivarUsuarioLogin($cedula,$idrol,$observacion,$estado)
    {   $obtenerIp = ModeloUsuarios::obtenerIp();
        $n_intentos = 0;
        $stmt = Conexion::conectar()->prepare('INSERT INTO GEN_LOGIN (ID_ROL, CEDULA, IP_INGRESO, F_INGRESO, OBSERVACION, IDESTADO, N_INTENTOS)
                                                VALUES ('.$idrol.', "'.$cedula.'", "'.$obtenerIp.'", NOW(), "'.$observacion.'", '.$estado.', '.$n_intentos.')'); 
        $stmt -> execute();
        return $stmt -> fetch();
    }
    
    static public function mdlActivarRolUsuario($cedula,$estado,$UsuarioModifica,$tabla)
    {   $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = $estado, u_modifica = :u_modifica, f_modifica = NOW()
                                                        WHERE cedula = '".$cedula."'");
        $stmt->bindParam(":u_modifica", $UsuarioModifica, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt -> fetch();
    }

    static public function mdlConsultarIntentosLogin($id_rol,$cedula)
    {   if($cedula != null)
        {   $stmt = Conexion::conectar()->prepare("SELECT *
                                                    FROM GEN_LOGIN
                                                    WHERE Date_format(F_INGRESO, '%d/%M/%Y') = Date_format(now(),'%d/%M/%Y')
                                                    AND ID_ROL = :id_rol
                                                    AND CEDULA = :usuario
                                                    ORDER BY ID DESC LIMIT 1");
            $stmt -> bindParam(":id_rol", $id_rol, PDO::PARAM_STR);
            $stmt -> bindParam(":usuario", $cedula, PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt -> fetch();
        }else{  
                $stmt = Conexion::conectar()->prepare("SELECT * FROM GEN_LOGIN ORDER BY F_INGRESO DESC");                                                
                $stmt -> execute();
                return $stmt -> fetchAll();
            }
    }

    static public function mdlBloqueoIntentosLogin($tablas,$id_rol,$cedula)
    {   $stmt = Conexion::conectar()->prepare("UPDATE $tablas
                                                SET au.ESTADO = 9, su.ESTADO = 9, sur.ESTADO = 9
                                                WHERE au.CEDULA = su.CEDULA
                                                AND su.CEDULA = sur.CEDULA
                                                AND au.CEDULA = :usuario
                                                AND su.CEDULA = :usuario
                                                AND sur.CEDULA = :usuario
                                                AND sur.ID_ROL = :id_rol");
        $stmt -> bindParam(":id_rol", $id_rol, PDO::PARAM_STR);
        $stmt -> bindParam(":usuario", $cedula, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt -> fetch();
    }

    static public function mdlObtenerRolesLogin($tabla)
    {   
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");
        $stmt -> execute();
        return $stmt -> fetchall();
    }

    static public function mdlMostrarUsuariosAlerta($tablas,$item,$valor)
    {   $stmt = Conexion::conectar()->prepare("SELECT SU.CEDULA, SU.NUM_SEG, AU.NOMBRE, AU.APELLIDOS, AU.N_EDIFICIO,
                                                AU.N_HABITACION, AU.TELEFONO, AU.CORREO
                                                FROM $tablas
                                                WHERE SU.CEDULA = AU.CEDULA
                                                AND $item = :usuario
                                                AND SU.ESTADO = 1");
        $stmt -> bindParam(":usuario", $valor, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt -> fetch();
    }

    static public function mdlUsuariosRecibirAlerta($tablasAdm,$itemAdm,$rol)
    {   $stmt = Conexion::conectar()->prepare("SELECT AU.NOMBRE, AU.CORREO, AU.TELEFONO, SUR.ID_ROL
                                                FROM $tablasAdm
                                                WHERE SU.CEDULA = AU.CEDULA
                                                AND SU.CEDULA = SUR.CEDULA
                                                AND $itemAdm = :id_rol
                                                AND SU.ESTADO = 1");
        $stmt -> bindParam(":id_rol", $rol, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt -> fetchAll();
    }

    static public function mdlInsertarAlerta($usersendalert,$txtAlert)
    {   $obtenerIp = Utils::obtenerIp();
        $cedula = $usersendalert["CEDULA"];
        $stmt = Conexion::conectar()->prepare('INSERT INTO SEG_ALERTA (CEDULA, DETALLE, IP, F_CREA)
                                                VALUES ("'.$cedula.'", RTRIM("'.utf8_decode($txtAlert).'"),"'.$obtenerIp.'", NOW())');
        $stmt -> execute();
        return $stmt -> fetch();
    }

    static public function mdlConsultarInvitados()
    {
        $stmt = Conexion::conectar()->prepare("SELECT * FROM ADM_INVITADO ORDER BY F_INGRESO DESC");
        $stmt -> execute();
        return $stmt -> fetchall();
    }

    static public function mdlInsertarInvitado($datos)
    {   $obtenerIp = Utils::obtenerIp();
        $cedula = $datos["CEDULA"];
        $nombre = $datos["NOMBRE"];
        $apellidos = $datos["APELLIDOS"];
        $telefono = $datos["TELEFONO"];
        $correo = $datos["CORREO"];
        $stmt = Conexion::conectar()->prepare('INSERT INTO ADM_INVITADO (CEDULA, NOMBRE, APELLIDOS, TELEFONO, CORREO, IP, F_INGRESO)
        VALUES ("'.$cedula.'", RTRIM("'.utf8_decode(utf8_encode($nombre)).'"), RTRIM("'.utf8_decode(utf8_encode($apellidos)).'"), "'.$telefono.'", RTRIM("'.utf8_encode($correo).'"), "'.$obtenerIp.'", NOW())');
        $stmt -> execute();
        return $stmt -> fetch();
    }

    static public function mdlConsultarAlertasUsuario($cedula)
    {   if($cedula != null){
        $stmt = Conexion::conectar()->prepare("SELECT CEDULA
                                                FROM SEG_ALERTA
                                                WHERE Date_format(F_CREA, '%d/%M/%Y') = Date_format(now(),'%d/%M/%Y')
                                                AND CEDULA = :usuario");
        $stmt -> bindParam(":usuario", $cedula, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt -> fetchAll();
        }else{  $stmt = Conexion::conectar()->prepare("SELECT * FROM SEG_ALERTA ORDER BY F_CREA DESC");
                $stmt -> execute();
                return $stmt -> fetchAll();}
    }

    static public function mdlIngresarAdmUsuario($tabla,$datos)
    {   $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(cedula,nombre,apellidos,n_edificio,n_habitacion,
                                                            telefono,correo,foto,u_crea,f_crea) 
                                                VALUES (:cedula,:nombre,:apellidos,:edificio,:habitacion,
                                                        :telefono,:correo,:foto,:u_crea,NOW())");
        $stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
        $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":apellidos", $datos["apellidos"], PDO::PARAM_STR);
        $stmt->bindParam(":edificio", $datos["edificio"], PDO::PARAM_STR);
        $stmt->bindParam(":habitacion", $datos["habitacion"], PDO::PARAM_STR);
        $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
        $stmt->bindParam(":correo", $datos["email"], PDO::PARAM_STR);
        $stmt->bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlIngresarSegUsuario($tabla,$datos)
    {   $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(cedula,clave,num_seg,confirmacion,u_crea,f_crea) 
                                                VALUES (:cedula,:clave,:num_seg,'N',:u_crea,NOW())");
        $stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
        $stmt->bindParam(":clave", $datos["password"], PDO::PARAM_STR);
		$stmt->bindParam(":num_seg", $datos["num_seg"], PDO::PARAM_STR);
        $stmt->bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlAsingarRolUsuario($tabla,$datos)
    {   $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(cedula,id_rol,u_crea,f_crea) 
                                                VALUES (:cedula,:rol,:u_crea,NOW())");
        $stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
        $stmt->bindParam(":rol", $datos["perfil"], PDO::PARAM_STR);
        $stmt->bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlEditarSegUsuario($tabla,$datos)
    {   $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET cedula = :cedula, clave = :clave, num_seg = :num_seg,
                                                            u_modifica = :u_modifica, f_modifica = NOW()
                                                        WHERE cedula = :cedulaActual");
		$stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
        $stmt->bindParam(":clave", $datos["password"], PDO::PARAM_STR);
        $stmt->bindParam(":num_seg", $datos["num_seg"], PDO::PARAM_STR);
        $stmt->bindParam(":u_modifica", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":cedulaActual", $datos["cedulaActual"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlEditarRolUsuario($tabla,$datos)
    {   $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET id_rol = :rol, u_modifica = :u_modifica, f_modifica = NOW()
                                                        WHERE cedula = :cedula");
        $stmt->bindParam(":rol", $datos["perfil"], PDO::PARAM_STR);
        $stmt->bindParam(":u_modifica", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }
    
    static public function mdlEditarAdmUsuario($tabla,$datos)
    {   $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, apellidos = :apellidos, n_edificio = :n_edificio,
                                                                    n_habitacion = :n_habitacion, telefono = :telefono, 
                                                                    correo = :correo, foto = :foto, 
                                                                    u_modifica = :u_modifica, f_modifica = NOW()
                                                        WHERE cedula = :cedula");
        $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
        $stmt->bindParam(":apellidos", $datos["apellidos"], PDO::PARAM_STR);
		$stmt->bindParam(":n_edificio", $datos["edificio"], PDO::PARAM_STR);
		$stmt->bindParam(":n_habitacion", $datos["habitacion"], PDO::PARAM_STR);
        $stmt->bindParam(":correo", $datos["email"], PDO::PARAM_STR);
        $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
        $stmt->bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
        $stmt->bindParam(":u_modifica", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }
    
    static public function mdlActualizarUsuario($tabla,$item1,$valor1,$item2,$valor2, $u_modifica){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1, u_modifica = :u_modifica, f_modifica = NOW() 
                                                WHERE $item2 = :$item2");
		$stmt->bindParam(":".$item1, $valor1, PDO::PARAM_STR);
        $stmt->bindParam(":".$item2, $valor2, PDO::PARAM_STR);
        $stmt->bindParam(":u_modifica", $u_modifica, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }
    
    static public function mdlBorrarUsuario($tabla,$datos){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = 11, u_modifica = :u_modifica, f_modifica = NOW() WHERE cedula = :cedula");
        $stmt->bindParam(":u_modifica", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":cedula", $datos, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function obtenerIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {   $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            {   $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
            $ip = $_SERVER['REMOTE_ADDR'];
            } return $ip;
    }

    static public function mdlTotalAccesos($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(id) as total FROM $tabla");
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlTotalUsuarios($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(cedula) as total FROM $tabla WHERE estado = 1");
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlObtenerAccesos($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT DATE(f_ingreso) AS FECHA, COUNT(f_ingreso) AS TOTAL FROM gen_login GROUP BY DATE(f_ingreso)");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }
}