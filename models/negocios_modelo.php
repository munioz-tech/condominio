<?php

require_once "conexion.php";

class ModeloNegocios{
    static public function mdlObtenerNegocios($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where ESTADO = 1");
        $stmt -> execute();
        return $stmt ->fetchAll();
    }

    static public function mdlObtenerRegistroNegocio($tabla){
        if($tabla == null){
            $stmt = Conexion::conectar()->prepare("SELECT * FROM ADM_REGISTRO_NEG where u_crea = :u_crea ORDER BY f_crea DESC");
            $stmt -> bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetchAll();
        }else{
            $stmt = Conexion::conectar()->prepare("SELECT AN.TIPO_NEGOCIO, AU.CEDULA, AU.NOMBRE, AU.APELLIDOS, ARN.ID_NEGOCIO, ARN.DESCRIPCION, ARN.T_NEGOCIO, ARN.PRECIO, ARN.F_CREA, ARN.OBSERVACION
                                                    FROM $tabla WHERE AN.ID = ARN.ID_NEGOCIO 
                                                    AND AU.ID = ARN.U_CREA AND ARN.ESTADO = 10 ORDER BY F_CREA DESC");
            $stmt -> bindParam(":u_crea", $_SESSION["idUsuario"], PDO::PARAM_STR);
            $stmt -> execute();
            return $stmt ->fetchAll();
        }
    }

    static public function mdlRegistrarNegocios($datos){
        $stmt = Conexion::conectar()->prepare('INSERT INTO ADM_REGISTRO_NEG (ID_NEGOCIO, DESCRIPCION, T_NEGOCIO,
                                                    PRECIO, U_CREA, U_PROCESA, RUTA_IMG, ESTADO)
                                                VALUES ('.$datos["ID_NEGOCIO"].', LTRIM(RTRIM("'.utf8_decode(utf8_encode($datos["DESCRIPCION"])).'")), 
                                                    LTRIM(RTRIM("'.utf8_decode(utf8_encode($datos["T_NEGOCIO"])).'")), LTRIM(RTRIM("'.utf8_decode(utf8_encode($datos["PRECIO"])).'")), 
                                                    '.$_SESSION["idUsuario"].', 0, "'.$datos["RUTA_IMG"].'", 2)');
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlEditarRegistroNegocio($datos){
        $stmt = Conexion::conectar()->prepare("UPDATE ADM_REGISTRO_NEG SET descripcion = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["DESCRIPCION"]))."')), 
                                                t_negocio = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["T_NEGOCIO"]))."')), 
                                                precio = LTRIM(RTRIM('".utf8_decode(utf8_encode($datos["PRECIO"]))."')), 
                                                U_PROCESA = :u_procesa, F_PROCESA = NOW(), ruta_img = :ruta_img, observacion = :observacion, estado = 2
                                                WHERE   ID = :id");
        $stmt->bindParam(":ruta_img", $datos["ruta_img"], PDO::PARAM_STR);
        $stmt->bindParam(":u_procesa", $_SESSION["idUsuario"], PDO::PARAM_STR);
        $stmt->bindParam(":observacion", $datos["OBSERVACION"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["ID"], PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlMostrarNegocios($tabla,$item,$valor)
    {   $stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla where $item = :valor");
        $stmt -> bindParam(":valor", $valor, PDO::PARAM_STR);
        $stmt -> execute();
        return $stmt ->fetch();
    }

    static public function mdlBorrarNegocio($tabla,$dato){
        $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = 11 WHERE id = :idNegocio");
        $stmt->bindParam(":idNegocio", $dato, PDO::PARAM_STR);
        if($stmt->execute()){
            return "ok";
        }else{  return "error";}
    }

    static public function mdlTotalNegocios($tabla){
        $stmt = Conexion::conectar()->prepare("SELECT COUNT(id) as total FROM $tabla");
        $stmt -> execute();
        return $stmt ->fetch();
    }
}