<?php
class ControladorNegocios{
    static public function ctrConsultarNegocios(){
        $tabla = "ADM_NEGOCIOS";
        $respuesta = ModeloNegocios::mdlObtenerNegocios($tabla);
        return $respuesta;
    }

    static public function ctrMostrarNegocio($item,$valor)
    {   $tabla = "ADM_REGISTRO_NEG";
        $respuesta = ModeloNegocios::mdlMostrarNegocios($tabla,$item,$valor);
        return $respuesta;
    }

    static public function ctrRegistrarNegocio(){
        if(isset($_POST["selectNegocio"]) && !empty($_POST["selectNegocio"])){
            if(isset($_POST["negocioDetalle"]) && !empty($_POST["negocioDetalle"])){
                   $ruta = ""; 
                if(isset($_FILES["fotoNegocio"]["tmp_name"]) && !empty($_FILES["fotoNegocio"]["tmp_name"])){
                    list($ancho, $alto) = getimagesize($_FILES["fotoNegocio"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "view/img/negocios/".$_SESSION["cedulaUsuario"];
                    if(!file_exists($directorio)){
                        mkdir($directorio, 0755);
                    }
                    
                    if($_FILES["fotoNegocio"]["type"] == "image/jpeg"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/negocios/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["fotoNegocio"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }
                    if($_FILES["fotoNegocio"]["type"] == "image/png"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/negocios/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["fotoNegocio"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }
                $datos = array( "ID_NEGOCIO" => $_POST["selectNegocio"],
                                "DESCRIPCION" => $_POST["negocioDetalle"],
                                "T_NEGOCIO" => $_POST["tipoNegocio"],
                                "PRECIO" => number_format($_POST["precioNegocio"],2,',',''),
                                "RUTA_IMG" => $ruta
                                );
                $respuesta = ModeloNegocios::mdlRegistrarNegocios($datos);   
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Publicación de Negocio Enviada","Su solicitud será procesada","");}
                else{Utils::MensajesSweetAlert("error","Ooops...","No se puede enviar su solicitud","");}  
            }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
        }
    }

    static public function ctrEditarNegocio(){
        if(isset($_POST["negocioDetalle"])){
            if(!empty($_POST["negocioDetalle"])){
                $ruta =  $_POST["fotoNegocioActual"];
                if(isset($_FILES["fotoNegocio"]["tmp_name"]) && !empty($_FILES["fotoNegocio"]["tmp_name"])){
                    list($ancho, $alto) = getimagesize($_FILES["fotoNegocio"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "view/img/negocios/".$_SESSION["cedulaUsuario"];
                    
                    if(!empty($_POST["fotoNegocioActual"])){
                        unlink($_POST["fotoNegocioActual"]);
                    }   else   {
                        mkdir($directorio, 0755);
                    }
                    
                    if($_FILES["fotoNegocio"]["type"] == "image/jpeg"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/negocios/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["fotoNegocio"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }
                    if($_FILES["fotoNegocio"]["type"] == "image/png"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/negocios/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["fotoNegocio"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }

                $datos = array( "DESCRIPCION" => $_POST["negocioDetalle"],
                                "T_NEGOCIO" => $_POST["tipoNegocio"],
                                "PRECIO" => number_format($_POST["precioNegocio"],2,',',''),
                                "ruta_img" => $ruta,
                                "ID" => $_POST["idNegocioHidden"],
                                "OBSERVACION" => "Negocio Modificado"
                            );
                $respuesta = ModeloNegocios::mdlEditarRegistroNegocio($datos);
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Los cambios se guardaron correctamente!","negocios");}
                elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el ringreso del negocio!","negocios");}
            }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
        }
    }

    static public function ctrProcesarNegocio(){
        if(empty($_POST["idNegocioHidden"])){
            ControladorNegocios::ctrRegistrarNegocio();
        } else {    ControladorNegocios::ctrEditarNegocio();}
    }

    static public function ctrBorrarNegocio(){
        if(isset($_GET["idNegocio"])){
            $tabla = "adm_registro_neg";
            $dato = $_GET["idNegocio"];
            
            if($_GET["fotoNegocio"] != ""){unlink($_GET["fotoNegocio"]);}            
            $respuesta = ModeloNegocios::mdlBorrarNegocio($tabla,$dato);
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Negocio borrado correctamente!","negocios");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Algo falló al intentar borrar este negocio!","negocios");}
        }
    }

    static public function ctrTotalNegocios(){
        return ModeloNegocios::mdlTotalNegocios("adm_registro_neg");
    }
}