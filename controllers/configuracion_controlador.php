<?php
class ControladorConfiguraciones{
    static public function ctrObtenerDatosUsuario($tabla,$item,$valor){
        $respuesta = ModeloConfiguraciones::mdlObtenerDatosUsuario($tabla,$item,$valor);
        return $respuesta;
    }

    static public function ctrEditarDatosUsuario(){
        if(isset($_POST["confNombre"]) || isset($_POST["confApellido"]) || isset($_POST["confEdificio"])
            || isset($_POST["confHabitacion"]) || isset($_POST["confTelefono"]) || isset($_POST["confEmail"])){
            $tabla = "adm_usuarios";
            $ruta = $_POST["conffotoActual"];    
            if(isset($_FILES["confFoto"]["tmp_name"]) && !empty($_FILES["confFoto"]["tmp_name"])){
                list($ancho, $alto) = getimagesize($_FILES["confFoto"]["tmp_name"]);
                $nuevoAncho = 500;
                $nuevoAlto = 500;
                $directorio = "view/img/usuarios/".$_SESSION["cedulaUsuario"];
                
                if(!empty($_POST["conffotoActual"])){
                    unlink($_POST["conffotoActual"]);
                }   else   {
                    mkdir($directorio, 0755);
                }
                
                if($_FILES["confFoto"]["type"] == "image/jpeg"){
                    $aleatorio = mt_rand(100, 900);
                    $ruta = "view/img/usuarios/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".jpg";
                    $origen = imagecreatefromjpeg($_FILES["confFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagejpeg($destino, $ruta);
                }
                if($_FILES["confFoto"]["type"] == "image/png"){
                    $aleatorio = mt_rand(100, 900);
                    $ruta = "view/img/usuarios/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".png";
                    $origen = imagecreatefrompng($_FILES["confFoto"]["tmp_name"]);
                    $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                    imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                    imagepng($destino, $ruta);
                }
            }
            $tabla2 = "seg_usuario";
            $dato1 = str_split($_POST["confNombre"]);
            $dato2 = substr($_SESSION["cedulaUsuario"], -2);
            $dato = $dato1[0].$dato2;
            $datos = array( "nombre" => $_POST["confNombre"],
                            "apellidos" => $_POST["confApellido"],
                            "edificio" => $_POST["confEdificio"],
                            "habitacion" => $_POST["confHabitacion"],
                            "telefono" => $_POST["confTelefono"],
                            "email" => $_POST["confEmail"],
                            "num_seg" => $dato,
                            "foto" => $ruta
                        );
            $respuesta = ModeloConfiguraciones::mdlEditarDatosUsuario($tabla,$datos);
            $cambiarNumSeg = ModeloConfiguraciones::mdlCambiarNumSeg($tabla2,$dato);
            $enviaCorreo = Utils::EnviarCorreoNumSegEditado($datos);

            $_SESSION["foto"] = $datos["foto"]; 
            $_SESSION["nombreUsuario"] = $datos["nombre"];
            $_SESSION["apellidosUsuario"] = $datos["apellidos"];

            if($respuesta == "ok"){ clearstatcache(); Utils::MensajesSweetAlert("success","Excelente","¡El usuario ha sido modificado correctamente!","configuracion");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Ocurrió un fallo al modificar el usuario!","configuracion");}
            clearstatcache();
        }
    }

    static public function ctrCambiarClaveUsuario(){
        if(isset($_POST["confPassword"]) && !empty($_POST["confPassword"])){ 
            $tabla = "seg_usuario";
            $encriptar = crypt($_POST["confPassword"], '$2a$07$usesomesillystringfore593TESIS./20condominioBjqp8I90dH6hi$');
            $respuesta = ModeloConfiguraciones::mdlCambiarClaveUsuario($tabla,$encriptar);
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Contraseña cambiada correctamente!","configuracion");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Ocurrió un fallo al modificar su contraseña!","configuracion");}
        }
    }
}