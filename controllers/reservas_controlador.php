<?php
class ControladorReservas{
    static public function ctrConsultarCanchas(){
        $tabla = "ADM_CANCHAS";
        $respuesta = ModeloReservas::mdlConsultarCanchas($tabla);
        return $respuesta;
    }

    static public function ctrRegistrarReserva(){ 
        if(isset($_POST["selectReservas"]) && !empty($_POST["selectReservas"])){
            if(isset($_POST["date_desde"]) && !empty($_POST["date_desde"])){
                if(isset($_POST["reservaDetalle"]) && !empty($_POST["reservaDetalle"])){
                    $separa = $_SESSION["nombreUsuario"]." ".$_SESSION["apellidosUsuario"];
                    $cancha = $_POST["selectReservas"];
                    $f_desde = substr($_POST["date_desde"], 0, 16);
                    $f_hasta = substr($_POST["date_desde"], -16);
                    $reservaDesde = ModeloReservas::mdlValidarReservaDesde($cancha,$f_desde);
                    $reservaHasta = ModeloReservas::mdlValidarReservaHasta($cancha,$f_hasta);
                    ini_set('date.timezone','America/Guayaquil'); $fecha = date("Y-m-d H:i");
                    if(strtotime($f_desde) > strtotime($fecha)){
                        $h_desde = substr($_POST["date_desde"],11,-19);
                        $h_hasta = substr($_POST["date_desde"],-5);
                        if(strtotime($h_desde) >= strtotime("7:00") && strtotime($h_desde) <= strtotime("21:00")){
                            if(strtotime($h_hasta) >= strtotime("7:00") && strtotime($h_hasta) <= strtotime("22:00")){
                                if($reservaDesde == "error" && $reservaHasta == "error"){
                                    $fDesde = ModeloReservas::mdlValidarF_Desde($cancha,$f_desde);
                                    $fHasta = ModeloReservas::mdlValidarF_Hasta($cancha,$f_desde);
                                    if($fDesde == "error" && $fHasta == "error"){
                                        $datos = array( "id_cancha" => $_POST["selectReservas"],
                                                        "separa" => $separa,
                                                        "descripcion" => $_POST["reservaDetalle"],
                                                        "fyh_desde" => $f_desde,
                                                        "fyh_hasta" => $f_hasta
                                                        );
                                        $respuesta = ModeloReservas::mdlRegistrarReserva($datos);
                                        if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Reserva realizada con éxito","Verificaremos la disponibilidad de las canchas","reservas");}
                                        else{Utils::MensajesSweetAlert("error","Ooops...","No se puede enviar su solicitud","reservas");}
                                    }else{Utils::MensajesSweetAlert("error","Ooops...","La cancha ya se encuentra reservada para la fecha seleccionada 1","reservas");}
                                }else{Utils::MensajesSweetAlert("error","Ooops...","La cancha ya se encuentra reservada para la fecha seleccionada","reservas");}
                            }else{Utils::MensajesSweetAlert("error","Ooops...","Las canchas no se encuentran disponibles para la hora de fin seleccionada. Horario disponible hasta 22:00","reservas");}
                        }else{Utils::MensajesSweetAlert("error","Ooops...","Las canchas no se encuentran disponibles para la hora de inicio seleccionada. Horario disponible desde 7:00 hasta 21:00","reservas");}
                    }else{Utils::MensajesSweetAlert("error","Ooops...","La fecha de reserva no puede ser la misma o anterior a la fecha actual","reservas");}
                }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
            }else{Utils::MensajesSweetAlert("error","Ooops...","Elija la fecha","");}
        }
    }

    static public function ctrEditarReserva(){
        if(isset($_POST["reservaDetalle"])){
            if(!empty($_POST["reservaDetalle"])){
                $datos = array( "descripcion" => $_POST["reservaDetalle"],
                                "id" => $_POST["idReservaHidden"],
                                "observacion" => "Registro Modificado"
                            );
                $tabla = "adm_registro_cancha";
                $respuesta = ModeloReservas::mdlEditarRegistroReserva($tabla,$datos);
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Los cambios se guardaron correctamente!","reservas");}
                elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el reingreso de su reserva!","reservas");}
            }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
        }
    }

    static public function ctrBorrarReserva(){
        if(isset($_GET["idReserva"])){
            $tabla = "adm_registro_cancha";
            $dato = $_GET["idReserva"];
            $respuesta = ModeloReservas::mdlBorrarReserva($tabla,$dato);
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","Registro borrado correctamente!","reservas");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Algo falló al intentar borrar este registro!","reservas");}
            
        }
    }

    static public function ctrProcesarReserva(){
        if(empty($_POST["idReservaHidden"])){self::ctrRegistrarReserva();}
        else {self::ctrEditarReserva();}
    }

    static public function ctrMostrarReserva($item,$valor)
    {   $tabla = "ADM_REGISTRO_CANCHA";
        $respuesta = ModeloReservas::mdlMostrarReserva($tabla,$item,$valor);
        return $respuesta;
    }

    static public function ctrTotalReservas(){
        return ModeloReservas::mdlTotalReservas("adm_registro_cancha");
    }

    static public function ctrReservar(){
        $tablas = "adm_canchas ac, adm_registro_cancha arc";
        $reservar = ModeloReservas::mdlReservar($tablas);//print_r($reservar);
        return $reservar;
    }
}
