<?php
class ControladorUsuario{
    static public function ctrIngresoUsuario()
    {   if(isset($_POST["ingUsuario"]) && !empty($_POST["ingUsuario"]))
        {   if(preg_match('/^[0-9]+$/',$_POST["ingUsuario"])
            && preg_match('/^[a-zA-Z0-9]+$/',$_POST["ingPassword"]))
            {   $rol = $_POST["ingRol"];
                $tablas = "seg_usuario su, seg_usuario_rol sur, adm_usuarios au, seg_rol sr";
                $item = "su.cedula";
                $valor = $_POST["ingUsuario"];
                $respuesta = ModeloUsuarios::MdlMostrarUsuarios($tablas,$item,$valor);
                if ($respuesta)
                {   if($respuesta["CEDULA"] == $_POST["ingUsuario"])
                    {   if($respuesta["ID_ROL"] == $rol[0])
                        {   $validaLogin = ControladorUsuario::ctrValidaIntentosLogin($respuesta["ID_ROL"],$respuesta["CEDULA"]);
                            if($validaLogin == "ok")
                            {   $encriptar = crypt($_POST["ingPassword"], '$2a$07$usesomesillystringfore593TESIS./20condominioBjqp8I90dH6hi$');
                                if( $respuesta["CLAVE"] == $encriptar)
                                {   $observacion = "Intento Exitoso";
                                    $estado = 7;
                                    $insertarLogin = ModeloUsuarios::mdlInsertarUsuarioLogin($respuesta,$observacion,$estado);
                                    $_SESSION["SesionActiva"] = "ok";
                                    $_SESSION["idUsuario"] = $respuesta["ID"];
                                    $_SESSION["cedulaUsuario"] = $respuesta["CEDULA"];
                                    $_SESSION["nombreUsuario"] = $respuesta["NOMBRE"];
                                    $_SESSION["apellidosUsuario"] = $respuesta["APELLIDOS"];
                                    $_SESSION["foto"] = $respuesta["FOTO"];
                                    $_SESSION["idRol"] = $respuesta["ID_ROL"];
                                    echo '<script>
                                            window.location = "inicio";
                                        </script>';
                                }else{  $observacion = "Intento Fallido";
                                        $estado = 8;
                                        ModeloUsuarios::mdlInsertarUsuarioLogin($respuesta,$observacion,$estado);
                                        echo '<br><div class="alert alert-danger">Contraseña incorrecta, vuelve intentarlo</div>';
                                    }
                            }else{Utils::MensajesSweetAlert("error","¡Intentos de sesión agotados!","Usuario bloqueado, ha superado el máximo de intentos, contáctese con el administrador","");}
                        }else{echo '<br><div class="alert alert-danger">Perfil incorrecto, vuelve intentarlo</div>';}
                    }else{echo '<br><div class="alert alert-danger">Usuario incorrecto, vuelve intentarlo</div>';}   
                }else{echo '<br><div class="alert alert-danger">Usuario incorrecto, vuelve intentarlo</div>';}   
            }else{echo '<br><div class="alert alert-danger">Ingresar campos correctamente, vuelve intentarlo</div>';}
        }
    }

    static public function ctrValidaIntentosLogin($id_rol,$cedula)
    {   $consultarLogin = ModeloUsuarios::mdlConsultarIntentosLogin($id_rol,$cedula);
        $tablas = "seg_usuario su, seg_usuario_rol sur, adm_usuarios au";
        if(isset($consultarLogin[7]) && $consultarLogin[7] >= 3)
        {   $bloquear = ModeloUsuarios::mdlBloqueoIntentosLogin($tablas,$id_rol,$cedula);
            $tablas1 = "seg_usuario su, adm_usuarios au";
            $item = "su.cedula";
            $usuario = ModeloUsuarios::mdlMostrarUsuariosAlerta($tablas1,$item,$cedula);
            Utils::avisoBloqueo($usuario);
            return "error";
        }
        return "ok";
    }

    static public function ctrCrearAlertaUsuario()
    {   if(isset($_SESSION["cedulaUsuario"])){
            if(isset($_POST["codigoSeguridad"]))
            {   if(preg_match('/^[A-Z0-9]+$/',$_POST["codigoSeguridad"]))
                {   $tablas = "seg_usuario su, adm_usuarios au";
                    $item = "su.cedula";
                    $valor = $_SESSION["cedulaUsuario"];
                    $usersendalert = ModeloUsuarios::mdlMostrarUsuariosAlerta($tablas,$item,$valor);

                    $tablasAdm = "seg_usuario su, adm_usuarios au, seg_usuario_rol sur";
                    $itemAdm = "sur.id_rol";
                    $rol = "1";
                    $useradmalert = ModeloUsuarios::mdlUsuariosRecibirAlerta($tablasAdm,$itemAdm,$rol);
                    foreach($useradmalert as $correo){$correoAdm[] = $correo["CORREO"];}
                    if($usersendalert["CEDULA"] == $_SESSION["cedulaUsuario"])
                    {   if($usersendalert["NUM_SEG"] == $_POST["codigoSeguridad"])
                        {   if(empty($_POST["alertDetalle"]))
                            {   $validarIntentos = ControladorUsuario::ctrValidaIntentosAlerta($usersendalert["CEDULA"]);
                                if($validarIntentos == "ok")
                                {   $txtAlert = Utils::PlantillaAlerta();
                                    $enviaCorreo = Utils::envioAlerta($usersendalert,$correoAdm,$txtAlert);
                                    Utils::avisoAlerta($usersendalert,$txtAlert);
                                    if($enviaCorreo == "OK")
                                    {   ModeloUsuarios::mdlInsertarAlerta($usersendalert,$txtAlert);
                                        Utils::MensajesSweetAlert("success","Aviso Enviado","Correo de aviso enviado correctamente","");
                                    } 
                                    elseif($enviaCorreo == "Error"){Utils::MensajesSweetAlert("error","Ooops...","No se pudo enviar el correo","");} 
                                    else{Utils::MensajesSweetAlert("error","¡Error Inesperado!","Ocurrió un error inesperado","");}
                                }else{Utils::MensajesSweetAlert("error","¡Intentos Agotados!","Usted ha superado el límite diario de intentos","");}
                            } 
                            else {
                                $countAlertDetalle = str_replace(' ', '', $_POST["alertDetalle"]);
                                if(strlen($countAlertDetalle) >= 5)
                                {   $validarIntentos = ControladorUsuario::ctrValidaIntentosAlerta($usersendalert["CEDULA"]);
                                    if($validarIntentos == "ok")
                                    {   $txtAlert = $_POST["alertDetalle"];
                                        $enviaCorreo = Utils::envioAlerta($usersendalert,$correoAdm,$txtAlert);
                                        Utils::avisoAlerta($usersendalert,$txtAlert);
                                        if($enviaCorreo == "OK")
                                        {   ModeloUsuarios::mdlInsertarAlerta($usersendalert,$txtAlert);
                                            Utils::MensajesSweetAlert("success","Aviso Enviado","Correo de aviso enviado correctamente","");} 
                                        elseif($enviaCorreo == "Error")
                                            {Utils::MensajesSweetAlert("error","Ooops...","No se pudo enviar el correo","");}
                                        else{Utils::MensajesSweetAlert("error","¡Error Inesperado!","Ocurrió un error inesperado","");}
                                    } 
                                    else{Utils::MensajesSweetAlert("error","¡Intentos Agotados!","Usted ha superado el límite diario de intentos","");}
                                }
                                else{Utils::MensajesSweetAlert("error","Ooops...","¡El texto es opcional O debe contener mínimo 5 palabras!","");}
                            }
                        }else{Utils::MensajesSweetAlert("error","Ooops...","¡Código de seguridad incorrecto!","");}
                    }else{Utils::MensajesSweetAlert("error","Ooops...","¡Usuario incorrecto!","");}            
                }else{Utils::MensajesSweetAlert("error","Ooops...","¡Digite los campos correctamente por favor!","");}
            }
        } else{
            if(isset($_POST["alertUsuario"]) && isset($_POST["codigoSeguridad"]))
            {   if(preg_match('/^[0-9]+$/',$_POST["alertUsuario"])
                && preg_match('/^[A-Z0-9]+$/',$_POST["codigoSeguridad"]))
                {   $tablas = "seg_usuario su, adm_usuarios au";
                    $item = "su.cedula";
                    $valor = $_POST["alertUsuario"];
                    $usersendalert = ModeloUsuarios::mdlMostrarUsuariosAlerta($tablas,$item,$valor);

                    $tablasAdm = "seg_usuario su, adm_usuarios au, seg_usuario_rol sur";
                    $itemAdm = "sur.id_rol";
                    $rol = "1";
                    $useradmalert = ModeloUsuarios::mdlUsuariosRecibirAlerta($tablasAdm,$itemAdm,$rol);
                    foreach($useradmalert as $correo){$correoAdm[] = $correo["CORREO"];}
                    if($usersendalert["CEDULA"] == $_POST["alertUsuario"])
                    {   if($usersendalert["NUM_SEG"] == $_POST["codigoSeguridad"])
                        {   if(empty($_POST["alertDetalle"]))
                            {   $validarIntentos = ControladorUsuario::ctrValidaIntentosAlerta($usersendalert["CEDULA"]);
                                if($validarIntentos == "ok")
                                {   $txtAlert = Utils::PlantillaAlerta();
                                    $enviaCorreo = Utils::envioAlerta($usersendalert,$correoAdm,$txtAlert);
                                    Utils::avisoAlerta($usersendalert,$txtAlert);
                                    if($enviaCorreo == "OK")
                                    {   ModeloUsuarios::mdlInsertarAlerta($usersendalert,$txtAlert);
                                        Utils::MensajesSweetAlert("success","Aviso Enviado","Correo de aviso enviado correctamente","");
                                    } 
                                    elseif($enviaCorreo == "Error"){Utils::MensajesSweetAlert("error","Ooops...","No se pudo enviar el correo","");} 
                                    else{Utils::MensajesSweetAlert("error","¡Error Inesperado!","Ocurrió un error inesperado","");}
                                }else{Utils::MensajesSweetAlert("error","¡Intentos Agotados!","Usted ha superado el límite diario de intentos","");}
                            } 
                            else {
                                $countAlertDetalle = str_replace(' ', '', $_POST["alertDetalle"]);
                                if(strlen($countAlertDetalle) >= 5)
                                {   $validarIntentos = ControladorUsuario::ctrValidaIntentosAlerta($usersendalert["CEDULA"]);
                                    if($validarIntentos == "ok")
                                    {   $txtAlert = $_POST["alertDetalle"];
                                        $enviaCorreo = Utils::envioAlerta($usersendalert,$correoAdm,$txtAlert);
                                        Utils::avisoAlerta($usersendalert,$txtAlert);
                                        if($enviaCorreo == "OK")
                                        {   ModeloUsuarios::mdlInsertarAlerta($usersendalert,$txtAlert);
                                            Utils::MensajesSweetAlert("success","Aviso Enviado","Correo de aviso enviado correctamente","");} 
                                        elseif($enviaCorreo == "Error")
                                            {Utils::MensajesSweetAlert("error","Ooops...","No se pudo enviar el correo","");}
                                        else{Utils::MensajesSweetAlert("error","¡Error Inesperado!","Ocurrió un error inesperado","");}
                                    } 
                                    else{Utils::MensajesSweetAlert("error","¡Intentos Agotados!","Usted ha superado el límite diario de intentos","");}
                                }
                                else{Utils::MensajesSweetAlert("error","Ooops...","¡El texto es opcional O debe contener mínimo 5 palabras!","");}
                            }
                        }else{Utils::MensajesSweetAlert("error","Ooops...","¡Código de seguridad incorrecto!","");}
                    }else{Utils::MensajesSweetAlert("error","Ooops...","¡Usuario incorrecto!","");}            
                }else{Utils::MensajesSweetAlert("error","Ooops...","¡Digite los campos correctamente por favor!","");}
            }
        }
    }

    static public function ctrUsuarioInvitado(){
        if(isset($_POST["invCedula"]) && isset($_POST["invNombre"]) && isset($_POST["invApellidos"])
        && isset($_POST["invTelefono"]) && isset($_POST["invCorreo"]))
        {   if(preg_match('/^[0-9]+$/',$_POST["invCedula"]) && 
                preg_match('/^[A-Za-zñÑáéíóúÁÉÍÓÚ]+$/',$_POST["invNombre"])&& 
                preg_match('/^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["invApellidos"]) && 
                preg_match('/^[0-9]+$/',$_POST["invTelefono"]) && 
                preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})+$/',$_POST["invCorreo"]))
            {   $datos = array( "CEDULA" => $_POST["invCedula"],
                                "NOMBRE" => $_POST["invNombre"],
                                "APELLIDOS" => $_POST["invApellidos"],
                                "TELEFONO" => $_POST["invTelefono"],
                                "CORREO" => $_POST["invCorreo"]
                            );
                ModeloUsuarios::mdlInsertarInvitado($datos);                            
                $_SESSION["SesionActiva"] = "ok";
                $_SESSION["nombreUsuario"] = $datos["NOMBRE"];
                $_SESSION["apellidosUsuario"] = $datos["APELLIDOS"];
                $_SESSION["rol"] = 3;                                
                echo '<script>
                window.location = "inicio";
                </script>';
            } else {Utils::MensajesSweetAlert("error","Ooops...","¡Ingrese los datos correctamente!","");}
        }
    }

    static public function ctrValidaIntentosAlerta($cedula)
    {   $consultarAlertas = ModeloUsuarios::mdlConsultarAlertasUsuario($cedula);
        if(sizeof($consultarAlertas) >= 3){
            return "error";
        } else { return "ok";}
    }

    static public function ctrCrearUsuario(){
        if(isset($_POST["nuevaCedula"])){
            $ValidarPassword = ControladorUsuario::ctrValidarPass($_POST["nuevoPassword"]);
            if($ValidarPassword == "ok"){
                if( preg_match('/^[0-9]+$/',$_POST["nuevaCedula"]) &&
                preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["nuevoNombre"]) && 
                preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["nuevoApellido"]) &&
                preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})+$/',$_POST["nuevoEmail"]) &&
                preg_match('/^[a-zA-Z0-9 ]+$/',$_POST["nuevoPassword"]) &&
                preg_match('/^[0-9]+$/',$_POST["nuevoTelefono"])){
                
                    $ruta = "";
                
                    if(isset($_FILES["nuevaFoto"]["tmp_name"]) && $_FILES["nuevaFoto"]["tmp_name"] != ""){
                        list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);
                        $nuevoAncho = 500;
                        $nuevoAlto = 500;
                        $directorio = "view/img/usuarios/".$_POST["nuevaCedula"];
                        mkdir($directorio, 0755);
                        
                        if($_FILES["nuevaFoto"]["type"] == "image/jpeg"){
                            $aleatorio = mt_rand(100, 900);
                            $ruta = "view/img/usuarios/".$_POST["nuevaCedula"]."/".$aleatorio.".jpg";
                            $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagejpeg($destino, $ruta);
                        }
                        if($_FILES["nuevaFoto"]["type"] == "image/png"){
                            $aleatorio = mt_rand(100, 900);
                            $ruta = "view/img/usuarios/".$_POST["nuevaCedula"]."/".$aleatorio.".png";
                            $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                            imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagepng($destino, $ruta);
                        }
                    }

                    $tabla1 = "seg_usuario";
                    $tabla2 = "adm_usuarios";
                    $tabla3 = "seg_usuario_rol";

                    $encriptar = crypt($_POST["nuevoPassword"], '$2a$07$usesomesillystringfore593TESIS./20condominioBjqp8I90dH6hi$');
                    
                    $dato1 = str_split($_POST["nuevoNombre"]);
                    $dato2 = substr($_POST["nuevaCedula"], -2);
                    $numero_seguridad = $dato1[0].$dato2;


                    $datos = array( "perfil" => $_POST["nuevoPerfil"],
                                    "cedula" => $_POST["nuevaCedula"],
                                    "nombre" => $_POST["nuevoNombre"],
                                    "apellidos" => $_POST["nuevoApellido"],
                                    "password" => $encriptar,
                                    "edificio" => $_POST["nuevoEdificio"],
                                    "habitacion" => $_POST["nuevaHabitacion"],
                                    "email" => $_POST["nuevoEmail"],
                                    "telefono" => $_POST["nuevoTelefono"],
                                    "num_seg" => $numero_seguridad,
                                    "foto" => $ruta);

                    ModeloUsuarios::mdlIngresarSegUsuario($tabla1, $datos);
                    ModeloUsuarios::mdlAsingarRolUsuario($tabla3, $datos);
                    $respuesta = ModeloUsuarios::mdlIngresarAdmUsuario($tabla2, $datos);
                    Utils::EnviarCorreoNumSeg($datos);

                    if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡El usuario ha sido guardado correctamente!","usuarios");}
                    elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el ingreso del usuario!","usuarios");}
                }else{Utils::MensajesSweetAlert("error","Ooops...","¡El nombre de usuario o la contrañesa no puede ir vacío o con caracteres especiales!","usuarios");}
            }else{Utils::MensajesSweetAlert("error","Contraseña Insegura","¡Por favor la contraseña debe tener mínimo una mayuscula, una minuscula y un número!","usuarios");}
        }
    }
    
    static public function ctrMostrarUsuario($item, $valor){
        $tabla = "seg_usuario su, seg_usuario_rol sur, adm_usuarios au, seg_rol sr";
        $respuesta = ModeloUsuarios::mdlMostrarUsuarios($tabla, $item, $valor);
        return $respuesta;
    }
    
    static public function ctrEditarUsuario(){
        if(isset($_POST["editarCedula"])){
            if( preg_match('/^[0-9]+$/',$_POST["editarCedula"]) &&
                preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["editarNombre"]) && 
                preg_match('/^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["editarApellido"]) &&
                preg_match('/^[A-z0-9\\._-]+@[A-z0-9][A-z0-9-]*(\\.[A-z0-9_-]+)*\\.([A-z]{2,6})+$/',$_POST["editarEmail"]) &&
                preg_match('/^[0-9]+$/',$_POST["editarTelefono"])){

                if($_POST["editarPassword"] != ""){
                    if(preg_match('/^[a-zA-Z0-9 ]+$/',$_POST["editarPassword"])){
                        $encriptar = crypt($_POST["editarPassword"], '$2a$07$usesomesillystringfore593TESIS./20condominioBjqp8I90dH6hi$');
                    }
                    else{ return Utils::MensajesSweetAlert("error","Ooops...","¡La contraseña no puede ir vacía o llevar caracteres especiales!","usuarios");}                 
                }   
                else    {
                    $encriptar = $_POST["passwordActual"];
                }   
                
                $ruta = $_POST["fotoActual"];
                
                if(isset($_FILES["editarFoto"]["tmp_name"]) && !empty($_FILES["editarFoto"]["tmp_name"])){
                    list($ancho, $alto) = getimagesize($_FILES["editarFoto"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "view/img/usuarios/".$_POST["cedulaActual"];
                    
                    if(!empty($_POST["fotoActual"])){
                        unlink($_POST["fotoActual"]);
                        mkdir($directorio, 0755);
                    }   else   {
                        mkdir($directorio, 0755);
                    }
                    
                    if($_FILES["editarFoto"]["type"] == "image/jpeg"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/usuarios/".$_POST["cedulaActual"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["editarFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }
                    if($_FILES["editarFoto"]["type"] == "image/png"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/usuarios/".$_POST["cedulaActual"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["editarFoto"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }

                $dato1 = str_split($_POST["editarNombre"]);
                $dato2 = substr($_POST["editarCedula"], -2);
                $numero_seguridad = $dato1[0].$dato2;

                $datos = array( "perfil" => $_POST["editarPerfil"],
                                "cedula" => $_POST["editarCedula"],
                                "cedulaActual" => $_POST["cedulaActual"],
                                "nombre" => $_POST["editarNombre"],
                                "apellidos" => $_POST["editarApellido"],
                                "password" => $encriptar,
                                "edificio" => $_POST["editarEdificio"],
                                "habitacion" => $_POST["editarHabitacion"],
                                "email" => $_POST["editarEmail"],
                                "telefono" => $_POST["editarTelefono"],
                                "num_seg" => $numero_seguridad,
                                "foto" => $ruta);

                $tabla1 = "seg_usuario";
                $tabla2 = "adm_usuarios";
                $tabla3 = "seg_usuario_rol";

                ModeloUsuarios::mdlEditarSegUsuario($tabla1, $datos);
                ModeloUsuarios::mdlEditarRolUsuario($tabla3, $datos);
                $respuesta = ModeloUsuarios::mdlEditarAdmUsuario($tabla2, $datos);
                Utils::EnviarCorreoNumSegEditado($datos);

                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Los cambios se guardaron correctamente!","usuarios");}
                elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el reingreso del usuario!","usuarios");}
            }   else{Utils::MensajesSweetAlert("error","Ooops...","¡Los campos no pueden estar vacío o tener caracteres especiales!","usuarios");}
        }
    }
    
    static public function ctrBorrarUsuario(){
        if(isset($_GET["idUsuario"])){
            $tabla = "seg_usuario";
            $datos = $_GET["idUsuario"];
            
            $respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $datos);
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡El usuario ha sido borrado correctamente!","usuarios");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Algo falló al intentar borrar al usuario!","usuarios");}
        }
    }

    static public function ctrValidarPass($password){
        $arrayPass = str_split($password);
        if(sizeof($arrayPass) >= 6){
            $mayus = 0;
            $minus = 0;
            $num = 0;
            foreach($arrayPass as $pass){
                if(ctype_upper($pass)){$mayus = $mayus +1;}
                elseif(ctype_lower($pass)){$minus = $minus + 1;}
                elseif(!ctype_alpha($num)){$num = $num + 1;}
            }
            if($mayus > 0 && $minus > 0 && $num > 0){return "ok";}
            else{return "error";}
        }else{return "error";}

    }

    static public function ctrTotalAccesos(){
        return ModeloUsuarios::mdlTotalAccesos("gen_login");
    }

    static public function ctrTotalUsuarios(){
        return ModeloUsuarios::mdlTotalUsuarios("seg_usuario");
    }

    static public function ctrConsultarAccesos(){
        return ModeloUsuarios::mdlObtenerAccesos("gen_login");
    }
}