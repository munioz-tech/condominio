<?php
class ControladorNoticias{
    static public function ctrRegistrarNoticia($datoNoticia){
        $respuesta = ModeloNoticias::mdlRegistrarNoticia($datoNoticia);
        return $respuesta;
    }

    static public function ctrMostrarNoticiaReunion(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaReuniones();
        return $respuesta;
    }

    static public function ctrMostrarNoticiaActividades(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaActividades();
        return $respuesta;
    }

    static public function ctrMostrarNoticiaNegocios(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaNegocios();
        return $respuesta;
    }

    static public function ctrMostrarNoticiaReservas(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaReservas();
        return $respuesta;
    }

    static public function ctrMostrarNoticiaBuzon(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaBuzon();
        return $respuesta;
    }
    
    static public function ctrTotalNoticia(){
        return ModeloNoticias::mdlTotalNoticias("adm_noticia");
    }

    //====================== Noticias Inicio ==================

    static public function ctrMostrarNoticiaActividadesTop(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaActividadesTop();
        return $respuesta;
    }

    static public function ctrMostrarNoticiaReservasTop(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaReservasTop();
        return $respuesta;
    }

    static public function ctrMostrarNoticiaNegociosTop(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaNegociosTop();
        return $respuesta;
    }

    static public function ctrMostrarNoticiaBuzonTop(){
        $respuesta = ModeloNoticias::mdlMostrarNoticiaBuzonTop();
        return $respuesta;
    }
}