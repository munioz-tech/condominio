<?php
class ControladorPublicacion{
    /*====================================
            MOSTRAR Y CONSULTAR
    ====================================*/
    static public function ctrConsultarPublicaciones($valPublicacion){
        if($valPublicacion == 1){
            $tablas = "adm_actividades aa, adm_registro_act ara, adm_usuarios au";
            $publicacion = ModeloPublicacion::mdlConsultarPublicacion($tablas,$valPublicacion);
            return $publicacion;
        }
        elseif($valPublicacion == 2){
            $tablas = "adm_negocios an, adm_registro_neg arn, adm_usuarios au";
            $publicacion = ModeloPublicacion::mdlConsultarPublicacion($tablas,$valPublicacion);
            return $publicacion;
        }
        elseif($valPublicacion == 3){
            $tablas = "adm_buzon ab, adm_usuarios au";
            $publicacion = ModeloPublicacion::mdlConsultarPublicacion($tablas,$valPublicacion);
            return $publicacion;
        }
        if($valPublicacion == 4){
            $tablas = "adm_canchas ac, adm_registro_cancha arc, adm_usuarios au";
            $publicacion = ModeloPublicacion::mdlConsultarPublicacion($tablas,$valPublicacion);
            return $publicacion;
        }
    }

    static public function ctrConsultarPublicacion($tablas,$item,$valPublicacion){
            $publicacion = ModeloPublicacion::mdlObtenerPublicacion($tablas,$item,$valPublicacion);
            return $publicacion;
    }

    /*====================================
            ACEPTAR O RECHAZAR
    ====================================*/
    static public function ctrProcesarPublicacionAct(){
        if(isset($_GET["valPublicar"]) && isset($_GET["procesar"]) && isset($_GET["idPublicacion"])
            && isset($_GET["observacion"]) && !empty($_GET["observacion"])){
            $datos = array( "procesar" => $_GET["procesar"],
                            "observacion" => $_GET["observacion"]
                        );
            $item = $_GET["idPublicacion"];
            $valPublica = $_GET["valPublicar"];
            if($_GET["valPublicar"] == 1){
                $tablas = "adm_registro_act ara, adm_actividades aa, adm_usuarios au";
                $publicacion = ModeloPublicacion::mdlObtenerPublicacion($tablas,$item,$valPublica);
                $CorreoUsuario = $publicacion['correo'];
                $tabla = "adm_registro_act";
                if($_GET["procesar"] == 1){
                    ControladorPublicacion::ctrRechazarPublicacion($tabla);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
                else{
                    ControladorPublicacion::ctrAceptarPublicacion($tabla);
                    $aceptar = ControladorPublicacion::ctrAceptarPublicacion($tabla);
                    $datoNoticia = array( "id_publicacion" => $item,
                                          "id_tipo_publicacion" => 1
                                        );
                    $noticia = ControladorNoticias::ctrRegistrarNoticia($datoNoticia);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
            }
            if($_GET["valPublicar"] == 2){
                $tablas = "adm_negocios an, adm_registro_neg arn, adm_usuarios au";
                $publicacion = ModeloPublicacion::mdlObtenerPublicacion($tablas,$item,$valPublica);
                $CorreoUsuario = $publicacion['correo'];
                $tabla = "adm_registro_neg";
                if($_GET["procesar"] == 1){
                    ControladorPublicacion::ctrRechazarPublicacion($tabla);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
                else{
                    ControladorPublicacion::ctrAceptarPublicacion($tabla);
                    $aceptar = ControladorPublicacion::ctrAceptarPublicacion($tabla);
                    $datoNoticia = array( "id_publicacion" => $item,
                                          "id_tipo_publicacion" => 3
                                        );
                    $noticia = ControladorNoticias::ctrRegistrarNoticia($datoNoticia);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
            }
            if($_GET["valPublicar"] == 3){
                $tablas = "adm_buzon ab, adm_usuarios au";
                $publicacion = ModeloPublicacion::mdlObtenerPublicacion($tablas,$item,$valPublica);
                $CorreoUsuario = $publicacion['correo'];
                $tabla = "adm_buzon";
                if($_GET["procesar"] == 1){
                    ControladorPublicacion::ctrRechazarPublicacion($tabla);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
                else{
                    ControladorPublicacion::ctrAceptarPublicacion($tabla);
                    $aceptar = ControladorPublicacion::ctrAceptarPublicacion($tabla);
                    $datoNoticia = array( "id_publicacion" => $item,
                                          "id_tipo_publicacion" => 4
                                        );
                    $noticia = ControladorNoticias::ctrRegistrarNoticia($datoNoticia);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
            }
            if($_GET["valPublicar"] == 4){
                $tablas = "adm_canchas ac, adm_registro_cancha arc, adm_usuarios au";
                $publicacion = ModeloPublicacion::mdlObtenerPublicacion($tablas,$item,$valPublica);
                $CorreoUsuario = $publicacion['correo'];
                $tabla = "adm_registro_cancha";
                if($_GET["procesar"] == 1){
                    ControladorPublicacion::ctrRechazarPublicacion($tabla);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
                else{
                    $aceptar = ControladorPublicacion::ctrAceptarPublicacion($tabla);
                    $datoNoticia = array( "id_publicacion" => $item,
                                          "id_tipo_publicacion" => 2
                                        );
                    $noticia = ControladorNoticias::ctrRegistrarNoticia($datoNoticia);
                    Utils::EnviarCorreoProcesarPublicaciones($CorreoUsuario,$datos);
                }
            }
        }
    }

    static public function ctrRechazarPublicacion($tabla){
        if(isset($_GET["idPublicacion"])){
            if(isset($_GET["observacion"]) && !empty($_GET["observacion"])){
                $datos = array( "ID" => $_GET["idPublicacion"],
                                "ESTADO" => 5,
                                "OBSERVACION" => $_GET["observacion"]
                            );
                $respuesta = ModeloPublicacion::mdlEnviarPublicacion($tabla,$datos);
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Los cambios se guardaron correctamente!","publicaciones");}
                elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el procesamiento de esta actividad!","publicaciones");}
            } else{Utils::MensajesSweetAlert("error","Ooops...","¡Observación no puede quedar vacía, inténtelo nuevamente!","publicaciones");}
        }
    }

    static public function ctrAceptarPublicacion($tabla){
        if(isset($_GET["idPublicacion"])){
            if(isset($_GET["observacion"]) && !empty($_GET["observacion"])){
                $datos = array( "ID" => $_GET["idPublicacion"],
                                "ESTADO" => 10,
                                "OBSERVACION" => $_GET["observacion"]
                            );
                $respuesta = ModeloPublicacion::mdlEnviarPublicacion($tabla,$datos);
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Los cambios se guardaron correctamente!","publicaciones");}
                elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el procesamiento de esta actividad!","publicaciones");}
            } else{Utils::MensajesSweetAlert("error","Ooops...","¡Observación no puede quedar vacía, inténtelo nuevamente!","publicaciones");}
        }
    }
}