<?php
class ControladorBuzon{
    static public function ctrMostrarBuzon($item,$valor)
    {   $tabla = "ADM_BUZON";
        $respuesta = ModeloBuzon::mdlMostrarBuzon($tabla,$item,$valor);
        return $respuesta;
    }

    static public function ctrRegistrarBuzon(){ 
        if(isset($_POST["radioBuzon"]) && !empty($_POST["radioBuzon"])){
            if(isset($_POST["buzonDetalle"]) && !empty($_POST["buzonDetalle"])){
                $datos = array( "DESCRIPCION" => $_POST["buzonDetalle"],
                                "QUEJA_IDEA" => $_POST["radioBuzon"]
                                );
                $respuesta = ModeloBuzon::mdlRegistrarBuzon($datos);
                if($respuesta == "ok"){
                    if($_POST["radioBuzon"] == 1){Utils::MensajesSweetAlert("success","Queja Enviada","Procesaremos su queja de manera anónima","buzon");}
                    if($_POST["radioBuzon"] == 2){Utils::MensajesSweetAlert("success","Idea Enviada","Gracias por ayudarnos a mejorar","buzon");}
                }else{Utils::MensajesSweetAlert("error","Ooops...","No se puede enviar su solicitud","");}
            }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
        }
    }

    static public function ctrEditarBuzon(){
        if(isset($_POST["buzonDetalle"])){
            if(!empty($_POST["buzonDetalle"])){
                $tabla = "ADM_BUZON";
                $datos = array( "DESCRIPCION" => $_POST["buzonDetalle"],
                                "ID" => $_POST["idBuzonHidden"],
                                "OBSERVACION" => "Registro Modificado"
                            );
                $respuesta = ModeloBuzon::mdlEditarRegistroBuzon($tabla,$datos);
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Los cambios se guardaron correctamente!","buzon");}
                elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el reingreso en el buzon!","buzon");}
            }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
        }
    }

    static public function ctrBorrarBuzon(){
        if(isset($_GET["idBuzon"])){
            $tabla = "adm_buzon";
            $dato = $_GET["idBuzon"];
            $respuesta = ModeloBuzon::mdlBorrarBuzon($tabla,$dato);
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","Registro borrado correctamente!","buzon");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Algo falló al intentar borrar este registro!","buzon");}
            
        }
    }

    static public function ctrProcesarBuzon(){
        if(empty($_POST["idBuzonHidden"])){
            ControladorBuzon::ctrRegistrarBuzon();
        } else {    ControladorBuzon::ctrEditarBuzon();}
    }

    static public function ctrTotalBuzon(){
        return ModeloBuzon::mdlTotalBuzon("adm_buzon");
    }
}