<?php
class ControladorReuniones{
    static public function ctrRegistrarReunion(){ 
        if(isset($_POST["reunionDetalle"]) && !empty($_POST["reunionDetalle"])){
            $descripcion = $_POST["reunionDetalle"];
            $respuesta = ModeloReuniones::mdlRegistrarReunion($descripcion);
            $obtRegistro = ModeloReuniones::mdlObtenerIdReuniones();
            if(isset($obtRegistro) && !empty($obtRegistro)){
                $datoNoticia = array(   "id_publicacion" => $obtRegistro[0],
                                        "id_tipo_publicacion" => 5
                                    );
                $noticia = ControladorNoticias::ctrRegistrarNoticia($datoNoticia);
            }else{
                $datoNoticia = array(   "id_publicacion" => 1,
                                        "id_tipo_publicacion" => 5
                                    );
                $noticia = ControladorNoticias::ctrRegistrarNoticia($datoNoticia);
            }
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Convocatoria a reunión enviada","Su convocatoria a reunión ha sido publicada","reuniones");}
            else{Utils::MensajesSweetAlert("error","Ooops...","No se puede enviar su solicitud","");}
        }
    }

    static public function ctrBorrarReunion(){
        if(isset($_GET["idReunion"])){
            $tabla = "ADM_REUNIONES";
            $dato = $_GET["idReunion"];
            $respuesta = ModeloReuniones::mdlBorrarReunion($tabla,$dato);
            ModeloNoticias::mdlBorrarNoticia($dato);
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Registro borrado correctamente!","reuniones");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Algo falló al intentar borrar este registro!","reuniones");}
        }
    }



    static public function ctrCrearReunion(){
        if(isset($_POST["nuevoTema"])){
                $FechaHora = $_POST["nuevoDia"].'T'.$_POST["nuevaHora"].':00';
                $validarFecha = ControladorReuniones::ValidarFecha($FechaHora);
                if($validarFecha == "ok"){
                    $Tema = $_POST["nuevoTema"];
                    $Contraseña = $_POST["nuevoPassword"];
                    $Meeting = Meetings::create_meeting($Tema,$FechaHora,$Contraseña);
                    $Data = [   "idReunion" => $Meeting->id,
                                "url"       => $Meeting->join_url,
                                "clave"     => $Contraseña,
                                "fecha"     => $_POST["nuevoDia"].' '.$_POST["nuevaHora"].':00',
                                "tema"      => $Tema 
                    ];
                    if($Meeting){
                        $respuesta = ModeloReuniones::mdlRegistrarReunion($Data);
                        if($respuesta == "ok")
                        {   $obtRegistro = ModeloReuniones::mdlObtenerIdReuniones();
                            if(isset($obtRegistro) && !empty($obtRegistro)){
                                $datoNoticia = array(   "id_publicacion" => $obtRegistro[0],
                                                        "id_tipo_publicacion" => 5
                                                    );
                                ControladorNoticias::ctrRegistrarNoticia($datoNoticia);
                                $tablasAdm = "seg_usuario su, adm_usuarios au, seg_usuario_rol sur";
                                $itemAdm = "sur.id_rol";
                                $rol = "1";
                                $useradmalert = ModeloUsuarios::mdlUsuariosRecibirAlerta($tablasAdm,$itemAdm,$rol);
                                foreach($useradmalert as $correo){$correoAdm[] = $correo["CORREO"];}
                                Utils::avisoBloqueo($correoAdm);
                            }
                            Utils::MensajesSweetAlert("success","Convocatoria a reunión enviada","Su convocatoria a reunión ha sido publicada","reuniones");
                        }else{Utils::MensajesSweetAlert("error","Ooops...","¡Algo falló al guardar la reunión!","reuniones");}
                    }else{Utils::MensajesSweetAlert("error","Oops","¡Ocurrió un error al crear la reunión!","reuniones");}
                }else{Utils::MensajesSweetAlert("error","Oops","¡La fecha debe ser mayor al día actual!","reuniones");}
        }
    }

    static public function ValidarFecha($FechaHora){
        $Fecha = (substr($FechaHora, 0, 10));
        if(date("Y-m-d") < $Fecha){
                return "ok";
        }   else{   return "error"; }
    }
}