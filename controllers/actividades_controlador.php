<?php
class ControladorActividades{
    static public function ctrConsultarActividades(){
        $tabla = "ADM_ACTIVIDADES";
        $item = "id_padre";
        $idPadre = 0;
        $respuesta = ModeloActividades::mdlObtenerActividades($tabla,$item,$idPadre);
        return $respuesta;
    }

    static public function ctrMostrarActividad($item,$valor)
    {   $tabla = "ADM_REGISTRO_ACT";
        $respuesta = ModeloActividades::mdlMostrarActividades($tabla,$item,$valor);
        return $respuesta;
    }

    static public function ctrRegistrarActividad(){
        if(isset($_POST["actividadHijo"]) && !empty($_POST["actividadHijo"])){
            if(isset($_POST["actividadDetalle"]) && !empty($_POST["actividadDetalle"])){
                $ruta = ""; 
                if(isset($_FILES["fotoActividad"]["tmp_name"]) && !empty($_FILES["fotoActividad"]["tmp_name"])){
                    list($ancho, $alto) = getimagesize($_FILES["fotoActividad"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "view/img/actividades/".$_SESSION["cedulaUsuario"];
                    if(!file_exists($directorio)){
                        mkdir($directorio, 0755);
                    } 
                    
                    if($_FILES["fotoActividad"]["type"] == "image/jpeg"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/actividades/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["fotoActividad"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }
                    if($_FILES["fotoActividad"]["type"] == "image/png"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/actividades/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["fotoActividad"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }
                $tabla = "ADM_REGISTRO_ACT";
                $datos = array( "ID_ACTIVIDAD" => $_POST["actividadHijo"],
                                "DESCRIPCION" => $_POST["actividadDetalle"],
                                "ID_ACT_PADRE" => $_POST["actividadPadre"],
                                "RUTA_IMG" => $ruta
                                );
                $respuesta = ModeloActividades::mdlRegistrarActividades($tabla,$datos);
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Actividad Enviada","Espere a que acepten su solicitud","");}
                else{Utils::MensajesSweetAlert("error","Ooops...","No se puede enviar su solicitud","");}  
            }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
        }
    }

    static public function ctrEditarActividad(){
        if(isset($_POST["actividadDetalle"])){
            if(!empty($_POST["actividadDetalle"])){
                $ruta = $_POST["fotoActividadActual"];
                if(isset($_FILES["fotoActividad"]["tmp_name"]) && !empty($_FILES["fotoActividad"]["tmp_name"])){
                    list($ancho, $alto) = getimagesize($_FILES["fotoActividad"]["tmp_name"]);
                    $nuevoAncho = 500;
                    $nuevoAlto = 500;
                    $directorio = "view/img/actividades/".$_SESSION["cedulaUsuario"];

                    if(!empty($_POST["fotoActividadActual"])){
                        unlink($_POST["fotoActividadActual"]);
                    }   else   {
                        if(!file_exists($directorio)){
                            mkdir($directorio, 0755);
                        }
                    }
                    
                    if($_FILES["fotoActividad"]["type"] == "image/jpeg"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/actividades/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".jpg";
                        $origen = imagecreatefromjpeg($_FILES["fotoActividad"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagejpeg($destino, $ruta);
                    }
                    if($_FILES["fotoActividad"]["type"] == "image/png"){
                        $aleatorio = mt_rand(100, 900);
                        $ruta = "view/img/actividades/".$_SESSION["cedulaUsuario"]."/".$aleatorio.".png";
                        $origen = imagecreatefrompng($_FILES["fotoActividad"]["tmp_name"]);
                        $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);
                        imagecopyresized($destino, $origen, 0, 0, 0, 0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                        imagepng($destino, $ruta);
                    }
                }
                $tabla = "ADM_REGISTRO_ACT";
                $datos = array( "DESCRIPCION" => $_POST["actividadDetalle"],
                                "RUTA_IMG" => $ruta,
                                "ID" => $_POST["idActividadHidden"],
                                "OBSERVACION" => "Actividad Modificada"
                                );
                $respuesta = ModeloActividades::mdlEditarRegistroActividad($tabla,$datos);
                if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Los cambios se guardaron correctamente!","actividades");}
                elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Falló el reingreso de su actividad!","actividades");}
            }else{Utils::MensajesSweetAlert("error","Ooops...","La descripción no puede quedar vacía","");}
        }
    }

    static public function ctrProcesarActividad(){
        if(empty($_POST["idActividadHidden"])){
            ControladorActividades::ctrRegistrarActividad();
        } else {    ControladorActividades::ctrEditarActividad();}
    }
    
    static public function ctrBorrarActividad(){
        if(isset($_GET["idActividad"])){
            $tabla = "adm_registro_act";
            $dato = $_GET["idActividad"];
            
            if($_GET["fotoActividad"] != ""){unlink($_GET["fotoActividad"]);}
            $respuesta = ModeloActividades::mdlBorrarActividad($tabla, $dato);
            if($respuesta == "ok"){Utils::MensajesSweetAlert("success","Excelente","¡Actividad borrada correctamente!","actividades");}
            elseif($respuesta == "error"){Utils::MensajesSweetAlert("error","Ooops...","¡Algo falló al intentar borrar esta actividad!","actividades");}
        }
    }

    static public function ctrTotalActividades(){
        return ModeloActividades::mdlTotalActividades("adm_registro_act");
    }
}
